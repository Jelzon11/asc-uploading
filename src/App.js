import React from 'react';
import ContentHolder from './components/Content-Holder';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Login from './components/Login';
import { Provider } from 'react-redux';
import store from './store';

function App() {
  return (
    <div className="wrapper">
      <Router>
        <Switch>
          <Route path="/" exact component={Login} /> 
          <Provider store={store}>
            <ContentHolder />
          </Provider>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
