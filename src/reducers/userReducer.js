import {
  FETCH_USERS,
  ADD_USER,
  FETCH_ME,
  UPDATE_USER,
  DELETE_USER,
  FETCH_LOGS
} from "../actions/types";
//import main_category_list from '../dummy/main-category-list';

const initialState = {
  userItems: [],
  userMe: {},
  logs: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_USERS:
      return {
        ...state,
        userItems: action.payload
      };
    case ADD_USER:
      return {
        ...state,
        userItems: [...state.userItems, action.payload]
      };
    case FETCH_ME:
      return {
        ...state,
        userMe: action.payload
      };
      case FETCH_LOGS:
      return {
        ...state,
        logs: action.payload
      };
    case UPDATE_USER: {
        const filteredState = state.userItems.map(item => {
            if (item.id == action.payload.id) {
              item = { ...item, ...action.payload };
            }
            return item;
          });
          return {
            ...state,
            userItems: [...filteredState]
          };
    }

    case DELETE_USER: {
      const filteredReturnState = state.userItems.filter(item => {
        return item.id != action.payload.id;
      });
      return {
        ...state,
        userItems: [...filteredReturnState]
      };
    }
    default:
      return state;
  }
}
