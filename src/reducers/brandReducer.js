import { FETCH_BRANDS, ADD_BRAND, UPDATE_BRAND, DELETE_BRAND } from "../actions/types";
//import main_category_list from '../dummy/main-category-list';

const initialState = {
  brandItems: [],
  brandItem: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_BRANDS:
      return {
        ...state,
        brandItems: action.payload
      };
    case ADD_BRAND:
      return {
        ...state,
        brandItems: [...state.brandItems, action.payload]
      };
    case UPDATE_BRAND: {
      const filteredState = state.brandItems.map(item => {
        if (item.id == action.payload.id) {
          item = { ...item, ...action.payload };
        }
        return item;
      });
      return {
        ...state,
        brandItems: [...filteredState]
      };
    }
    case DELETE_BRAND: {
        const filteredReturnState = state.brandItems.filter((item) => {
            return item.id != action.payload.id;
        })
        return {
            ...state, 
            brandItems: [...filteredReturnState]
        }
    }
    default:
      return state;
  }
}
