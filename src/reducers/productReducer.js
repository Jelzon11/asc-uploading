import { FETCH_PRODUCTS, ADD_PRODUCT, UPDATE_PRODUCT, DELETE_PRODUCT } from "../actions/types";

const initialState = {
  productItems: [],
  productItem: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_PRODUCTS:
      return {
        ...state,
        productItems: action.payload
      };
    case ADD_PRODUCT:
      return {
        ...state,
        productItems: [...state.productItems, action.payload]
      };
    case UPDATE_PRODUCT: {
      const filteredState = state.productItems.map(item => {
        if (item.id == action.payload.id) {
          item = { ...item, ...action.payload };
        }
        return item;
      });
      return {
        ...state,
        productItems: [...filteredState]
      };
    }
    case DELETE_PRODUCT: {
        const filteredReturnState = state.productItems.filter((item) => {
            return item.id != action.payload.id;
        })
        return {
            ...state, 
            productItems: [...filteredReturnState]
        }
    }
    default:
      return state;
  }
}
