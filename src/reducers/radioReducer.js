import {
  FETCH_RADIO_MATERIALS,
  ADD_RADIO_MATERIAL,
  DELETE_RADIO_MATERIAL
} from "../actions/types";

const initialState = {
  items: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_RADIO_MATERIALS:
      return {
        ...state,
        items: action.payload
      };
    case ADD_RADIO_MATERIAL:
      return {
        ...state,
        items: [...state.items, action.payload]
      };
    case DELETE_RADIO_MATERIAL: {
      const filteredReturnState = state.items.filter(item => {
        return item.id != action.payload.id;
      });
      return {
        ...state,
        items: [...filteredReturnState]
      };
    }
    default:
      return state;
  }
}
