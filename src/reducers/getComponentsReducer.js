import { FETCH_COMPONENTS } from "../actions/types";
const initialState = {
  items: [],
  item: {}
};
export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_COMPONENTS:
      return {
        ...state,
        items: action.payload
      };
    default:
      return state;
  }
}
