import {
  FETCH_SUB_CATEGORIES,
  ADD_SUB_CATEGORY,
  UPDATE_SUB_CATEGORY,
  DELETE_SUB_CATEGORY
} from "../actions/types";

const initialState = {
  items: [],
  item: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_SUB_CATEGORIES:
      return {
        ...state,
        items: action.payload
      };
    case ADD_SUB_CATEGORY:
      return {
        ...state,
        items: [...state.items, action.payload]
      };
    case UPDATE_SUB_CATEGORY: {
      const filteredState = state.items.map(item => {
        if (item.id == action.payload.id) {
          item = { ...item, ...action.payload };
        }
        return item;
      });
      return {
        ...state,
        items: [...filteredState]
      };
    }

    case DELETE_SUB_CATEGORY: {
      const filteredReturnState = state.items.filter(item => {
        return item.id != action.payload.id;
      });
      return {
        ...state,
        items: [...filteredReturnState]
      };
    }
    default:
      return state;
  }
}
