import {
  FETCH_ROLE,
  ADD_ROLE,
  UPDATE_ROLE,
  DELETE_ROLE

} from "../actions/types";

const initialState = {
  items: [],
  item: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_ROLE:
      return {
        ...state,
        items: action.payload
      };
    case ADD_ROLE:
      return {
        ...state,
        items: [...state.items, action.payload]
      };
    case UPDATE_ROLE: {
      const filteredState = state.items.map(item => {
        if (item.id == action.payload.id) {
          item = { ...item, ...action.payload };
        }
        return item;
      });
      return {
        ...state,
        items: [...filteredState]
      };
    }

    case DELETE_ROLE: {
      const filteredReturnState = state.items.filter(item => {
        return item.id != action.payload.id;
      });
      return {
        ...state,
        items: [...filteredReturnState]
      };
    }
    default:
      return state;
  }
}
