import { FETCH_MAIN_CATEGORIES, ADD_MAIN_CATEGORY, UPDATE_MAIN_CATEGORY, DELETE_MAIN_CATEGORY} from '../actions/types';
//import main_category_list from '../dummy/main-category-list';

const initialState = {
    items: [],
    item: {},
    
}

export default function(state=initialState, action){
    switch(action.type){
        case FETCH_MAIN_CATEGORIES:
            return{
                ...state,
                items: action.payload
            }
        case ADD_MAIN_CATEGORY:
            return{
                ...state,
                items: [...state.items,action.payload]
            }
            case UPDATE_MAIN_CATEGORY: {
                const filteredState = state.items.map((item) => {
                    if(item.id == action.payload.id) {
                        item = {...item, ...action.payload}
                    }
                    return item;
                })
                return {
                    ...state,
                    items: [...filteredState]
                }
            }
    
            case DELETE_MAIN_CATEGORY: {
                const filteredReturnState = state.items.filter((item) => {
                    return item.id != action.payload.id;
                })
                return {
                    ...state, 
                    items: [...filteredReturnState]
                }
            }
        default:
            return state;
    }
}