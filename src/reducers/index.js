import { combineReducers } from 'redux';
import main_categories_Reducers from './mainCategoryReducer';
import sub_categories_Reducer from './subCategoryReducer';
import sub_sub_categories_Reducers from './subSubCategoriesReducer';
import company_Reducer from './companyReducer';
import brand_Reducer from './brandReducer';
import user_Reducer from './userReducer';
import radioReducer from './radioReducer';
import roleReducer from './roleReducer';
import getComponentsReducer from './getComponentsReducer';
import productReducer from './productReducer';


export default combineReducers({
    
    main_categories: main_categories_Reducers,
    sub_categories: sub_categories_Reducer,
    sub_sub_categories : sub_sub_categories_Reducers,
    companies: company_Reducer,
    brands: brand_Reducer,
    users: user_Reducer,
    radioMaterials: radioReducer,
    roles: roleReducer,
    components: getComponentsReducer,
    products: productReducer
})