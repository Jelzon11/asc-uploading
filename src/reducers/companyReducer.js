import { FETCH_COMPANIES, ADD_COMPANY, UPDATE_COMPANY, DELETE_COMPANY} from '../actions/types';
//import main_category_list from '../dummy/main-category-list';

const initialState = {
    companyItems: [],
    companyItem: {},
    
}

export default function(state=initialState, action){
    switch(action.type){
        case FETCH_COMPANIES:
            return{
                ...state,
                companyItems: action.payload
            }
        case ADD_COMPANY:
            return{
                ...state,
                companyItems: [...state.companyItems,action.payload]
            }
        case UPDATE_COMPANY: {
            const filteredState = state.companyItems.map((item) => {
                if(item.id == action.payload.id) {
                    item = {...item, ...action.payload}
                }
                return item;
            })
            return {
                ...state,
                companyItems: [...filteredState]
            }
        }

        case DELETE_COMPANY: {
            const filteredReturnState = state.companyItems.filter((item) => {
                return item.id != action.payload.id;
            })
            return {
                ...state, 
                companyItems: [...filteredReturnState]
            }
        }
        default:
            return state;
    }
}