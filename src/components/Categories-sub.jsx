import React, { Fragment, useState, useEffect } from "react";
import SubCategoryModalForm from "./Categories-sub-modal";
import SubCategoryEdit from './SubCategoryEdit';
import SubCategoryDelete from "./SubCategoryDelete";
import {
  MDBDataTable,
  MDBCard,
  MDBCardHeader,
  MDBCardBody,
  MDBBtn,
  MDBIcon
} from "mdbreact";
import { connect } from "react-redux";
import moment from "moment";


const SubCategories = props => {
  const [subCategories, setSubCategories] = useState([]);
  const [isModalOpen, setModalOpen] = useState(false);
  const [isEditModalOpen, setEditModalOpen] = useState(false);
  const [isDeleteModalOpen, setDeleteModalOpen] = useState(false);
  const [isCreatedSuccess, setCreatedSuccess] = useState(false);
  const [subCategoryDetails, setSubCategoryDetails] = useState("");

  const columns = () => {
    return [
      {
        label: "Name",
        field: "name",
        sort: "asc",
        width: 500
      },
      {
        label: "Main Category",
        field: "mainCategoryName",
        sort: "asc",
        width: 500
      },
      {
        label: "Last Updated",
        field: "updatedTimestamp",
        sort: "asc",
        width: 100
      },
      {
        label: "Created Date",
        field: "createdTimestamp",
        sort: "asc",
        width: 100
      },
      {
        label: "Action",
        field: "action",
        sort: "asc",
        width: 100
      }
    ];
  };

  const handleModalOpen = () => {
    if (isModalOpen == false) {
      setModalOpen(true);
    } else {
      setModalOpen(false);
    }
  };
  const handleDeleteModalOpen = subCategory => {
    if (isDeleteModalOpen == false) {
      setDeleteModalOpen(true);
      setSubCategoryDetails(subCategory);
    } else {
      setDeleteModalOpen(false);
    }
  };

  const handleEditModalOpen = subCategory => {
    if (isEditModalOpen == false) {
      setEditModalOpen(true);
      setSubCategoryDetails(subCategory);
    } else {
      setEditModalOpen(false);
    }
  };
  const successToastHandler = () => {
    setCreatedSuccess(true);
  };

  useEffect(() => {
    setSubCategories(props.sub_categories);
  }, [props.sub_categories]);

  const renderData = data => {
    const newData = data.map(item => {
      return {
        name: item.name,
        mainCategoryName: item.mainCategoryDetails.name,
        updatedTimestamp: moment.unix(item["updatedTimestamp"]).calendar(),
        createdTimestamp: moment.unix(item["createdTimestamp"]).format("L"),
        action: (
          <Fragment>
            <center>
              <MDBBtn
                id={item.id}
                color="light-blue"
                size="sm"
                onClick={() => handleEditModalOpen(item)}
              >
                <MDBIcon icon="pencil-alt" />
              </MDBBtn>
              <MDBBtn
                id={item.id}
                color="blue-grey"
                size="sm"
                onClick={() => handleDeleteModalOpen(item)}
              >
                <MDBIcon icon="trash-alt" />
              </MDBBtn>
            </center>
          </Fragment>
        )
      };
    });
    return newData;
  };

  return (
    <Fragment>
      <SubCategoryModalForm
        isModalOpen={isModalOpen}
        successToastHandler={successToastHandler}
        handleModalOpen={handleModalOpen}
      />
      <SubCategoryEdit
        isEditModalOpen={isEditModalOpen}
        handleEditModalOpen={handleEditModalOpen}
        subCategoryDetails={subCategoryDetails}
      />
      <SubCategoryDelete
       isDeleteModalOpen={isDeleteModalOpen}
       handleDeleteModalOpen={handleDeleteModalOpen}
        subCategoryDetails={subCategoryDetails}
      />
      <MDBCard style={{ padding: "1rem" }}>
        <MDBCardHeader tag="h4" className="font-weight-bold text-uppercase">
          Sub Category
        </MDBCardHeader>
        <div className="d-flex align-items-start flex-column bd-highlight example-parent">
          <MDBBtn onClick={handleModalOpen} color="default">
            Add New
          </MDBBtn>
        </div>
        <hr />
        <MDBCardBody>
          <MDBDataTable
            fixed={true}
            small
            bordered
            hover
            order={["name", "desc"]}
            disableRetreatAfterSorting={true}
            sortable={true}
            responsive={true}
            searchLabel={"Search..."}
            responsiveLg={true}
            responsiveXl={true}
            noBottomColumns={true}
            data={{
              rows: renderData(subCategories),
              columns: columns()
            }}
          />
        </MDBCardBody>
      </MDBCard>
    </Fragment>
  );
};

const mapStateToProps = state => ({
  main_categories: state.main_categories.items,
  sub_categories: state.sub_categories.items
});

export default connect(mapStateToProps)(SubCategories);
