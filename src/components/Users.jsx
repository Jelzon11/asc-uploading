import "../css/datatables.css";
import "../css/bs4.css";
import React, { useEffect, useState, Fragment } from "react";
import { Link } from 'react-router-dom';
import Registration from "./Registration";
import UserEdit from './UserEdit';
import UserDelete from "./UserDelete";
import {
  MDBDataTable,
  MDBCard,
  MDBCardHeader,
  MDBCardBody,
  MDBBtn,
  MDBIcon
} from "mdbreact";
import { connect } from "react-redux";
import moment from "moment";
const _userPageId = 'f83a4623-a734-427f-9e72-159a7d1a13fd';

const columns = () => {
  return [
    {
      label: "First Name",
      field: "firstName",
      sort: "asc",
      width: 500
    },
    {
      label: "Last Name",
      field: "lastName",
      sort: "asc",
      width: 500
    },
    {
      label: "Email",
      field: "email",
      sort: "asc",
      width: 500
    },
    {
      label: "Role",
      field: "role",
      sort: "asc",
      width: 500
    },
    {
      label: "Created Date",
      field: "createdTimestamp",
      sort: "asc",
      width: 100
    },
    {
      label: "Action",
      field: "action",
      sort: "asc",
      width: 100
    }
  ];
};
const Users = props => {
  const [userList, setUserList] = useState([]);
  const [isModalOpen, setModalOpen] = useState(false);
  const [isEditModalOpen, setEditModalOpen] = useState(false);
  const [isDeleteModalOpen, setDeleteModalOpen] = useState(false);
  const [isCreatedSuccess, setCreatedSuccess] = useState(false);
  const [userDetails, setUserDetails] = useState([]);

  const handleModalOpen = () => {
    if (isModalOpen == false) {
      setModalOpen(true);
    } else {
      setModalOpen(false);
    }
  };
  const handleDeleteModalOpen = user => {
    if (isDeleteModalOpen == false) {
      setDeleteModalOpen(true);
      setUserDetails(user);
    } else {
      setDeleteModalOpen(false);
    }
  };

  const handleEditModalOpen = user => {
    if (isEditModalOpen == false) {
      setEditModalOpen(true);
      setUserDetails(user);
    } else {
      setEditModalOpen(false);
    }
  };

  const successToastHandler = () => {
    setCreatedSuccess(true);
  };

  useEffect(() => {
    setUserList(props.users);
    
  }, [props.users]);

  const renderData = userData => {
    const newUserData = userData.map(user => {
      return {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        role: user.roleDetails.name,
        updatedTimestamp: moment.unix(user["updatedTimestamp"]).calendar(),
        createdTimestamp: moment.unix(user["createdTimestamp"]).format("L"),
        action: (
          <Fragment>
            <center>
            <Link to={{pathname: '/userLogs/' + user.id}}>
                <MDBBtn  color="light-green" size="sm">
                <MDBIcon icon="eye" size="1x" className="eye-button"/>
                </MDBBtn>
              </Link>
              <MDBBtn id={user.id} color="light-blue" size="sm" onClick={() => handleEditModalOpen(user)}>
                <MDBIcon icon="pencil-alt" />
              </MDBBtn>
              <MDBBtn id={user.id} color="blue-grey" size="sm" onClick={() => handleDeleteModalOpen(user)}>
              <MDBIcon icon="trash-alt" />
              </MDBBtn>
            </center>
          </Fragment>
        )
      };
    });
    return newUserData;
  };
  return (
    <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="col-md-12">
            <br />
            <div>
              <Registration
                isModalOpen={isModalOpen}
                successToastHandler={successToastHandler}
                handleModalOpen={handleModalOpen}
                userList={userList}
              />
              <UserEdit
                isEditModalOpen={isEditModalOpen}
                handleEditModalOpen={handleEditModalOpen}
                userDetails={userDetails}
              />
              <UserDelete
                isDeleteModalOpen={isDeleteModalOpen}
                handleDeleteModalOpen={handleDeleteModalOpen}
                userDetails={userDetails}
              />
              <MDBCard style={{ padding: "1rem" }}>
                <MDBCardHeader
                  tag="h4"
                  className="font-weight-bold text-uppercase"
                >
                  Users
                </MDBCardHeader>
                <div className="d-flex align-items-start flex-column bd-highlight example-parent">
                  <MDBBtn onClick={handleModalOpen} color="default">
                    Add User
                  </MDBBtn>
                </div>
                <hr />

                <MDBCardBody>
                  <MDBDataTable
                    fixed={true}
                    small
                    bordered
                    hover
                    order={["name", "desc"]}
                    disableRetreatAfterSorting={true}
                    sortable={true}
                    responsive={true}
            searchLabel={"Search..."}
            responsiveSm={true}
            responsiveMd={true}
            responsiveLg={true}
            responsiveXl={true}
                    noBottomColumns={true}
                    data={{
                      rows: renderData(userList),
                      columns: columns()
                    }}
                  />
                </MDBCardBody>
              </MDBCard>
            </div>
            <br />
          </div>
        </div>
      </div>
    </div>
  );
};
const mapStateToProps = state => ({
  users: state.users.userItems
});

export default connect(mapStateToProps)(Users);
