import React, {useEffect} from 'react';
import { connect } from 'react-redux';

const Company_List = props => {
    const company_list = props.companies.map(company => (
        <div key={company.id}>
            <h3>{company.name}</h3>
        </div>
    ));
    return (
        <div>
            {company_list}
        </div>
    )
}
const mapStateToProps = state => ({
   companies: state.companies.companyItems
})

export default connect(mapStateToProps, { })(Company_List)
