import "../css/datatables.css";
import "../css/bs4.css";
import React, { useEffect, useState, Fragment } from "react";
import ProductAdd from "./ProductAdd";
import ProductEdit from './ProductEdit';
import ProductDelete from "./ProductDelete";
import {
  MDBDataTable,
  MDBCard,
  MDBCardHeader,
  MDBCardBody,
  MDBBtn,
  MDBIcon
} from "mdbreact";
import { connect } from "react-redux";
import moment from "moment";
import { isAuthorize } from '../utils';
//const _userPageId = 'f83a4623-a734-427f-9e72-159a7d1a13fd';

const _productDeleteButton = '46fed3f9-4a86-439b-b369-98f910b640ef';
const _productEditButton = '109d70a8-02bb-4749-b694-ee0ee9765264';

const columns = () => {
  return [
    {
      label: "Product Name",
      field: "name",
      sort: "asc",
      width: 500
    },
    {
      label: "Company",
      field: "company",
      sort: "asc",
      width: 500
    },
    {
      label: "Brand",
      field: "brand",
      sort: "asc",
      width: 500
    },
    {
      label: "Main Category",
      field: "mainCategory",
      sort: "asc",
      width: 500
    },
    {
      label: "Sub Category",
      field: "subCategory",
      sort: "asc",
      width: 100
    },
    {
      label: "Sub Sub Category",
      field: "subSubCategory",
      sort: "asc",
      width: 100
    }
  ];
};
const Products = props => {
  const [productList, setProductList] = useState([]);
  const [isModalOpen, setModalOpen] = useState(false);
  const [isEditModalOpen, setEditModalOpen] = useState(false);
  const [isDeleteModalOpen, setDeleteModalOpen] = useState(false);
  const [isCreatedSuccess, setCreatedSuccess] = useState(false);
  const [productDetails, setProductDetails] = useState([]);
  const setUpColum = columns();

  const isDeleteButtonAllow = isAuthorize(_productDeleteButton); 
  const isEditButtonAllow = isAuthorize(_productEditButton); 
  (isDeleteButtonAllow || isEditButtonAllow) && setUpColum.push({
    label: "Action",
    field: "action",
    sort: "asc",
    width: 100
  })

  const handleModalOpen = () => {
    if (isModalOpen == false) {
      setModalOpen(true);
    } else {
      setModalOpen(false);
    }
  };
  const handleDeleteModalOpen = product => {
    if (isDeleteModalOpen == false) {
      setDeleteModalOpen(true);
      setProductDetails(product);
    } else {
      setDeleteModalOpen(false);
    }
  };

  const handleEditModalOpen = product => {
    if (isEditModalOpen == false) {
      setEditModalOpen(true);
      setProductDetails(product);
    } else {
      setEditModalOpen(false);
    }
  };

  const successToastHandler = () => {
    setCreatedSuccess(true);
  };

  useEffect(() => {
    setProductList(props.products);
  }, [props.products]);

  const renderData = productData => {
    const newProductData = productData.map(product => {
      return {
        name: product.name,
        company: product.companyDetails.name,
        brand: product.brandDetails.name,
        mainCategory: product.mainCategoryDetails.name,
        subCategory: product.subCategoryDetails.name,
        subSubCategory: product.subSubCategoryDetails.name,
        updatedTimestamp: moment.unix(product["updatedTimestamp"]).calendar(),
        createdTimestamp: moment.unix(product["createdTimestamp"]).format("L"),
        action: (
          <Fragment>
            <center>
              {isDeleteButtonAllow && <MDBBtn id={product.id} color="light-blue" size="sm" onClick={() => handleEditModalOpen(product)}>
                <MDBIcon icon="pencil-alt" />
              </MDBBtn>
              }
              {isEditButtonAllow && <MDBBtn id={product.id} color="blue-grey" size="sm" onClick={() => handleDeleteModalOpen(product)}>
                <MDBIcon icon="trash-alt" />
              </MDBBtn>
              }
            </center>
          </Fragment>
        )
      };
    });
    return newProductData;
  };
  return (
    <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="col-md-12">
            <br />
            <div>
              <ProductAdd
                isModalOpen={isModalOpen}
                successToastHandler={successToastHandler}
                handleModalOpen={handleModalOpen}
                productList={productList}
              />
              <ProductEdit
                isEditModalOpen={isEditModalOpen}
                handleEditModalOpen={handleEditModalOpen}
                productDetails={productDetails}
              />
              <ProductDelete
                isDeleteModalOpen={isDeleteModalOpen}
                handleDeleteModalOpen={handleDeleteModalOpen}
                productDetails={productDetails}
              />
              <MDBCard style={{ padding: "1rem" }}>
                <MDBCardHeader
                  tag="h4"
                  className="font-weight-bold text-uppercase"
                >
                  Products
                </MDBCardHeader>
                <div className="d-flex align-items-start flex-column bd-highlight example-parent">
                  <MDBBtn onClick={handleModalOpen} color="default">
                    Add product
                  </MDBBtn>
                </div>
                <hr />

                <MDBCardBody>
                  <MDBDataTable
                    fixed={true}
                    small
                    bordered
                    hover
                    order={["name", "desc"]}
                    disableRetreatAfterSorting={true}
                    sortable={true}
                    responsive={true}
                    searchLabel={"Search..."}
                    responsiveSm={true}
                    responsiveMd={true}
                    responsiveLg={true}
                    responsiveXl={true}
                    noBottomColumns={true}
                    data={{
                      rows: renderData(productList),
                      columns: setUpColum
                    }}
                  />
                </MDBCardBody>
              </MDBCard>
            </div>
            <br />
          </div>
        </div>
      </div>
    </div>
  );
};
const mapStateToProps = state => ({
  products: state.products.productItems
});

export default connect(mapStateToProps)(Products);