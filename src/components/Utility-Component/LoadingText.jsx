import React, { Fragment } from 'react';

export default (props) => {
  return props.loading !== true ? (
    <Fragment>
      {props.text}
    </Fragment>
  ) : (
    <Fragment>
      <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
      {props.loadingText ? props.loadingText : 'Loading..'}
    </Fragment>
  )
}