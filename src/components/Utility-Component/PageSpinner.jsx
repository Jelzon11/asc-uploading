import React from 'react'

export default () => {
  return (
    <div style={{ 
      width: '100%', 
      height: Math.max(document.documentElement.clientHeight, window.innerHeight || 1000),
      display: 'table'
    }}>
      <div style={{ 
        display: 'table-cell',
        verticalAlign: 'middle',
        textAlign:'center'
      }}>
        <div class="spinner-border slow" style={{
            height: '150px', 
            width: '150px',
          }} role="status">
          <span class="sr-only" style={{width:'1rem'}}>Loading...</span>
        </div>
      </div> 
    </div>  
  )
}