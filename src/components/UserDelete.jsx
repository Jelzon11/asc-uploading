import React, { useState, useEffect } from "react";
import { LoadingText } from "./Utility-Component";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { deleteUser } from "../actions/userAction";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol,
} from "mdbreact";

const UserDelete = props => {
  const [userId, setUserId] = useState("");
  const [inProcess, setInProcess] = useState(false);

  
  useEffect(() => {
    setUserId(props.userDetails.id);
  }, [props.userDetails]);

  const handleSubmit = e => {
    e.preventDefault();

    if (!userId) return false;
    setInProcess(true);
    props.deleteUser(
      {
        id: userId
      },
      () => {
        setInProcess(false);
        toastr.success("Brand Successfully Deleted");
        closeSelfHandler();
      }
    );
  };

  const closeSelfHandler = () => {
    props.handleDeleteModalOpen();
  };

  return (
    <MDBModal
      isOpen={props.isDeleteModalOpen}
      toggle={props.handleDeleteModalOpen}
      full-height
      position="top"
    >
      <MDBModalHeader
        toggle={closeSelfHandler}
        tag="h4"
        className="font-weight-bold text-uppercase"
      >
        {props.userDetails.firstName + " "+ props.userDetails.lastName}
      </MDBModalHeader>
      <form className="needs-validation" onSubmit={handleSubmit} noValidate>
        <MDBModalBody>
          <MDBRow>
            <MDBCol>
              <p>
                Are you sure you want to delete{" "}
                <font style={{ fontWeight: "900", color: "red" }}>
                  {" "}
                  {props.userDetails.firstName + " "+ props.userDetails.lastName}
                </font>
                ?
              </p>
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
            No
          </MDBBtn>
          <MDBBtn color="light-green" type="submit">
            <LoadingText text={"Yes"} loading={inProcess} />
          </MDBBtn>
        </MDBModalFooter>
      </form>
    </MDBModal>
  );
};
UserDelete.propTypes = {
  deleteUser: PropTypes.func.isRequired
};
export default connect(null, { deleteUser })(UserDelete);
