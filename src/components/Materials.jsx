import React, { useState } from "react";
import MaterialRadio from './materialRadio';
import MaterialOnline from './materialOnline';
import {Tab, Tabs} from 'react-bootstrap';
import { isAuthorize } from '../utils';
import UnAuthorize from './UnAuthorize';
const _materialPageId = '81cb6447-4aab-40c6-a0f6-7561046fded5';

const Materials = (props) => {
  const [key, setKey] = useState("radio");

  return isAuthorize(_materialPageId) ? (
    <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="col-md-12" style={{ padding: "1rem" }}>
            <Tabs
              id="controlled-tab-example1"
              activeKey={key}
              onSelect={k => setKey(k)}
            >
              <Tab eventKey="radio" title="Radio">
                <MaterialRadio setKey={setKey} />
              </Tab>
              <Tab eventKey="online" title="Online">
                <MaterialOnline setKey={setKey} />
              </Tab>
            </Tabs>
          </div>
        </div>
      </div>
    </div>
  ) : <UnAuthorize />
};
export default Materials;
