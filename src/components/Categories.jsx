import React, { useState } from 'react';
import { Tab, Tabs } from 'react-bootstrap';
import CategoriesMain from './Categories-main';
import SubCategories from './Categories-sub';
import SubSubCategories from './Categories-sub-sub';
import { isAuthorize } from '../utils';
import UnAuthorize from './UnAuthorize';
const _categoryPageId = '17f9d564-35f9-4bd5-9021-f66b01e2673e';

const Categories = (props) => {
  const [key, setKey] = useState('main');
  
  return (
    isAuthorize(_categoryPageId) ? 
    <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="col-md-12" style={{padding: '1rem'}}>
            <Tabs 
              id="controlled-tab-example" 
              activeKey={key} 
              onSelect={k => setKey(k)}
            >
              <Tab eventKey="main" title="Main Category">
                <CategoriesMain setKey={setKey} />
              </Tab>
              <Tab eventKey="sub" title="Sub Category">
                <SubCategories setKey={setKey} />
              </Tab>
              <Tab eventKey="sub-sub" title="Sub-Sub Category">
                <SubSubCategories setKey={setKey} />
              </Tab>
            </Tabs>
          </div>
        </div>
      </div>
    </div> : 
    (<UnAuthorize />)
  );
}

export default Categories;

