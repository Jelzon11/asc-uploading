import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Select, LoadingText } from './Utility-Component';
import { createSubCategory } from '../actions/subCategoryAction';
import { setToInvalid, setToValid } from '../utils';
import toastr from 'toastr';
import { 
  MDBModal, MDBModalHeader, MDBModalBody, MDBBtn, MDBModalFooter,
  MDBRow, MDBCol
} from 'mdbreact';

const SubCategoryModalForm = (props) => {
  const [nameInput, setName] = useState();
  const [mainCategoryList, setMainCategoryList] = useState([]);
  const [selectedMainCategoryId, setSelectedMainCategory] = useState();
  const [inProcess, setInProcess] = useState(false);

  const handleNameChange = (e) => {
    setName(e.target.value);
    setToValid(e.target);
    (e.target.value)
     ? setToValid(e.target) 
     : setToInvalid(e.target);
  }

  const handleSetSelectedMainCategory = (e) => {
    setSelectedMainCategory(e.target.value);
    (e.target.value != 0) 
     ? setToValid(e.target) 
     : setToInvalid(e.target);
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    if(!selectedMainCategoryId) setToInvalid(e.target.elements['mainCategorySelect']);
    if(!nameInput) setToInvalid(e.target.elements['nameInput']);
    if(!selectedMainCategoryId || !nameInput) return false;
    setInProcess(true);
    props.createSubCategory({ 
      name : nameInput,
      mainCategoryId: selectedMainCategoryId
    },() => {
      setInProcess(false);
      toastr.success('Sub Category Successfully Added');
      closeSelfHandler();
    });
    
  }

  const closeSelfHandler = () => {
    props.handleModalOpen();
    setSelectedMainCategory(0);
    setName('')
  }
  useEffect(() => {
    setMainCategoryList(props.main_categories)
  }, [props.main_categories])

  return (
    <MDBModal
      isOpen={props.isModalOpen} 
      toggle={props.handleModalOpen}  
      full-height  
      position="top"
    >
      <MDBModalHeader toggle={closeSelfHandler}>Add New</MDBModalHeader>
        <form 
          className="needs-validation"
          onSubmit={handleSubmit}
          noValidate
        >
          <MDBModalBody>
            <MDBRow style={{marginBottom: '1rem'}}>
              <MDBCol>
                <label  > Select Main Category </label>
                <Select 
                  items={mainCategoryList} 
                  onChange={e => handleSetSelectedMainCategory(e)}
                  name={'mainCategorySelect'}
                />
              </MDBCol>
            </MDBRow>
            <MDBRow>
              <MDBCol>
                <label htmlFor="nameInput"  >
                  Sub Category Name
                </label>
                <input
                  name={'nameInput'}
                  className="form-control"
                  onChange={e => handleNameChange(e)}
                  id={'subcategory-name-input'}
                  required
                />
                <div className="invalid-feedback">
                  Please provide Sub-Category Name
                </div>
              </MDBCol>
            </MDBRow>
          </MDBModalBody>
          <MDBModalFooter>
          <MDBBtn color="blue-grey" onClick={closeSelfHandler}>Cancel</MDBBtn>
            <MDBBtn color="light-green" type="submit">
              <LoadingText
              text={'Submit'}
              loading={inProcess}
             />
            </MDBBtn>
          </MDBModalFooter>
        </form>
    </MDBModal>
  );
}

SubCategoryModalForm.propTypes = {
  main_categories: PropTypes.array.isRequired,
}

const mapStateToProps = (state) => ({
  main_categories: state.main_categories.items,
})

export default connect(mapStateToProps, { createSubCategory })(SubCategoryModalForm);
