import React, { useState, useEffect } from "react";
import { LoadingText } from "./Utility-Component";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { deleteProduct } from "../actions/productAction";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol,
} from "mdbreact";

const ProductDelete = props => {
  const [productId, setProductId] = useState("");
  const [inProcess, setInProcess] = useState(false);

  
  useEffect(() => {
    setProductId(props.productDetails.id);
  }, [props.productDetails]);

  const handleSubmit = e => {
    e.preventDefault();

    if (!productId) return false;
    setInProcess(true);
    props.deleteProduct(
      {
        id: productId
      },
      () => {
        setInProcess(false);
        toastr.success("Product Successfully Deleted");
        closeSelfHandler();
      }
    );
  };

  const closeSelfHandler = () => {
    props.handleDeleteModalOpen();
  };

  return (
    <MDBModal
      isOpen={props.isDeleteModalOpen}
      toggle={props.handleDeleteModalOpen}
      full-height
      position="top"
    >
      <MDBModalHeader
        toggle={closeSelfHandler}
        tag="h4"
        className="font-weight-bold text-uppercase"
      >
        {props.productDetails.name}
      </MDBModalHeader>
      <form className="needs-validation" onSubmit={handleSubmit} noValidate>
        <MDBModalBody>
          <MDBRow>
            <MDBCol>
              <p>
                Are you sure you want to delete{" "}
                <font style={{ fontWeight: "900", color: "red" }}>
                  {" "}
                  {props.productDetails.name}
                </font>
                ?
              </p>
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
            No
          </MDBBtn>
          <MDBBtn color="light-green" type="submit">
            <LoadingText text={"Yes"} loading={inProcess} />
          </MDBBtn>
        </MDBModalFooter>
      </form>
    </MDBModal>
  );
};
ProductDelete.propTypes = {
  deleteProduct: PropTypes.func.isRequired
};
export default connect(null, { deleteProduct })(ProductDelete);
