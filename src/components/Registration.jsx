import React, { useState, useEffect } from "react";
import "react-datepicker/dist/react-datepicker.css";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Select, LoadingText } from "./Utility-Component";
import { createUser } from "../actions/userAction";
import { fetchUserEmails } from "../actions/userAction";
import { setToInvalid, setToValid } from "../utils";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol
} from "mdbreact";

const Registration = props => {
  const [lastName, setLastName] = useState("");
  const [firstName, setFirstName] = useState("");
  const [middleName, setMiddleName] = useState("");
  const [contact, setContact] = useState("");
  const [email, setEmail] = useState("");
  const [inProcess, setInProcess] = useState(false);
  const [selectedRoleId, setSelectedRoleId] = useState();
  const [roleList, setRoleList] = useState([]);
  const [checkEmail, setCheckEmail] = useState([]);
  const [password, setPassword] = useState([]);
  const [passwordModal, setPasswordModal] = useState(false);

  const toggle = () => {
    setPasswordModal(!passwordModal);
  };
  const handleLastNameChange = e => {
    setLastName(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };
  const handleFirstNameChange = e => {
    setFirstName(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };
  const handleMiddleNameChange = e => {
    setMiddleName(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };
  const handleContactChange = e => {
    setContact(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };
  

  const handleEmailBlur = e => {
     //setEmail(e.target.value);
     setToValid(e.target);
     e.target.value ? setToValid(e.target) : setToInvalid(e.target);
      const getEmail = e.target.value;
     props.fetchUserEmails(
      {
        email: e.target.value
      },
      res => {
        if(res === 'exist') {
          toastr.error("Email already exist");
        }else {
          setEmail(getEmail);
        }
      }
    );
  }

  const handleRole = e => {
    setSelectedRoleId(e.target.value);
    e.target.value != 0 ? setToValid(e.target) : setToInvalid(e.target);
  };

  const closeSelfHandler = () => {
    props.handleModalOpen();
    setLastName("");
    setFirstName("");
    setMiddleName("");
    setContact("");
    setEmail("");
  };

  const handleSubmit = e => {
    e.preventDefault();
    if (!lastName) setToInvalid(e.target.elements["lastName"]);
    if (!firstName) setToInvalid(e.target.elements["firstName"]);
    if (!middleName) setToInvalid(e.target.elements["middleName"]);
    if (!contact) setToInvalid(e.target.elements["contact"]);
    if (!email) setToInvalid(e.target.elements["email"]);
    if (!selectedRoleId) setToInvalid(e.target.elements["roleSelect"]);
    if (
      !lastName ||
      !firstName ||
      !middleName ||
      !contact ||
      !email ||
      !selectedRoleId
    )
      return false;
    setInProcess(true);
    props.createUser(
      {
        lastName: lastName,
        firstName: firstName,
        middleName: middleName,
        contact: contact,
        email: email,
        roleId: selectedRoleId
      },
      user => {
        //console.log(user)
        setInProcess(false);
        toastr.success("User Successfully Added");
        closeSelfHandler();
        setPassword(user.password);
        toggle();
      }
    );
  };

  useEffect(() => {
    setRoleList(props.roles);
    setCheckEmail(props.userList);
  }, [props.roles]);

  return (
    <div>
      <MDBModal isOpen={passwordModal} toggle={toggle}>
        <MDBModalHeader toggle={toggle}>Password</MDBModalHeader>
        <MDBModalBody>
          <p>
            <font color="red">*Note: Update the password when logged-in </font>
          </p>
          Your password is: <h1>{password}</h1>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="secondary" onClick={toggle}>
            Okay
          </MDBBtn>
        </MDBModalFooter>
      </MDBModal>
      <MDBModal
        isOpen={props.isModalOpen}
        toggle={props.handleModalOpen}
        full-height
        position="top"
      >
        <MDBModalHeader toggle={closeSelfHandler}>Add New</MDBModalHeader>
        <form className="needs-validation" onSubmit={handleSubmit} noValidate>
          <MDBModalBody>
            <MDBRow style={{ marginBottom: "1rem" }}>
              <MDBCol>
                <label htmlFor="nameInput">Last Name</label>
                <input
                  name={"lastName"}
                  className="form-control"
                  onChange={e => handleLastNameChange(e)}
                  required
                />
                <div className="invalid-feedback">Please provide last name</div>
              </MDBCol>
            </MDBRow>
            <MDBRow style={{ marginBottom: "1rem" }}>
              <MDBCol>
                <label htmlFor="nameInput">First Name</label>
                <input
                  name={"firstName"}
                  className="form-control"
                  onChange={e => handleFirstNameChange(e)}
                  required
                />
                <div className="invalid-feedback">
                  Please provide first name
                </div>
              </MDBCol>
            </MDBRow>
            <MDBRow style={{ marginBottom: "1rem" }}>
              <MDBCol>
                <label htmlFor="nameInput">Middle Name</label>
                <input
                  name={"middleName"}
                  className="form-control"
                  onChange={e => handleMiddleNameChange(e)}
                  required
                />
                <div className="invalid-feedback">
                  Please provide middle name
                </div>
              </MDBCol>
            </MDBRow>
            <MDBRow style={{ marginBottom: "1rem" }}>
              <MDBCol>
                <label htmlFor="nameInput">Contact</label>
                <input
                  name={"contact"}
                  type="text"
                  className="form-control"
                  onChange={e => handleContactChange(e)}
                  required
                />
                <div className="invalid-feedback">Please provide contact</div>
              </MDBCol>
            </MDBRow>
            <MDBRow style={{ marginBottom: "1rem" }}>
              <MDBCol>
                <label htmlFor="nameInput">Email</label>
                <input
                  name={"email"}
                  type="email"
                  className="form-control"
                  onBlur={e => handleEmailBlur(e)}
                  required
                />
                <div className="invalid-feedback">Please provide email</div>
              </MDBCol>
            </MDBRow>
            <MDBRow style={{ marginBottom: "1rem" }}>
              <MDBCol>
                <label> Select Role </label>
                {
                  <Select
                    items={roleList}
                    onChange={e => handleRole(e)}
                    name={"roleSelect"}
                  />
                }
              </MDBCol>
            </MDBRow>
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
              Cancel
            </MDBBtn>
            <MDBBtn color="light-green" type="submit">
              <LoadingText text={"Submit"} loading={inProcess} />
            </MDBBtn>
          </MDBModalFooter>
        </form>
      </MDBModal>
    </div>
  );
};
Registration.propTypes = {
  createUser: PropTypes.func.isRequired,
  fetchUserEmails: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  roles: state.roles.items
});
export default connect(mapStateToProps, { createUser, fetchUserEmails })(Registration);
