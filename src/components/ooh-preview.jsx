import React, { useState, useEffect } from 'react';
import Dropzone from 'react-dropzone';

 const imageMaxSize = 100000000; //byte
 const acceptedFileTypes = 'image/jpg, image/jpeg';
 const acceptedFileTypesArray = acceptedFileTypes.split(",").map((item) => {return item.trim()});
const Ooh_preview = (props) => {
  


        
        const { src, material } = props;
        //console.log(imgSrc);

        return (

            <div>
              
              

            

                {src != null ?
                
                <div> <Dropzone onDrop={props.handleOnDrop} accept={acceptedFileTypes} multiple={false} maxSize={imageMaxSize}>
                    {({ getRootProps, getInputProps }) => (


                        <div>
                            <section>
                                <div {...getRootProps()} style={{ "width": "100%" }}>
                                    <input {...getInputProps()} />
                                        <p>{material.name}</p>
                                </div>
                            </section>

                        </div>


                    )}
                </Dropzone>
                    <div style={{ "textAlignment": "center", "margin": "5px 15px", "borderLeft": "1px solid gray", "borderRight": "1px solid gray", "borderTop": "5px solid gray", "borderBottom": "5px solid gray" }}>

                    <img src={src} style={{ "width": "100%", "height": "auto" }}/> 
                        

                    </div>
                    </div>: <Dropzone onDrop={props.handleOnDrop} accept={acceptedFileTypes} multiple={false} maxSize={imageMaxSize}>
                    {({ getRootProps, getInputProps }) => (
                         <div style={{ "borderStyle": "dashed", "textAlignment": "center", "margin": "5px 15px" }}>
                         <section>
                             <div {...getRootProps()} style={{ "width": "100%" }}>
                                 <input {...getInputProps()} />
                                    <p style={{ "height": "200px", "width": "100%", "textAlign": "center" }}>Drag 'n' drop an image here, or click to select</p>
                             </div>
                         </section>

                     </div>
                    )}
                </Dropzone>}
                {/* 
                <div style={{ "textAlignment": "center", "margin": "5px 15px", "borderLeft": "1px solid gray", "borderRight": "1px solid gray", "borderTop": "5px solid gray", "borderBottom": "5px solid gray" }}>
                </div> */}
            </div>
        )
    }

export default Ooh_preview;