import React, { useEffect, useState, Fragment } from "react";
import { connect } from "react-redux";
import RadioMaterialDelete from "./radioMaterialDelete";
import { isAuthorize } from '../utils';
import moment from "moment";
import {
  MDBDataTable,
  MDBCard,
  MDBCardHeader,
  MDBCardBody,
  MDBBtn,
  MDBIcon
} from "mdbreact";

  
const columns = () => {
  return [
    {
      label: "Reference Code",
      field: "referenceCode",
      sort: "asc",
      width: 100
    },
    {
      label: "Title",
      field: "title",
      sort: "asc",
      width: 100
    },
    {
      label: "Product",
      field: "product",
      sort: "asc",
      width: 100
    },
    {
      label: "Created Date",
      field: "createdTimestamp",
      sort: "asc",
      width: 100
    },
    {
      label: "Material",
      field: "material",
      sort: "asc",
      width: 100
    }
  ];
};

const _radioMaterialDeleteButton = 'aadfd1f1-35ce-4b66-a5db-82772787a5cc';

const MaterialRadio = props => {
  const [radioList, setRadioList] = useState([]);
  const [isDeleteModalOpen, setDeleteModalOpen] = useState(false);
  const [radioDetails, setRadioDetails] = useState("");
  let setColumns = columns();

  isAuthorize(_radioMaterialDeleteButton) && setColumns.push({
    label: "Action",
    field: "action",
    sort: "asc",
    width: "auto"
  })

  const handleDeleteModalOpen = radio => {
    if (isDeleteModalOpen == false) {
      setDeleteModalOpen(true);
      setRadioDetails(radio);
    } else {
      setDeleteModalOpen(false);
    }
  };
  useEffect(() => {
    setRadioList(props.radioMaterials);
  }, [props.radioMaterials]);

  const renderData = RadioItems => {
    const newRadioList = RadioItems.map(item => {
      return {
        referenceCode: item.referenceId,
        title: item.title,
        product: item.productDetails.name,
        createdTimestamp: moment.unix(item["createdTimestamp"]).format("L"),
        material: (
          <Fragment>
            <audio
              src={item.fileURL}
              controls
              style={{ width: "100%" }}
            ></audio>
          </Fragment>
        ),
        action: isAuthorize(_radioMaterialDeleteButton) && (
          <Fragment>
            <center>
              <MDBBtn id={item.id} color="blue-grey" size="sm"  onClick={() => handleDeleteModalOpen(item)}>
                <MDBIcon icon="trash-alt" />
              </MDBBtn>

              {/* <MDBBtn id={item.id} color="blue-grey" size="sm">
                View
              </MDBBtn> */}
            </center>
          </Fragment>
        )
      };
    });
    return newRadioList;
  };
  return (
    <Fragment>
      <RadioMaterialDelete
        isDeleteModalOpen={isDeleteModalOpen}
        handleDeleteModalOpen={handleDeleteModalOpen}
        radioDetails={radioDetails}
      />
      <MDBCard style={{ padding: "1rem" }}>
        <MDBCardHeader tag="h4" className="font-weight-bold text-uppercase">
          Radio Materials
        </MDBCardHeader>
        <hr />
        <MDBCardBody>
          <MDBDataTable
            fixed={true}
            small
            bordered
            hover
            order={["name", "desc"]}
            disableRetreatAfterSorting={true}
            sortable={true}
            responsive={true}
            searchLabel={"Search..."}
            responsiveSm={true}
            responsiveMd={true}
            responsiveLg={true}
            responsiveXl={true}
            noBottomColumns={true}
            data={{
              rows: renderData(radioList),
              columns: setColumns
            }}
          />
        </MDBCardBody>
      </MDBCard>
    </Fragment>
  );
};
const mapStateToProps = state => ({
  radioMaterials: state.radioMaterials.items
});

export default connect(mapStateToProps, {})(MaterialRadio);
