import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Select, LoadingText } from "./Utility-Component";
import { createProduct } from "../actions/productAction";
import { setToInvalid, setToValid } from "../utils";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol
} from "mdbreact";

const ProductsAdd = props => {
  const [productInput, setProduct] = useState();
  const [inProcess, setInProcess] = useState(false);
  const [subCategoryList, setSubCategoryList] = useState([]);
  const [filteredSubCategoryList, setFilteredSubCategoryList] = useState([]);
  const [mainCategoryList, setMainCategoryList] = useState([]);
  const [subSubCategoryList, setSubSubCategoryList] = useState([]);
  const [filteredSubSubCategoryList, setFilteredSubSubCategoryList] = useState([]);
  const [companyList, setCompanyList] = useState([]);
  const [brandList, setBrandList] = useState([]);


  const [selectedMainCategoryId, setSelectedMainCategory] = useState(0);
  const [selectedSubCategoryId, setSelectedSubCategoryId] = useState(0);
  const [selectedSubSubCategoryId, setSelectedSubSubCategoryId] = useState(0);
  const [selectedCompanyId, setSelectedCompanyId] = useState(0);
  const [selectedBrandId, setSelectedBrandId] = useState(0);
  const [filteredBrandList, setFilteredBrandList] = useState([]);

  const [selectedBrand, setSelectedBrand] = useState("");

  const handleProductChange = e => {
    setProduct(e.target.value);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };

  const selectMainCategoryHandler = e => {
    setSelectedMainCategory(e.target.value);
    e.target.value != 0 ? setToValid(e.target) : setToInvalid(e.target);
    setFilteredSubCategoryList(subCategoryList.filter(item => item.mainCategoryId == e.target.value)); 
    setFilteredSubSubCategoryList([]);
    setSelectedSubSubCategoryId(0);
    setSelectedSubCategoryId(0);
  };

  const selectSubCategoryHandler = e => {
    setSelectedSubCategoryId(e.target.value);
    e.target.value != 0 ? setToValid(e.target) : setToInvalid(e.target);
    setFilteredSubSubCategoryList(
      subSubCategoryList.filter(item => item.subCategoryId == e.target.value)
    );
    setSelectedSubSubCategoryId(0);
  };

  const selectCompanyHandler = e => {
    setSelectedCompanyId(e.target.value);
    e.target.value != 0 ? setToValid(e.target) : setToInvalid(e.target);
    setFilteredBrandList(
      brandList.filter(item => item.companyId == e.target.value)
    );
  };

  const selectBrandHandler = e => {
    setSelectedBrandId(e.target.value);
    const filteredSelectedItem = filteredBrandList.filter(item => item.id == e.target.value);
    setSelectedBrand( filteredSelectedItem[0]);
      setProduct(filteredSelectedItem[0] && filteredSelectedItem[0].name)
    e.target.value != 0 ? setToValid(e.target) : setToInvalid(e.target);
    console.log('from brand Handler' + filteredBrandList)
  };

  const selectSubSubCategoryHandler = e => {
    setSelectedSubSubCategoryId(e.target.value);
    e.target.value != 0 ? setToValid(e.target) : setToInvalid(e.target);
  };

  const closeSelfHandler = () => {
    
    setSelectedMainCategory(0);
    setSelectedSubCategoryId(0);
    setSelectedSubSubCategoryId(0);
    setSelectedCompanyId(0);
    setSelectedBrandId(0);
    setProduct("");

    setFilteredSubCategoryList([]);
    setFilteredSubSubCategoryList([]);
    setFilteredBrandList([]);

    props.handleModalOpen();
  };

  const handleSubmit = e => {
    e.preventDefault();
    if (!productInput) setToInvalid(e.target.elements["productInput"]);
    if (selectedMainCategoryId <= 0) setToInvalid(e.target.elements["mainCategorySelect"]);
    if (selectedSubCategoryId <= 0) setToInvalid(e.target.elements["subCategorySelect"]);
    if (selectedSubSubCategoryId <= 0) setToInvalid(e.target.elements["subSubCategorySelect"]);
    if (selectedCompanyId <= 0) setToInvalid(e.target.elements["companySelect"]);
    if (selectedBrandId <= 0) setToInvalid(e.target.elements["brandSelect"]);
    if (
      !productInput ||
      selectedMainCategoryId <= 0 ||
      selectedSubCategoryId <= 0 ||
      selectedSubSubCategoryId <= 0 ||
      selectedCompanyId <= 0 ||
      selectedBrandId <= 0
    ) return false;
      
    setInProcess(true);
    props.createProduct({
      name: productInput,
      mainCategoryId: selectedMainCategoryId,
      subCategoryId: selectedSubCategoryId,
      subSubCategoryId: selectedSubSubCategoryId,
      companyId: selectedCompanyId,
      brandId: selectedBrandId
    }, () => {
      setInProcess(false);
      toastr.success("Product Successfully Added");
      closeSelfHandler();
    });
  };

  useEffect(() => {
    setMainCategoryList(props.main_categories);
    setSubCategoryList(props.sub_categories);
    setSubSubCategoryList(props.sub_sub_categories);
    setCompanyList(props.companies);
    setBrandList(props.brands);
  }, [
    props.main_categories,
    props.sub_categories,
    props.sub_sub_categories,
    props.companies,
    props.brands
  ]);

  return (
    <MDBModal
      isOpen={props.isModalOpen}
      toggle={props.handleModalOpen}
      full-height
      position="top"
    >
      <MDBModalHeader toggle={closeSelfHandler}>Add New</MDBModalHeader>
      <form className="needs-validation" onSubmit={handleSubmit} noValidate>
        <MDBModalBody>
          <MDBRow style={{ marginBottom: "1rem" }}>
            <MDBCol>
              <label> Company </label>
              <Select
                items={companyList}
                onChange={e => selectCompanyHandler(e)}
                name={"companySelect"}
              />
            </MDBCol>
          </MDBRow>
          <MDBRow style={{ marginBottom: "1rem" }}>
            <MDBCol>
              <label> Brand </label>
              <Select
                disabled={selectedCompanyId == 0 && true}
                items={filteredBrandList}
                onChange={e => selectBrandHandler(e)}
                name={"brandSelect"}
              />
            </MDBCol>
          </MDBRow>
          <MDBRow style={{ marginBottom: "1rem" }}>
            <MDBCol>
              <label htmlFor="mainCategorySelect">Select Main Category </label>
              <Select
                items={mainCategoryList}
                onChange={e => selectMainCategoryHandler(e)}
                name={"mainCategorySelect"}
              />
              <div className="invalid-feedback">
                Please Select Main Category
              </div>
            </MDBCol>
          </MDBRow>
          <MDBRow style={{ marginBottom: "1rem" }}>
            <MDBCol>
              <label> Select Sub Category </label>
              <Select
                disabled={selectedMainCategoryId == 0 && true}
                items={filteredSubCategoryList}
                onChange={e => selectSubCategoryHandler(e)}
                name={"subCategorySelect"}
              />
              <div className="invalid-feedback">Please Select Sub Category</div>
            </MDBCol>
          </MDBRow>
          <MDBRow style={{ marginBottom: "1rem" }}>
            <MDBCol>
              <label> Select Sub Sub Category </label>
              <Select
                disabled={selectedSubCategoryId == 0 && true}
                items={filteredSubSubCategoryList}
                onChange={e => selectSubSubCategoryHandler(e)}
                name={"subSubCategorySelect"}
              />
              <div className="invalid-feedback">
                Please Select Sub Sub Category
              </div>
            </MDBCol>
          </MDBRow>
          <MDBRow style={{ marginBottom: "1rem" }}>
            <MDBCol>
              <label htmlFor="productInput">Product</label>
              <input
                name={"productInput"}
                className="form-control"
                value={productInput}
                onChange={e => handleProductChange(e)}
                required
              />
              <div className="invalid-feedback">
                Please provide product name
              </div>
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
            Cancel
          </MDBBtn>
          <MDBBtn color="light-green" type="submit">
            <LoadingText
              text={"Submit"}
              loading={inProcess}
            />
          </MDBBtn>
        </MDBModalFooter>
      </form>
    </MDBModal>
  );
};
const mapStateToProps = state => ({
  main_categories: state.main_categories.items,
  sub_categories: state.sub_categories.items,
  sub_sub_categories: state.sub_sub_categories.items,
  companies: state.companies.companyItems,
  brands: state.brands.brandItems
});

ProductsAdd.propTypes = {
  createProduct: PropTypes.func.isRequired
};

export default connect(mapStateToProps, { createProduct })(ProductsAdd);
