import React from "react";
import { useHistory } from "react-router-dom";
import { LogoutUser } from "../actions/userAction";
import { removeToken } from "../utils";

export default () => {
  const history = useHistory();

  // const unmount = () => {
  //   //let mountNode = React.findDOMNode(this.refs.wassup);
  //   React.unmountComponentAtNode(document.getElementById('root'));
  // }

  const logoutOnclickHandler = () => {
    LogoutUser(async () => {
      await removeToken();
      //unmount();
      window.location = "/";
      // history.push("/");
    });
  };

  return (
    <nav className="main-header navbar navbar-expand navbar-danger navbar-light">
      <ul className="navbar-nav">
        <li className="nav-item">
          <a className="nav-link" data-widget="pushmenu" href="#">
            <i style={{ color: "#f2f2f2" }} className="fas fa-bars fa-lg"></i>
          </a>
        </li>
      </ul>
      <ul className="navbar-nav ml-auto">
      <a onClick={() => logoutOnclickHandler()} className="nav-link" href="#">
            <i style={{ color: "#f2f2f2" }} className="fas fa-power-off fa-lg"></i>
          </a>
        {/* <buttton
          // onClick={() => logoutOnclickHandler()}
          // style={{
          //   backgroundColor: "#fff",
          //   borderRadius: "4px",
          //   color: "000",
          //   textAlign: "center",
          //   textDecoration: "none",
          //   display: "inline-block",
          //   fontSize: "16px",
          //   margin: "4px 2px",
          //   cursor: "pointer",
          //   padding: "4px 5px"
          // }}
        >
          {" "}
          Logout
        </buttton> */}
      </ul>
    </nav>
  );
};
