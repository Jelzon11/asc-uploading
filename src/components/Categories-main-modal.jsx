import React, { useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { LoadingText } from './Utility-Component';
import { 
  MDBModal, MDBModalHeader, MDBModalBody, MDBBtn, MDBModalFooter,
  MDBRow, MDBCol
} from 'mdbreact';
import { setToInvalid, setToValid } from '../utils';
import toastr from 'toastr';
import { createMainCategory } from '../actions/mainCategoryAction';

const CategoriesMainForm = (props) => {
  const [nameInput, setName] = useState();
  const [inProcess, setInProcess] = useState(false);

  const handleNameChange = (e) => {
    setName(e.target.value);
    setToValid(e.target);
    (e.target.value)
     ? setToValid(e.target) 
     : setToInvalid(e.target);
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    if(!nameInput) setToInvalid(e.target.elements['nameInput']);
    setInProcess(true);
    props.createMainCategory({ 
      name : nameInput
    },() => {
      setInProcess(false);
      toastr.success('Main Category Successfully Added');
      closeSelfHandler();
    });
  }

  const closeSelfHandler = () => {
    props.handleModalOpen();
    setName('')
  }
  return (
    <MDBModal
      isOpen={props.isModalOpen} 
      toggle={props.handleModalOpen}  
      full-height  
      position="top"
    >
      <MDBModalHeader toggle={closeSelfHandler}>Add New</MDBModalHeader>
      <form 
          className="needs-validation"
          onSubmit={handleSubmit}
          noValidate
        >
          <MDBModalBody>
            <MDBRow>
              <MDBCol>
                <label htmlFor="defaultFormRegisterEmailEx"  >
                  Main Category Name
                </label>
                <input
                  name={'nameInput'}
                  className="form-control"
                  onChange={e => handleNameChange(e)}
                  required
                />
                <div className="invalid-feedback">
                  Please provide a valid category.
                </div>
              </MDBCol>
            </MDBRow>
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn color="blue-grey" onClick={closeSelfHandler}>Cancel</MDBBtn>
            <MDBBtn color="light-green" type="submit">
              <LoadingText
                text={'Submit'}
                loading={inProcess}
              />
             </MDBBtn>
          </MDBModalFooter>
        </form>
    </MDBModal>
  );
}

CategoriesMainForm.propTypes = {
  createMainCategory: PropTypes.func.isRequired
};

export default connect(null, { createMainCategory })(CategoriesMainForm);
