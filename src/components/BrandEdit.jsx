import React, { useState, Fragment, useEffect } from "react";
import { setToInvalid, setToValid } from "../utils";
import { LoadingText } from "./Utility-Component";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { updateBrand } from "../actions/brandAction";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol
} from "mdbreact";

const BrandEdit = (props) => {
  const [toEdit, setToEdit] = useState(true);
  const [brand, setBrand] = useState("");
  const [brandId, setBrandId] = useState("");
  const [inProcess, setInProcess] = useState(false);

  const handleClick = () => {
    setToEdit(!toEdit);
  };
  const handleBrandChange = e => {
    setBrand(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };

  const handleSubmit = e => {
    e.preventDefault();
    if (!brand) setToInvalid(e.target.elements["brand"]);
    if (!brand || !brandId) return false;
    const update_brand = {
      name: brand,
      id: brandId
    };
    setInProcess(true);
    props.updateBrand(update_brand, () => {
      setInProcess(false);
      toastr.success("Brand Successfully Updated");
      closeSelfHandler();
    });
  };

  useEffect(() => {
    setBrand(props.brandDetails.name);
    setBrandId(props.brandDetails.id);
  }, [props.brandDetails]);

  const closeSelfHandler = () => {
    props.handleEditModalOpen();
    setToEdit(!toEdit);
  };
  const closeHandler = () => {
    props.handleEditModalOpen();
    setToEdit(true);
  };

  const viewMode = () => {
    return (
      <Fragment>
        <MDBModalBody>
          <MDBRow>
            <MDBCol>
              <label>Brand</label>
              <input
                value={props.brandDetails.name}
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol>
              <label>Company</label>
              <input
                value={(props.brandDetails && props.brandDetails.companyDetails) && props.brandDetails.companyDetails.name}
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>

        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="light-green" onClick={handleClick} type="submit">
            {" "}
            Edit
          </MDBBtn>
        </MDBModalFooter>
      </Fragment>
       
    )
  }

  const editMode = () => {
    return (
      <Fragment>
        <form className="needs-validation" onSubmit={handleSubmit} noValidate>
          <MDBModalBody>
            <MDBRow>
              <MDBCol>
                <label>Brand</label>
                <input
                  name={"brand"}
                  type="text"
                  value={brand}
                  className="form-control"
                  onChange={e => handleBrandChange(e)}
                  required
                />
                <div className="invalid-feedback">
                  Please provide brand Name
                </div>
              </MDBCol>
            </MDBRow>
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
              Cancel
            </MDBBtn>
            <MDBBtn color="light-green" type="submit">
              <LoadingText text={"Submit"} loading={inProcess} />
            </MDBBtn>
          </MDBModalFooter>
        </form>
      </Fragment>
    )
  }
  
  return (
    <MDBModal
      isOpen={props.isEditModalOpen}
      toggle={props.handleEditModalOpen}
      full-height
      position="top"
    >
      <MDBModalHeader toggle={closeHandler}>
        {props.brandDetails.name}
      </MDBModalHeader>

      {toEdit ? viewMode() : editMode()}
    </MDBModal>
  )
}
BrandEdit.propTypes = {
  updateBrand: PropTypes.func.isRequired
};
export default connect(null, { updateBrand })(BrandEdit);