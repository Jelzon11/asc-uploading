import React, { useState, useEffect } from "react";
import { LoadingText } from "./Utility-Component";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { deleteRadioMaterial } from "../actions/radioAction";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol
} from "mdbreact";

const RadioMaterialDelete = props => {
  const [radioMaterialId, setRadioMaterialId] = useState("");
  const [inProcess, setInProcess] = useState(false);

  useEffect(() => {
    setRadioMaterialId(props.radioDetails.id);
  }, [props.radioDetails]);

  const handleSubmit = e => {
    e.preventDefault();
    if (!radioMaterialId) return false;
    setInProcess(true);
    props.deleteRadioMaterial(
      {
        id: radioMaterialId
      },
      () => {
        setInProcess(false);
        toastr.success("Radio Material Successfully Deleted");
        closeSelfHandler();
      }
    );
  };

  const closeSelfHandler = () => {
    props.handleDeleteModalOpen();
  };

  return (
    <MDBModal
      isOpen={props.isDeleteModalOpen}
      toggle={props.handleDeleteModalOpen}
      full-height
      position="top"
    >
      <MDBModalHeader
        toggle={closeSelfHandler}
        tag="h4"
        className="font-weight-bold text-uppercase"
      >
        {props.radioDetails.title}
      </MDBModalHeader>
      <form className="needs-validation" onSubmit={handleSubmit} noValidate>
        <MDBModalBody>
          <MDBRow>
            <MDBCol>
              <p>
                Are you sure you want to delete{" "}
                <font style={{ fontWeight: "900", color: "red" }}>
                  {" "}
                  {props.radioDetails.title}{" "}
                </font>
                ?
              </p>
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
            No
          </MDBBtn>
          <MDBBtn color="light-green" type="submit">
            <LoadingText text={"Yes"} loading={inProcess} />
          </MDBBtn>
        </MDBModalFooter>
      </form>
    </MDBModal>
  );
};
RadioMaterialDelete.propTypes = {
  deleteRadioMaterial: PropTypes.func.isRequired
};
export default connect(null, { deleteRadioMaterial })(RadioMaterialDelete);
