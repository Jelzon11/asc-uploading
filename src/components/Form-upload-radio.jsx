import React, { useState, useEffect, Fragment } from "react";
import { Form, Button, Container, Row, Col, Card } from "react-bootstrap";
import Dropzone from "react-dropzone";
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import toastr from 'toastr';
//Internal Modules
import { commercialCategories, setToInvalid, setToValid } from "../utils";
import { createRadioMaterial, fetchReferenceCode } from '../actions/radioAction';
import { Select, ProgressBar } from './Utility-Component';
const audioMaxSize = 100000000; //byte //800 MegaBytesY
const acceptedFileTypes = "audio/mp3, audio/MP3";
const acceptedFileTypesArray = acceptedFileTypes.split(",").map(item => {
  return item.trim();
});

const Radio = props => {
  const [referenceCode, setReferenceCode] = useState('');
  const [title, setTitle] = useState('');
  const [duration, setDuration] = useState('');
  const [commercialCategory, selectedCommercialCategory] = useState(0);
  const [material, setMaterial] = useState('');
  const [src, setSrc] = useState(null);
  const [progressPercent, setProgressPercent] = useState(0);
  const [isInProgress, setInProgress] = useState(false);
  //Selelected Id's
  const [selectedProductId, setProductId] = useState(0);
  //List
  const [productList, setProductList] = useState([]);

  //File Verification
  const verifyFile = files => {
    if (files && files.length > 0) {
      const currentFile = files[0];
      const currentFileType = currentFile.type;
      const currentFileSize = currentFile.size;
      if (currentFileSize > audioMaxSize) {
        alert("This file is not allowed." + currentFileSize + " bytes is too large");
        return false;
      }
      if (!acceptedFileTypesArray.includes(currentFileType)) {
        alert("This file is not allowed. Only mp3 files are allowed.");
        return false;
      }
      const fileType = currentFile.name.split(".").pop();
      return fileType;
    }
  }

  //File OnDrop event Handler
  const handleOnDrop = (files, rejectedFiles) => {
    if (rejectedFiles && rejectedFiles.length > 0) verifyFile(rejectedFiles);
    if (files && files.length > 0) {
      const isVerified = verifyFile(files);
      if (isVerified) {
        const currentFile = files[0];
        const myFileItemReader = new FileReader();
        myFileItemReader.addEventListener("load", () => {
          setSrc(myFileItemReader.result);
          setMaterial(currentFile);
        }, false);
        myFileItemReader.readAsDataURL(currentFile);
      }
    }
  }

  //Form Submit handler
  const handleSubmit = async (e) => {
    e.preventDefault();
    if(!referenceCode ) setToInvalid(e.target.elements['referenceCode']);
    if(!title ) setToInvalid(e.target.elements['title']);
    if(!duration ) setToInvalid(e.target.elements['duration']);
    if(!selectedProductId) setToInvalid(e.target.elements['productSelect']);
    if(!commercialCategory) setToInvalid(e.target.elements['commercialCategorySelect']);
    if(!material) setToInvalid(e.target.elements['material']);
    if(
      !title || !duration || !selectedProductId || 
      !commercialCategory ||  !material || !referenceCode
    ) return false;
    let formData = new FormData();
    formData.set('referenceId', referenceCode);
    formData.set('title', title);
    formData.set('duration', duration); 
    formData.set('productId', selectedProductId);
    formData.set('commercialType', commercialCategory);
    formData.set('file', material);
    setInProgress(true);
    props.createRadioMaterial(formData, percent => setProgressPercent(percent), () => {
      toastr.success('Radio Material Successfully Uploaded', 'System');
      handleResetForm();
    });
  };

  const handleResetForm = () => {
    setProductId(0);
    setTitle('');
    setDuration('');
    selectedCommercialCategory(0);
    setInProgress(false);
    setSrc(null);
    setMaterial(null);
    setReferenceCode('');
    alert('referenceId: ' + referenceCode);
  }
  //referenceCodeOnChangeHandler
  // const referenceCodeOnChangeHandler = (e) => {
  //   (e.target.value !== '') 
  //   ? setToValid(e.target) 
  //   : setToInvalid(e.target);
  //   const referenceCode = e.target.value;
  //   setReferenceCode(referenceCode);
  // }

  const handleReferenceCodeBlur = e => {
    //setEmail(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
     const getReferenceCode = e.target.value;
     console.log(getReferenceCode);
     props.fetchReferenceCode(
     {
      referenceId: e.target.value,

     },
     res => {
       if(res === 'exist') {
         toastr.error("Reference Code already exist");
       }else {
        setReferenceCode(getReferenceCode);
       }
     }
   );
 }

  //Tile Onchange Handler
  const titleOnChangeHandler = (e) => {
    (e.target.value !== '') 
    ? setToValid(e.target) 
    : setToInvalid(e.target);
    const title = e.target.value;
    setTitle(title);
  }

  //Duration Onchange Handler
  const durationOnChangeHandler = (e) => {
    (e.target.value !== 0) 
    ? setToValid(e.target) 
    : setToInvalid(e.target);
    const duration = e.target.value;
    setDuration(duration);
  }

  //Comapany Select Onchange Hanlder
  const productOnChangeHandler = (e) => {
    (e.target.value !== 0) 
    ? setToValid(e.target) 
    : setToInvalid(e.target);
    const productId = e.target.value;
    setProductId(productId);
  }

  const commercialTypeOnChangeHandler = (e) => {
    (e.target.value !== 0) 
    ? setToValid(e.target) 
    : setToInvalid(e.target);
    const commercialCategory = e.target.value;
    selectedCommercialCategory(commercialCategory);
  }
  
  //User Effect lifecycle
  useEffect(() => {
    setProductList(props.products);
  }, [props.products])

  return (
    <div class="container-fluid">
      <Form className="needs-validation" onSubmit={handleSubmit} noValidate>
        <Row>
          <Col sm={12} xl={6}>
          <Form.Group>
              <label htmlFor="titleInput"  >
                Reference Code
              </label>
              <input
                type="text"
                name="referenceCode"
                //value={referenceCode}
                className="form-control"
                placeholder="Enter reference code"
                onBlur={e => handleReferenceCodeBlur(e)}
                //onChange={e => referenceCodeOnChangeHandler(e)}
                required
              />
              <div className="invalid-feedback">Please Fill Reference Code Field</div>
            </Form.Group>
            <Form.Group>
              <label htmlFor="titleInput"  >
                Title
              </label>
              <input
                type="text"
                name="title"
                value={title}
                className="form-control"
                placeholder="Enter title"
                onChange={e => titleOnChangeHandler(e)}
                required
              />
              <div className="invalid-feedback">Please Fill Title Field</div>
            </Form.Group>
            <Form.Group>
              <label htmlFor="titleInput"  >
                Duration (in secs)
              </label>
              <input
                type="number"
                name="duration"
                value={duration}
                className="form-control"
                placeholder="00"
                onChange={e => durationOnChangeHandler(e)}
                required
              />
              <div className="invalid-feedback">Please Fill Duration Field</div>
            </Form.Group>
            <Form.Group>
              <label htmlFor="productSelect"  >
                Product
              </label>
              <Select
                items={productList}
                value={selectedProductId}
                onChange={e => productOnChangeHandler(e)}
                name={"productSelect"}
                key={"productSelect"}
              />
              <div className="invalid-feedback">Please Select Product</div>
            </Form.Group>        
            <Form.Group>
              <label htmlFor="commercialCategorySelect"  >
                Commercial Category
              </label>
              <Select
                value={commercialCategory}
                items={commercialCategories}
                onChange={e => commercialTypeOnChangeHandler(e)}
                name={"commercialCategorySelect"}
                key={"commercialCategorySelect"}
                required
              />
              <div className="invalid-feedback">
                Please Select Commercial Type
              </div>
            </Form.Group>
          </Col>
          <Col sm={12} xl={6}>
            <Form.Group>
              <label htmlFor="material"  >
                Choose File
              </label>
              <Dropzone
                onDrop={handleOnDrop}
                accept={acceptedFileTypes}
                multiple={false}
                maxSize={audioMaxSize}
              >
                {({ getRootProps, getInputProps }) => (
                  <Fragment>
                    <div
                      style={{
                        borderColor: "lightgray",
                        borderStyle: "dashed",
                        textAlignment: "center",
                        margin: "5px 15px",
                        height: "120px"
                      }}
                    >
                      <section className={"is-invalid"}>
                        <div {...getRootProps()} style={{ width: "100%" }}>
                          <input 
                            type={"file"}
                            name={"material"}
                            key={"material"}
                            required
                           {...getInputProps()}
                          />
                          <p
                            style={{
                              color: "lightgray",
                              fontWeight: "bold",
                              textTransform: "uppercase",
                              height: "120px",
                              width: "100%",
                              textAlign: "center"
                            }}
                          >
                            Drag 'n' drop an audio here, or click to select
                          </p>
                          <div className="invalid-feedback">
                            Please Attach Audio File
                          </div>
                        </div>
                      </section>
                    </div>
                  </Fragment>
                )}
              </Dropzone>
              <hr />
              {src && material && (
                <Fragment>
                  <label  >Preview</label>
                  <div style={{ padding: "1rem" }}>
                    <Card>
                      <Card.Body>
                        <Card.Text>
                          Title :{" "}
                          <text style={{ color: "#3283a8" }}>
                            {material.name}
                          </text>
                        </Card.Text>
                        <Card.Text>
                          Type:{" "}
                          <text style={{ color: "#3283a8" }}>
                            {material.type}
                          </text>
                        </Card.Text>
                        {src && (
                          <audio
                            src={src}
                            controls
                            style={{ width: "100%" }}
                          ></audio>
                        )}
                      </Card.Body>
                    </Card>
                  </div>
                </Fragment>
              )}
            </Form.Group>
          </Col>
        </Row>
        <hr />
        <Row>
          {isInProgress ? (
            <Col>
              <label htmlFor="material"  >
                Progress
              </label>
              <ProgressBar percent={progressPercent} />
            </Col>
          ) : (
            <Fragment>
              <Col xl={9} sm={6}></Col>
              <Col xl={3} sm={6}>
                <Button variant="default" onClick={() => handleResetForm()}>
                  Clear
                </Button>
                <Button variant="primary" type="submit">
                  Submit
                </Button>
              </Col>
            </Fragment>
          )}
        </Row>
      </Form>
    </div>
  );
};

//Props Type
Radio.propTypes = {
  fetchReferenceCode: PropTypes.array.isRequired,
  products: PropTypes.array.isRequired
  
};

const mapStateToProps = state => ({
  products: state.products.productItems
});

export default connect(mapStateToProps, { fetchReferenceCode, createRadioMaterial })(Radio);
