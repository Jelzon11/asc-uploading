import React, { useState, useEffect } from 'react';
import { Modal, Button, Form, Toast } from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Select, LoadingText } from './Utility-Component';
import { createBrand } from '../actions/brandAction';
import { setToInvalid, setToValid } from '../utils';
import toastr from 'toastr';
import { 
  MDBModal, MDBModalHeader, MDBModalBody, MDBBtn, MDBModalFooter,
  MDBRow, MDBCol
} from 'mdbreact';



const BrandModal = (props) => {
  let [companyId, setCompanyId] = useState();
  const [companyList, setCompanyList] = useState([]);
  const [brand, setBrand] = useState('');
  const [inProcess, setInProcess] = useState(false);

  const handleBrandChange = (e) => {
    setBrand(e.target.value);
    setToValid(e.target);
    (e.target.value)
     ? setToValid(e.target) 
     : setToInvalid(e.target);
  }

  const handleCompanyId = (e) => {
    setCompanyId(e.target.value);
    setToValid(e.target);
  }
  const add_brand = {
    name : brand,
    companyId: companyId
  }
  

  const closeSelfHandler = () => {
    props.handleModalOpen();
    setCompanyId(0);
    setBrand('')
  }
  const handleSubmit = (e) => {
    e.preventDefault();
    if(!companyId) setToInvalid(e.target.elements['companySelect']);
    if(!brand) setToInvalid(e.target.elements['brand']);
    if(!companyId || !brand) return false;
    setInProcess(true);
      props.createBrand(add_brand,
        () => {
        setInProcess(false);
        toastr.success('Brand Successfully Added');
        closeSelfHandler();
      });
      
    
  }
  useEffect(() => {
    setCompanyList(props.companies)
  }, [props.companies])
  return (
    <MDBModal
      isOpen={props.isModalOpen} 
      toggle={props.handleModalOpen}  
      full-height  
      position="top"
    >
      <MDBModalHeader toggle={closeSelfHandler}>Add New</MDBModalHeader>
        <form 
          className="needs-validation"
          onSubmit={handleSubmit}
          noValidate
        >
          <MDBModalBody>
            <MDBRow style={{marginBottom: '1rem'}}>
              <MDBCol>
                <label > Company </label>
                <Select 
                  items={companyList} 
                  onChange={e => handleCompanyId(e)}
                  name={'companySelect'}
                />
              </MDBCol>
            </MDBRow>
            <MDBRow>
              <MDBCol>
                <label htmlFor="nameInput" >
                  Brand Name
                </label>
                <input
                  name={'brand'}
                  className="form-control"
                  onChange={e => handleBrandChange(e)}
                  required
                />
                <div className="invalid-feedback">
                  Please provide a brand name
                </div>
              </MDBCol>
            </MDBRow>
          </MDBModalBody>
          <MDBModalFooter>
          <MDBBtn color="blue-grey" onClick={closeSelfHandler}>Cancel</MDBBtn>
            <MDBBtn color="light-green" type="submit">
              <LoadingText
              text={'Submit'}
              loading={inProcess}
             />
            </MDBBtn>
          </MDBModalFooter>
        </form>
    </MDBModal>
  );
}

const mapStateToProps = state => ({
  companies: state.companies.companyItems,
  brands: state.brands.brandItems
})
BrandModal.propTypes = {
  createBrand: PropTypes.func.isRequired
 };
export default connect(mapStateToProps, {createBrand})(BrandModal);
