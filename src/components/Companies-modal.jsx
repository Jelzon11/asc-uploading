import React, { useState } from 'react';
import { Modal, Button, Toast } from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Select, LoadingText } from './Utility-Component';
import { createCompany } from '../actions/companyAction';
import { setToInvalid, setToValid } from '../utils';
import toastr from 'toastr';
import { 
  MDBModal, MDBModalHeader, MDBModalBody, MDBBtn, MDBModalFooter,
  MDBRow, MDBCol
} from 'mdbreact';

const CompaniesModal = (props) => {

  const [company, setCompany] = useState('');
  const [email, setEmail] = useState('');
  const [contact, setContact] = useState('');
  const [website, setWebsite] = useState('');
  const [inProcess, setInProcess] = useState(false);

  const handleCompanyChange = (e) => {
    setCompany(e.target.value);
    setToValid(e.target);
    (e.target.value)
     ? setToValid(e.target) 
     : setToInvalid(e.target);
  }
  const handleEmailChange = (e) => {
    setEmail(e.target.value);
    setToValid(e.target);
    (e.target.value)
     ? setToValid(e.target) 
     : setToInvalid(e.target);
  }
  const handleWebsiteChange = (e) => {
    setWebsite(e.target.value);
    setToValid(e.target);
    (e.target.value)
     ? setToValid(e.target) 
     : setToInvalid(e.target);
  }
  const handleContactChange = (e) => {
    setContact(e.target.value);
    setToValid(e.target);
    (e.target.value)
     ? setToValid(e.target) 
     : setToInvalid(e.target);
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    
    const add_company = {
      name: company,
      email: email,
      contact: contact,
      website: website,
    }
    
    if(!company) setToInvalid(e.target.elements['company']);
    if(!email) setToInvalid(e.target.elements['email']);
    if(!contact) setToInvalid(e.target.elements['contact']);
    if(!website) setToInvalid(e.target.elements['website']);
    if(!company || !email || !contact || !website) return false;
    setInProcess(true);
    props.createCompany(add_company,
      () => {
      setInProcess(false);
      toastr.success('Company Successfully Added');
      closeSelfHandler();
    });
  }

  const closeSelfHandler = () => {
    props.handleModalOpen();
    setCompany('');
    setEmail('');
    setContact('');
    setWebsite('');
  }
  return (
    <MDBModal
    isOpen={props.isModalOpen} 
    toggle={props.handleModalOpen}  
    full-height  
    position="top"
  >
    <MDBModalHeader toggle={closeSelfHandler}>Add New</MDBModalHeader>
      <form 
        className="needs-validation"
        onSubmit={handleSubmit}
        noValidate
      >
        <MDBModalBody>
          <MDBRow style={{ marginBottom: "1rem" }}>
            <MDBCol>
              <label   >
                Company
              </label>
              <input
                name={'company'}
                type= "text"
                className="form-control"
                onChange={e => handleCompanyChange(e)}
                required
              />
              <div className="invalid-feedback">
                Please provide company Name
              </div>
            </MDBCol>
          </MDBRow>
          <MDBRow style={{ marginBottom: "1rem" }}>
            <MDBCol>
              <label   >
                Email
              </label>
              <input
                name={'email'}
                type= "email"
                className="form-control"
                onChange={e => handleEmailChange(e)}
                required
              />
              <div className="invalid-feedback">
                Please provide an email Name
              </div>
            </MDBCol>
          </MDBRow>
          <MDBRow style={{ marginBottom: "1rem" }}>
            <MDBCol>
              <label   >
                Contact
              </label>
              <input
                name={'contact'}
                type= "text"
                className="form-control"
                onChange={e => handleContactChange(e)}
                required
              />
              <div className="invalid-feedback">
                Please provide a contact number 
              </div>
            </MDBCol>
          </MDBRow>
          <MDBRow style={{ marginBottom: "1rem" }}>
            <MDBCol>
              <label   >
                Website
              </label>
              <input
                name={'website'}
                type= "url"
                placeholder="http://tvcxpress.com"
                className="form-control"
                onChange={e => handleWebsiteChange(e)}
                required
              />
              <div className="invalid-feedback">
                Please provide a website 
              </div>
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
        <MDBBtn color="blue-grey" onClick={closeSelfHandler}>Cancel</MDBBtn>
          <MDBBtn color="light-green" type="submit">
            <LoadingText
            text={'Submit'}
            loading={inProcess}
           />
          </MDBBtn>
        </MDBModalFooter>
      </form>
  </MDBModal>
  );
}
CompaniesModal.propTypes = {
  createCompany: PropTypes.func.isRequired
 };
export default connect(null, { createCompany })(CompaniesModal);
