import React, { Component } from "react";
import Dropzone from "react-dropzone";

const videoMaxSize = 100000000; //byte
const acceptedFileTypes = "video/mxf, video/mov, video/mp4, video/mpeg";
const acceptedFileTypesArray = acceptedFileTypes.split(",").map(item => {
  return item.trim();
});
const Ooh_preview_video = props => {
  const { src, material } = props;
  //const material = props;
  console.log(material);
  return (
    <div>
      {src != null ? (
        <div>
          {" "}
          <Dropzone
            onDrop={props.handleOnDrop}
            accept={acceptedFileTypes}
            multiple={false}
            maxSize={videoMaxSize}
          >
            {({ getRootProps, getInputProps }) => (
              <div>
                <section>
                  <div {...getRootProps()} style={{ width: "100%" }}>
                    <input {...getInputProps()} />
                    <p>{material.name}</p>
                  </div>
                </section>
              </div>
            )}
          </Dropzone>
          <div
            style={{
              textAlignment: "center",
              margin: "5px 15px",
              borderLeft: "1px solid gray",
              borderRight: "1px solid gray",
              borderTop: "5px solid gray",
              borderBottom: "5px solid gray"
            }}
          >
            <video
              style={{ width: "100%", height: "auto" }}
              src={src}
              controls
            ></video>
          </div>
        </div>
      ) : (
        <Dropzone
          onDrop={props.handleOnDrop}
          accept={acceptedFileTypes}
          multiple={false}
          maxSize={videoMaxSize}
        >
          {({ getRootProps, getInputProps }) => (
            <div
              style={{
                borderStyle: "dashed",
                textAlignment: "center",
                margin: "5px 15px"
              }}
            >
              <section>
                <div {...getRootProps()} style={{ width: "100%" }}>
                  <input {...getInputProps()} />
                  <p
                    style={{
                      height: "200px",
                      width: "100%",
                      textAlign: "center"
                    }}
                  >
                    Drag 'n' drop a video here, or click to select
                  </p>
                </div>
              </section>
            </div>
          )}
        </Dropzone>
      )}
      {/* 
                <div style={{ "textAlignment": "center", "margin": "5px 15px", "borderLeft": "1px solid gray", "borderRight": "1px solid gray", "borderTop": "5px solid gray", "borderBottom": "5px solid gray" }}>
                </div> */}
    </div>
  );
};

export default Ooh_preview_video;
