import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Select, LoadingText } from "./Utility-Component";
import { createRole } from "../actions/roleAction";
import { setToInvalid, setToValid } from "../utils";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol,
  MDBTable,
  MDBTableBody,
  MDBTableHead
} from "mdbreact";

const RoleModal = props => {
  const [role, setRole] = useState("");
  const [inProcess, setInProcess] = useState(false);
  const [componentList, setComponentList] = useState([]);
  const [selectedComponents, setSelectedComponents] = useState();
  const [componentsArray, setComponentsArray] = useState([]);

  const handleRoleChange = e => {
    setRole(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };

  const handleSelectedComponents = e => {
    setSelectedComponents(e.target.value);
    const selectedComponent = componentList.filter(item => {
      return item.id == e.target.value;
    })[0];
    const selectedComponentSTR = JSON.stringify(selectedComponent);
    const componentsArraySTRSTR = JSON.stringify(componentsArray);
    const filterComponentsArraySTR = componentsArraySTRSTR.replace(
      selectedComponentSTR + ",",
      ""
    );
    const filterComponentsArrayParse = JSON.parse(filterComponentsArraySTR);

    setComponentsArray(filterComponentsArrayParse.concat(selectedComponent));
    e.target.value != 0 ? setToValid(e.target) : setToInvalid(e.target);
  };

  const handleRemove = itemId => {
    //alert(itemId);
    setComponentsArray(componentsArray.filter(item => item.id !== itemId));
  };
  const closeSelfHandler = () => {
    props.handleModalOpen();
    setRole("");
  };
  const add_role = {
    name: role
  };

  useEffect(() => {
    setComponentList(props.components);
  }, [props.components]);

  const items = componentsArray.map(function(item) {
    return (
      <tr key={item.id} id={item.id}>
        <td> {item.name} </td>
        <td>
          <MDBBtn size="sm" color="red" onClick={() => handleRemove(item.id)}>
            X
          </MDBBtn>
        </td>
      </tr>
    );
  });

  const handleSubmit = e => {
    e.preventDefault();
    if (!role) setToInvalid(e.target.elements["role"]);
    if (!selectedComponents)
      setToInvalid(e.target.elements["selectedComponents"]);
    setInProcess(true);
    setInProcess(false);
    toastr.success("User Successfully Added");
    closeSelfHandler();
  };

  return (
    <MDBModal
      isOpen={props.isModalOpen}
      toggle={props.handleModalOpen}
      full-height
      position="top"
    >
      <MDBModalHeader toggle={closeSelfHandler}>Add New</MDBModalHeader>
      <form className="needs-validation" onSubmit={handleSubmit} noValidate>
        <MDBModalBody>
          <MDBRow style={{ marginBottom: "1rem" }}>
            <MDBCol>
              <label htmlFor="nameInput">Role</label>
              <input
                name={"role"}
                className="form-control"
                onChange={e => handleRoleChange(e)}
                required
              />
              <div className="invalid-feedback">Please provide a role</div>
            </MDBCol>
          </MDBRow>
          <MDBRow style={{ marginBottom: "1rem" }}>
            <MDBCol size="9">
              <label> Select Components </label>
              <Select
                items={componentList}
                onChange={e => handleSelectedComponents(e)}
                name={"selectedComponents"}
              />
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol>
              <label htmlFor="nameInput">Components</label>
              <MDBTable>
                <MDBTableHead color="primary-color" textWhite>
                  <tr>
                    <th>Component</th>
                    <th>Action</th>
                  </tr>
                </MDBTableHead>
                <MDBTableBody>{items}</MDBTableBody>
              </MDBTable>
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
            Cancel
          </MDBBtn>
          <MDBBtn color="light-green" type="submit">
            <LoadingText text={"Submit"} loading={inProcess} />
          </MDBBtn>
        </MDBModalFooter>
      </form>
    </MDBModal>
  );
};
RoleModal.propTypes = {
  createRole: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  components: state.components.items
});

export default connect(mapStateToProps, { createRole })(RoleModal);
