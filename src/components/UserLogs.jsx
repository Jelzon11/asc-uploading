import "../css/datatables.css";
import "../css/bs4.css";
import React, { useEffect, useState, Fragment } from "react";
import {
  MDBDataTable,
  MDBCard,
  MDBCardHeader,
  MDBCardBody,
  MDBBtn,
  MDBIcon
} from "mdbreact";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import moment from "moment";
import { fetchLogs } from "../actions/userAction";
import { logsTemplate } from "../utils"
const _logsPageId = 'f83a4623-a734-427f-9e72-159a7d1a13fd';


const columns = () => {
  return [
    {
      label: "Action",
      field: "action",
      sort: "asc",
      width: 500
    },
    
    {
      label: "Date & Time",
      field: "dateTime",
      sort: "asc",
      width: 100
    }
  ];
};
const UserLogs = props => {
  const [isCreatedSuccess, setCreatedSuccess] = useState(false);
  const [logsDetails, setlogsDetails] = useState([]);
  const [logsList, setLogsList] = useState([]);

  
  
  const successToastHandler = () => {
    setCreatedSuccess(true);
  };

  useEffect(() => {
    props.fetchLogs(props.match.params.userId);
  }, [props.match.params.userId]);

  useEffect(() => {
    setlogsDetails(props.logs);
  }, [props.logs]);
  
  // logsDetails && logsDetails.logs &&
  //   logsTemplate(logsDetails.logs.map(log => {
  //     return {
  //       logId: log.id,
  //       parameters: log.parameter,
  //     }
  //   }));
  const renderData = logsDetails => {
    const newLogsData = logsDetails && logsDetails.logs && logsDetails.logs.map(log => {
      return {
        action: logsTemplate(log.logId, log.parameter, log.logDetails),
        dateTime: moment.unix(log["createdTimestamp"]).format("YYYY-MM-dddd hh:mm:ss a"),
        
      };
    });
    return newLogsData;
  };
  return (
    <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="col-md-12">
            <br />
            <div>
              <MDBCard style={{ padding: "1rem" }}>
                <MDBCardHeader
                  tag="h4"
                  className="font-weight-bold text-uppercase"
                >
                 User Logs
                </MDBCardHeader>
               
                <hr />

                <MDBCardBody>
                  <MDBDataTable
                    fixed={true}
                    small
                    bordered
                    hover
                    order={["name", "desc"]}
                    disableRetreatAfterSorting={true}
                    sortable={true}
                    responsive={true}
                    searchLabel={"Search..."}
                    responsiveSm={true}
                    responsiveMd={true}
                    responsiveLg={true}
                    responsiveXl={true}
                    noBottomColumns={true}
                    data={{
                      rows: renderData(logsDetails),
                      columns: columns()
                    }}
                  />
                </MDBCardBody>
              </MDBCard>
            </div>
            <br />
          </div>
        </div>
      </div>
    </div>
  );
};
UserLogs.propTypes = {
  fetchLogs: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  logs: state.users.logs
});

export default connect(mapStateToProps, {fetchLogs})(UserLogs);
