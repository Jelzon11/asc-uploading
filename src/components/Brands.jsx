import React, { useState, useEffect, Fragment } from "react";
import BrandModal from "./Brands-modal";
import BrandEdit from "./BrandEdit";
import BrandDelete from "./BrandDelete";
import {
  MDBDataTable,
  MDBCard,
  MDBCardHeader,
  MDBCardBody,
  MDBBtn,
  MDBIcon
} from "mdbreact";
import { connect } from "react-redux";
import moment from "moment";
import { isAuthorize } from '../utils';
import UnAuthorize from './UnAuthorize';
const _brandPageId = 'd356a4f4-5e17-4b86-96da-f7c1718b3024';

const Brands = props => {
  const [brandList, setBrandList] = useState([]);
  const [isModalOpen, setModalOpen] = useState(false);
  const [isEditModalOpen, setEditModalOpen] = useState(false);
  const [isDeleteModalOpen, setDeleteModalOpen] = useState(false);
  const [isCreatedSuccess, setCreatedSuccess] = useState(false);
  const [brandDetails, setBrandDetails] = useState("");

  const columns = () => {
    return [
      {
        label: "Name",
        field: "name",
        sort: "asc",
        width: 500
      },
      {
        label: "Company",
        field: "companyName",
        sort: "asc",
        width: 500
      },
      {
        label: "Last Updated",
        field: "updatedTimestamp",
        sort: "asc",
        width: 100
      },
      {
        label: "Created Date",
        field: "createdTimestamp",
        sort: "asc",
        width: 100
      },
      {
        label: "Action",
        field: "action",
        sort: "asc",
        width: 100
      }
    ];
  };
  const handleModalOpen = () => {
    if (isModalOpen === false) {
      setModalOpen(true);
    } else {
      setModalOpen(false);
    }
  };

  const handleDeleteModalOpen = brand => {
    if (isDeleteModalOpen === false) {
      setDeleteModalOpen(true);
      setBrandDetails(brand);
    } else {
      setDeleteModalOpen(false);
    }
  };

  const handleEditModalOpen = brand => {
    if (isEditModalOpen === false) {
      setEditModalOpen(true);
      setBrandDetails(brand);
    } else {
      setEditModalOpen(false);
    }
  };

  const successToastHandler = () => {
    setCreatedSuccess(true);
  };

  useEffect(() => {
    setBrandList(props.brands);
    
  }, [props.brands]);

  const renderData = brandData => {
    const newBrandData = brandData.map(brand => {
      return {
        name: brand.name,
        companyName: brand.companyDetails.name,
        updatedTimestamp: moment.unix(brand["updatedTimestamp"]).calendar(),
        createdTimestamp: moment.unix(brand["createdTimestamp"]).format("L"),
        action: (
          <Fragment>
            <MDBBtn
              id={brand.id}
              color="light-blue"
              size="sm"
              onClick={() => handleEditModalOpen(brand)}
            >
              <MDBIcon icon="pencil-alt" />
            </MDBBtn>
            <MDBBtn
              id={brand.id}
              color="blue-grey"
              size="sm"
              onClick={() => handleDeleteModalOpen(brand)}
            >
              <MDBIcon icon="trash-alt" />
            </MDBBtn>
          </Fragment>
        )
      };
    });
    return newBrandData;
  };
  
  return (
    isAuthorize(_brandPageId) ? ( 
    <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="col-md-12">
            <br />
            <div className="">
              <BrandModal
                isModalOpen={isModalOpen}
                successToastHandler={successToastHandler}
                handleModalOpen={handleModalOpen}
              />
              <BrandEdit
                isEditModalOpen={isEditModalOpen}
                handleEditModalOpen={handleEditModalOpen}
                brandDetails={brandDetails}
                
              />

              <BrandDelete
                isDeleteModalOpen={isDeleteModalOpen}
                handleDeleteModalOpen={handleDeleteModalOpen}
                brandDetails={brandDetails}
              />
              <MDBCard style={{ padding: "1rem" }}>
                <MDBCardHeader
                  tag="h4"
                  className="font-weight-bold text-uppercase"
                >
                  Brands
                </MDBCardHeader>
                <div className="d-flex align-items-start flex-column bd-highlight example-parent">
                  <MDBBtn onClick={handleModalOpen} color="default">
                    Add Brand
                  </MDBBtn>
                </div>
                <hr />

                <MDBCardBody>
                  <MDBDataTable
                    fixed={true}
                    small
                    bordered
                    hover
                    order={["name", "desc"]}
                    disableRetreatAfterSorting={true}
                    sortable={true}
                    responsive={true}
                    searchLabel={"Search..."}
                    responsiveLg={true}
                    responsiveXl={true}
                    noBottomColumns={true}
                    data={{
                      rows: renderData(brandList),
                      columns: columns()
                    }}
                  />
                </MDBCardBody>
              </MDBCard>
            </div>
            <br />
          </div>
        </div>
      </div>
    </div>
    ) : <UnAuthorize />
  ) 
};
const mapStateToProps = state => ({
  companies: state.companies.companyItems,
  brands: state.brands.brandItems
});
export default connect(mapStateToProps)(Brands);
