import React, { useState, useEffect, Fragment } from "react";
import CompaniesModal from "./Companies-modal";
import CompanyEdit from "./CompanyEdit";
import CompanyDelete from "./CompanyDelete";
import { Redirect } from 'react-router-dom';
import UnAuthorize from './UnAuthorize';
import { isAuthorize } from '../utils';
import {
  MDBDataTable,
  MDBCard,
  MDBCardHeader,
  MDBCardBody,
  MDBBtn,
  MDBIcon
} from "mdbreact";
import { connect } from "react-redux";
import moment from "moment";
const _companyPageId = 'b0588087-d8c8-400b-ab08-fa63d8e2ccf5';


const columns = () => {
  return [
    {
      label: "Name",
      field: "name",
      sort: "asc",
      width: 500
    },
    {
      label: "Last Updated",
      field: "updatedTimestamp",
      sort: "asc",
      width: 100
    },
    {
      label: "Created Date",
      field: "createdTimestamp",
      sort: "asc",
      width: 100
    },
    {
      label: "Action",
      field: "action",
      sort: "asc",
      width: 100
    }
  ];
};

const Companies = props => {
  const [companyList, setCompanyList] = useState([]);
  const [isModalOpen, setModalOpen] = useState(false);
  const [isEditModalOpen, setEditModalOpen] = useState(false);
  const [isDeleteModalOpen, setDeleteModalOpen] = useState(false);
  const [isCreatedSuccess, setCreatedSuccess] = useState(false);
  const [companyDetails, setCompanyDetails] = useState("");

  const handleModalOpen = () => {
    if (isModalOpen == false) {
      setModalOpen(true);
    } else {
      setModalOpen(false);
    }
  };

  const handleDeleteModalOpen = company => {
    if (isDeleteModalOpen == false) {
      setDeleteModalOpen(true);
      setCompanyDetails(company);
    } else {
      setDeleteModalOpen(false);
    }
  };

  const handleEditModalOpen = company => {
    if (isEditModalOpen == false) {
      setEditModalOpen(true);
      setCompanyDetails(company);
    } else {
      setEditModalOpen(false);
    }
  };

  const successToastHandler = () => {
    setCreatedSuccess(true);
  };

  useEffect(() => {
    setCompanyList(props.companies);
  }, [props.companies]);

  const renderData = companyList => {
    const newCompanyList = companyList.map(company => {
      return {
        name: company.name,
        updatedTimestamp: moment.unix(company["updatedTimestamp"]).calendar(),
        createdTimestamp: moment.unix(company["createdTimestamp"]).format("L"),
        action: (
          <Fragment>
            <center>
            <MDBBtn
                id={company.id}
                color="light-blue"
                size="sm"
                onClick={() => handleEditModalOpen(company)}
              >
                <MDBIcon icon="pencil-alt" />
              </MDBBtn>
              <MDBBtn
                id={company.id}
                onClick={() => handleDeleteModalOpen(company)}
                color="blue-grey"
                size="sm"
              >
                <MDBIcon icon="trash-alt" />
              </MDBBtn>
            </center>
          </Fragment>
        )
      };
    });
    return newCompanyList;
  };
  return isAuthorize(_companyPageId) ? (
    <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="col-md-12">
            <br />
            <CompaniesModal
              isModalOpen={isModalOpen}
              successToastHandler={successToastHandler}
              handleModalOpen={handleModalOpen}
            />
            <CompanyEdit
              isEditModalOpen={isEditModalOpen}
              handleEditModalOpen={handleEditModalOpen}
              companyDetails={companyDetails}
            />

            <CompanyDelete
              isDeleteModalOpen={isDeleteModalOpen}
              handleDeleteModalOpen={handleDeleteModalOpen}
              companyDetails={companyDetails}
            />
            <MDBCard style={{ padding: "1rem" }}>
              <MDBCardHeader
                tag="h4"
                className="font-weight-bold text-uppercase"
              >
                Companies
              </MDBCardHeader>

              <div className="d-flex align-items-start flex-column bd-highlight example-parent">
                <MDBBtn onClick={handleModalOpen} color="default">
                  Add Company
                </MDBBtn>
              </div>
              <hr />
              <MDBCardBody>
                <MDBDataTable
                  fixed={true}
                  small
                  bordered
                  hover
                  order={["name", "desc"]}
                  disableRetreatAfterSorting={true}
                  sortable={true}
                  responsive={true}
                  searchLabel={"Search..."}
                  responsiveLg={true}
                  responsiveXl={true}
                  noBottomColumns={true}
                  data={{
                    rows: renderData(companyList),
                    columns: columns()
                  }}
                />
              </MDBCardBody>
            </MDBCard>
          </div>
          <br />
        </div>
      </div>
    </div>
  ) : <UnAuthorize />
};
const mapStateToProps = state => ({
  companies: state.companies.companyItems
});
export default connect(mapStateToProps)(Companies);
