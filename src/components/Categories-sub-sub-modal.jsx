import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Select, LoadingText } from './Utility-Component';
import { createSubSubCategories } from '../actions/subSubCategoryAction'
import { setToInvalid, setToValid } from '../utils';
import toastr from 'toastr';
import { 
  MDBModal, MDBModalHeader, MDBModalBody, MDBBtn, MDBModalFooter,
  MDBRow, MDBCol
} from 'mdbreact';


const SubSubCategoryModalForm = (props) => {
  const [nameInput, setName] = useState();
  const [inProcess, setInProcess] = useState(false);
  const [subCategoryList, setSubCategoryList] = useState([]);
  const [filteredSubCategoryList, setFilteredSubCategoryList] = useState([]);
  const [mainCategoryList, setMainCategoryList] = useState([]);
  const [selectedMainCategoryId, setSelectedMainCategory] = useState(0);
  const [selectedSubCategoryId, setSelectedSubCategoryId] = useState(0);

  const handleNameChange = (e) => {
    setName(e.target.value);
    (e.target.value)
     ? setToValid(e.target) 
     : setToInvalid(e.target);
  }

  const selectMainCategoryHandler = (e) => {
    setSelectedMainCategory(e.target.value);
    (e.target.value != 0) 
     ? setToValid(e.target) 
     : setToInvalid(e.target);
     setFilteredSubCategoryList(subCategoryList.filter(item => item.mainCategoryId == e.target.value));
  }

  const selectSubCategoryHandler = (e) => {
    setSelectedSubCategoryId(e.target.value);
    (e.target.value != 0) 
     ? setToValid(e.target) 
     : setToInvalid(e.target);
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    if(!selectedMainCategoryId) setToInvalid(e.target.elements['mainCategorySelect']);
    if(!selectedSubCategoryId) setToInvalid(e.target.elements['subCategorySelect']);
    if(!nameInput) setToInvalid(e.target.elements['nameInput']);
    if(!selectedMainCategoryId || !nameInput) return false;
    setInProcess(true);
    props.createSubSubCategories({ 
      name : nameInput,
      subCategoryId: selectedSubCategoryId
    },() => {
      setInProcess(false);
      toastr.success('Sub-Sub Category Successfully Added');
      closeSelfHandler();
    });
  }

  const closeSelfHandler = () => {
    props.handleModalOpen();
    setSelectedMainCategory(0);
    setSelectedSubCategoryId(0);
    setFilteredSubCategoryList([]);
    setName('')
  }

  useEffect(() => {
    setMainCategoryList(props.main_categories);
    setSubCategoryList(props.sub_categories);
  }, [
    props.main_categories,
    props.sub_categories
  ])

  return (
    <MDBModal
      isOpen={props.isModalOpen} 
      toggle={props.handleModalOpen}  
      full-height  
      position="top"
    >
      <MDBModalHeader toggle={closeSelfHandler}>Add New</MDBModalHeader>
        <form 
          className="needs-validation"
          onSubmit={handleSubmit}
          noValidate
        >
          <MDBModalBody>
            <MDBRow style={{marginBottom: '1rem'}}>
              <MDBCol>
                <label  >Select Main Category </label>
                <Select 
                  items={mainCategoryList} 
                  onChange={e => selectMainCategoryHandler(e)}
                  name={'mainCategorySelect'}
                />
              <div className="invalid-feedback">
                Please Select Main Category
              </div>
              </MDBCol>
            </MDBRow>
            <MDBRow style={{marginBottom: '1rem'}}>
              <MDBCol>
                <label  > Select Sub Category </label>
                <Select 
                  disabled={(selectedMainCategoryId == 0) && true}
                  items={filteredSubCategoryList} 
                  onChange={e => selectSubCategoryHandler(e)}
                  name={'subCategorySelect'}
                />
              <div className="invalid-feedback">
                Please Select Sub Category
              </div>
              </MDBCol>
            </MDBRow>
            <MDBRow>
              <MDBCol>
                <label htmlFor="nameInput"  >
                  Sub Sub Category Name
                </label>
                <input
                  name={'nameInput'}
                  className="form-control"
                  onChange={e => handleNameChange(e)}
                  id={'subcategory-name-input'}
                  required
                />
                <div className="invalid-feedback">
                  Please provide Sub-Sub-Category Name
                </div>
              </MDBCol>
            </MDBRow>
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn color="blue-grey" onClick={closeSelfHandler}>Cancel</MDBBtn>
            <MDBBtn color="light-green" type="submit">
              <LoadingText
              text={'Submit'}
              loading={inProcess}
             />
            </MDBBtn>
          </MDBModalFooter>
        </form>
    </MDBModal>
  );
}

SubSubCategoryModalForm.propTypes = {
  main_categories: PropTypes.array.isRequired,
  sub_categories: PropTypes.array.isRequired
}

const mapStateToProps = (state) => ({
  main_categories: state.main_categories.items,
  sub_categories: state.sub_categories.items
})

export default connect(mapStateToProps, { createSubSubCategories })(SubSubCategoryModalForm);
