import React, { } from "react";


const Error404 = () => {
 
  return (
    <div className="content-wrapper">
        <div className="content">
          <div className="container-fluid">
            <div className="col-md-12">
             <h1>Error 404, Page not found.</h1>
            </div>
          </div>
        </div>
      </div>
  );
};

export default Error404;
