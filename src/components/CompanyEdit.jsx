import React, { useState, Fragment, useEffect } from "react";
import { setToInvalid, setToValid } from "../utils";
import { LoadingText } from "./Utility-Component";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { updateCompany } from "../actions/companyAction";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol
} from "mdbreact";


const CompanyEdit = props => {
  const [toEdit, setToEdit] = useState(true);
  const [company, setCompany] = useState("");
  const [email, setEmail] = useState("");
  const [contact, setContact] = useState("");
  const [website, setWebsite] = useState("");
  const [companyId, setCompanyId] = useState("");
  const [inProcess, setInProcess] = useState(false);

  const handleClick = () => {
    setToEdit(!toEdit);
  };

  const handleCompanyChange = e => {
    setCompany(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };
  const handleEmailChange = e => {
    setEmail(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };
  const handleWebsiteChange = e => {
    setWebsite(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };
  const handleContactChange = e => {
    setContact(e.target.value);
    setToValid(e.target);

    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };

  const handleSubmit = e => {
    e.preventDefault();
    if (!company) setToInvalid(e.target.elements["company"]);
    if (!email) setToInvalid(e.target.elements["email"]);
    if (!contact) setToInvalid(e.target.elements["contact"]);
    if (!website) setToInvalid(e.target.elements["website"]);
    if (!company || !email || !contact || !website || !companyId) return false;
    const update_company = {
      name: company,
      email: email,
      contact: contact,
      website: website,
      id: companyId
    };
    setInProcess(true);
    props.updateCompany(update_company, () => {
      setInProcess(false);
      toastr.success("Company Successfully Updated");
      closeSelfHandler();
    });
  };

  useEffect(() => {
    setCompany(props.companyDetails.name);
    setEmail(props.companyDetails.email);
    setContact(props.companyDetails.contact);
    setWebsite(props.companyDetails.website);
    setCompanyId(props.companyDetails.id);

  }, [props.companyDetails]);

  const closeSelfHandler = () => {
    props.handleEditModalOpen();
    setToEdit(!toEdit);
  };

  const closeHandler = () => {
    props.handleEditModalOpen();
    setToEdit(true);
  };
  const viewMode = () => {
    return (
      <Fragment>
        <MDBModalBody>
          <MDBRow>
            <MDBCol>
              <label>Company</label>
              <input
                value={props.companyDetails.name}
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol>
              <label>Email</label>
              <input
                value={props.companyDetails.email}
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol>
              <label>Contact</label>
              <input
                value={props.companyDetails.contact}
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol>
              <label>Website</label>
              <input
                value={props.companyDetails.website}
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="light-green" onClick={handleClick} type="submit">
            {" "}
            Edit
          </MDBBtn>
        </MDBModalFooter>
      </Fragment>
    );
  };
  const editMode = () => {
    return (
      <Fragment>
        <form className="needs-validation" onSubmit={handleSubmit} noValidate>
          <MDBModalBody>
            <MDBRow>
              <MDBCol>
                <label>Company</label>
                <input
                  name={"company"}
                  type="text"
                  value={company}
                  className="form-control"
                  onChange={e => handleCompanyChange(e)}
                  required
                />
                <div className="invalid-feedback">
                  Please provide company Name
                </div>
              </MDBCol>
            </MDBRow>
            <MDBRow>
              <MDBCol>
                <label>Email</label>
                <input
                  name={"email"}
                  type="email"
                  value={email}
                  className="form-control"
                  onChange={e => handleEmailChange(e)}
                  required
                />
                <div className="invalid-feedback">Please provide email</div>
              </MDBCol>
            </MDBRow>
            <MDBRow>
              <MDBCol>
                <label>Contact</label>
                <input
                  name={"contact"}
                  type="text"
                  value={contact}
                  className="form-control"
                  onChange={e => handleContactChange(e)}
                  required
                />
                <div className="invalid-feedback">Please provide contact</div>
              </MDBCol>
            </MDBRow>
            <MDBRow>
              <MDBCol>
                <label>Website</label>
                <input
                  name={"website"}
                  type="url"
                  value={website}
                  className="form-control"
                  onChange={e => handleWebsiteChange(e)}
                  required
                />
                <div className="invalid-feedback">Please provide website</div>
              </MDBCol>
            </MDBRow>
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
              Cancel
            </MDBBtn>
            <MDBBtn color="light-green" type="submit">
              <LoadingText text={"Submit"} loading={inProcess} />
            </MDBBtn>
          </MDBModalFooter>
        </form>
      </Fragment>
    );
  };
  //console.log(props.companyDetails);
  return (
    <MDBModal
      isOpen={props.isEditModalOpen}
      toggle={props.handleEditModalOpen}
      full-height
      position="top"
    >
      <MDBModalHeader toggle={closeHandler}>
        {props.companyDetails.name}
      </MDBModalHeader>

      {toEdit ? viewMode() : editMode()}
    </MDBModal>
  );
};
CompanyEdit.propTypes = {
  updateCompany: PropTypes.func.isRequired
};
export default connect(null, { updateCompany })(CompanyEdit);
