import React, { useState, useEffect, Fragment } from "react";
import { Link, NavLink } from "react-router-dom";
import "../css/CustomSideNavbar.css";
import { isAuthorize } from "../utils";
import { connect } from "react-redux";

const _categoryPage = "17f9d564-35f9-4bd5-9021-f66b01e2673e";
const _materialPage = "81cb6447-4aab-40c6-a0f6-7561046fded5";
const _userPage = "f83a4623-a734-427f-9e72-159a7d1a13fd";
const _companyPage = "b0588087-d8c8-400b-ab08-fa63d8e2ccf5";
const _brandPage = "d356a4f4-5e17-4b86-96da-f7c1718b3024";
const _uploadingPage = "10a2b656-6cf2-42e1-875d-5433004aaa02";

const CustomSideNavbar = props => {
  const [activeNav, setActiveNav] = useState("upload");
  const [mySelf, setMySelf] = useState();

  useEffect(() => {
    setMySelf(props.me);
  }, [props.me]);

  return (
    <aside className="main-sidebar sidebar-light-danger elevation-4">
      <Link to="/" className="brand-link">
        <img
          src="../assets/dist/img/asc-logo.png"
          alt="ASC Logo"
          className="brand-image img-box elevation-3"
          style={{ opacity: ".8" }}
        />
        <span className="brand-text font-weight-bold">ASC </span>
      </Link>
      <div className="sidebar">
        <div className="user-panel mt-3 pb-3 mb-3 d-flex">
          <div className="image">
            <img
              src="../assets/dist/img/avatar5.png"
              className="img-circle elevation-2"
              alt="User Image"
            />
          </div>
          <div className="info">
            {" "}
            {props.me && (
              <Fragment>
                <a href="#" className="d-block">
                  {`${props.me.firstName} ${props.me.lastName}`}
                </a>
                <text style={{ color: "#3283a8", fontSize: "0.8rem" }}>
                  {props.me &&
                    props.me.roleDetails &&
                    props.me.roleDetails.name}
                </text>
              </Fragment>
            )}
          </div>
        </div>
        <nav className="mt-2">
          <ul className="nav nav-pills nav-sidebar flex-column">
            {isAuthorize(_uploadingPage) && (
              <li className="nav-item">
                <NavLink
                  exact={true}
                  activeClassName="active"
                  to="/upload"
                  className="nav-link"
                >
                  <i className="nav-icon fas fa-cloud-upload-alt"></i>
                  <p>Upload</p>
                </NavLink>
              </li>
            )}
            {isAuthorize(_categoryPage) && (
              <li className="nav-item">
                <NavLink
                  activeClassName="active"
                  to="/categories"
                  className="nav-link"
                >
                  <i className="nav-icon fas fa-table"></i>
                  <p>Categories</p>
                </NavLink>
              </li>
            )}
            {isAuthorize(_companyPage) && (
              <li className="nav-item">
                <NavLink
                  activeClassName="active"
                  to="/companies"
                  className="nav-link"
                >
                  <i className="nav-icon fas fa-building"></i>
                  <p>Company</p>
                </NavLink>
              </li>
            )}
            {isAuthorize(_brandPage) && (
              <li className="nav-item">
                <NavLink
                  activeClassName="active"
                  to="/brands"
                  className="nav-link"
                >
                  <i className="nav-icon fas fa-table"></i>
                  <p>Brand</p>
                </NavLink>
              </li>
            )}
             <li className="nav-item">
                <NavLink
                  activeClassName="active"
                  to="/products"
                  className="nav-link"
                >
                  <i className="nav-icon fas fa-boxes"></i>
                  
                  <p>Products</p>
                </NavLink>
              </li>
              {isAuthorize(_materialPage) && (
              <li className="nav-item">
                <NavLink
                  activeClassName="active"
                  to="/materials"
                  className="nav-link"
                >
                  <i className="nav-icon fas fa-archive"></i>
                  <p>Materials</p>
                </NavLink>
              </li>
            )}
            {isAuthorize(_userPage) && (
              <li className="nav-item">
                <NavLink
                  activeClassName="active"
                  to="/users"
                  className="nav-link"
                >
                  <i className="nav-icon fas fa-user"></i>
                  <p>Users</p>
                </NavLink>
              </li>
            )}
          </ul>
        </nav>
      </div>
    </aside>
  );
};

const mapStateToProps = state => ({
  me: state.users.userMe
});

export default connect(mapStateToProps)(CustomSideNavbar);
