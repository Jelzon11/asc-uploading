import "../css/datatables.css";
import "../css/bs4.css";
import React, { useEffect, useState, Fragment } from "react";
import CategoriesMainForm from "./Categories-main-modal";
import MainCategoryEdit from "./MainCategoryEdit";
import MainCategoryDelete from "./MainCategoryDelete";
import PropTypes from "prop-types";
import {
  MDBDataTable,
  MDBCard,
  MDBCardHeader,
  MDBCardBody,
  MDBBtn,
  MDBIcon
} from "mdbreact";
import { connect } from "react-redux";

import moment from "moment";

const columns = () => {
  return [
    {
      label: "Name",
      field: "name",
      sort: "asc",
      width: 500
    },
    {
      label: "Last Updated",
      field: "updatedTimestamp",
      sort: "asc",
      width: 100
    },
    {
      label: "Created Date",
      field: "createdTimestamp",
      sort: "asc",
      width: 100
    },
    {
      label: "Action",
      field: "action",
      sort: "asc",
      width: 100
    }
  ];
};

const CategoriesMain = props => {
  const [mainCategoryList, setMainCategoryList] = useState([]);
  const [isModalOpen, setModalOpen] = useState(false);
  const [isEditModalOpen, setEditModalOpen] = useState(false);
  const [isDeleteModalOpen, setDeleteModalOpen] = useState(false);
  const [isCreatedSuccess, setCreatedSuccess] = useState(false);
  const [mainCategoryDetails, setMainCategoryDetails] = useState("");

  const handleModalOpen = () => {
    if (isModalOpen == false) {
      setModalOpen(true);
    } else {
      setModalOpen(false);
    }
  };

  const handleDeleteModalOpen = mainCategory => {
    if (isDeleteModalOpen == false) {
      setDeleteModalOpen(true);
      setMainCategoryDetails(mainCategory);
    } else {
      setDeleteModalOpen(false);
    }
  };

  const handleEditModalOpen = mainCategory => {
    if (isEditModalOpen == false) {
      setEditModalOpen(true);
      setMainCategoryDetails(mainCategory);
    } else {
      setEditModalOpen(false);
    }
  };

  const successToastHandler = () => {
    setCreatedSuccess(true);
  };

  useEffect(() => {
    setMainCategoryList(props.main_categories);
  }, [props.main_categories]);

  const renderData = items => {
    const newMainCategoryList = items.map(item => {
      return {
        name: item.name,
        updatedTimestamp: moment.unix(item["updatedTimestamp"]).calendar(),
        createdTimestamp: moment.unix(item["createdTimestamp"]).format("L"),
        action: (
          <Fragment>
            <center>
              <MDBBtn
                id={item.id}
                color="light-blue"
                size="sm"
                onClick={() => handleEditModalOpen(item)}
              >
                <MDBIcon icon="pencil-alt" />
              </MDBBtn>
              <MDBBtn
                id={item.id}
                color="blue-grey"
                size="sm"
                onClick={() => handleDeleteModalOpen(item)}
              >
                <MDBIcon icon="trash-alt" />
              </MDBBtn>
            </center>
          </Fragment>
        )
      };
    });
    return newMainCategoryList;
  };

  return (
    <Fragment>
      <CategoriesMainForm
        isModalOpen={isModalOpen}
        successToastHandler={successToastHandler}
        handleModalOpen={handleModalOpen}
      />
      <MainCategoryEdit
        isEditModalOpen={isEditModalOpen}
        handleEditModalOpen={handleEditModalOpen}
        mainCategoryDetails={mainCategoryDetails}
      />

      <MainCategoryDelete
        isDeleteModalOpen={isDeleteModalOpen}
        handleDeleteModalOpen={handleDeleteModalOpen}
        mainCategoryDetails={mainCategoryDetails}
      />

      <MDBCard style={{ padding: "1rem" }}>
        <MDBCardHeader tag="h4" className="font-weight-bold text-uppercase">
          Main Category
        </MDBCardHeader>
        <div className="d-flex align-items-start flex-column bd-highlight example-parent">
          <MDBBtn onClick={handleModalOpen} color="default">
            Add New
          </MDBBtn>
        </div>
        <hr />
        <MDBCardBody>
          <MDBDataTable
            fixed={true}
            small
            bordered
            hover
            order={["name", "desc"]}
            disableRetreatAfterSorting={true}
            sortable={true}
            responsive={true}
            searchLabel={"Search..."}
            responsiveLg={true}
            responsiveXl={true}
            noBottomColumns={true}
            data={{
              rows: renderData(mainCategoryList),
              columns: columns()
            }}
          />
        </MDBCardBody>
      </MDBCard>
    </Fragment>
  );
};

CategoriesMain.propTypes = {
  main_categories: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  main_categories: state.main_categories.items
});

export default connect(mapStateToProps)(CategoriesMain);
