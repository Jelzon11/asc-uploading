import React, { useState, useEffect, Fragment } from "react";
import { Form, Button, Container, Row, Col, Card } from "react-bootstrap";
import ImagePreview from "./ImagePreview";
import VideoPreview from "./VideoPreview";
import Dropzone from "react-dropzone";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import toastr from "toastr";
//Internal Modules
import { commercialCategories, setToInvalid, setToValid } from "../utils";
import { createRadioMaterial } from "../actions/radioAction";
import { Select, ProgressBar } from "./Utility-Component";
const videoMaxSize = 100000000; //byte
const acceptedFileTypes = "image/jpg, image/jpeg, video/mp4";
const acceptedFileTypesArray = acceptedFileTypes.split(",").map(item => {
  return item.trim();
});

const Online = props => {
  const [title, setTitle] = useState("");
  const [duration, setDuration] = useState("");
  const [commercialCategory, selectedCommercialCategory] = useState(0);
  const [material, setMaterial] = useState("");
  const [src, setSrc] = useState(null);
  const [option, selectedOption] = useState("Image");
  const [progressPercent, setProgressPercent] = useState(0);
  const [isInProgress, setInProgress] = useState(false);
  //Selelected Id's
  const [selectedCompanyId, setCompanyId] = useState(0);
  const [selectedBrandId, setBrandId] = useState(0);
  const [selectedMainCategoryId, setMainCategoryId] = useState(0);
  const [selectedSubCategoryId, setSubCategoryId] = useState(0);
  const [selectedSubSubCategoryId, setSubSubCategoryId] = useState(0);
  //List
  const [companyList, setCompanyList] = useState([]);
  const [brandList, setBrandList] = useState([]);
  const [mainCategoryList, setMainCategoryList] = useState([]);
  const [subCategoryList, setSubCategoryList] = useState([]);
  const [subSubCategoryList, setSubSubCategoryList] = useState([]);
  //Filtered List
  const [filteredSubCategoryList, setFilteredSubCategoryList] = useState([]);
  const [filteredSubSubCategoryList, setFilteredSubSubCategoryList] = useState(
    []
  );
  const [filteredBrandList, setFilteredBrandList] = useState([]);

  //File Verification
  const verifyFile = files => {
    if (files && files.length > 0) {
      const currentFile = files[0];
      const currentFileType = currentFile.type;
      const currentFileSize = currentFile.size;
      if (currentFileSize > videoMaxSize) {
        alert(
          "This file is not allowed." + currentFileSize + " bytes is too large"
        );
        return false;
      }
      if (!acceptedFileTypesArray.includes(currentFileType)) {
        alert("This file is not allowed. Only images/videos are allowed.");
        return false;
      }
      const fileType = currentFile.name.split(".").pop();
      return fileType;
    }
  };

  //File OnDrop event Handler
  const handleOnDrop = (files, rejectedFiles) => {
    if (rejectedFiles && rejectedFiles.length > 0) verifyFile(rejectedFiles);
    if (files && files.length > 0) {
      const isVerified = verifyFile(files);
      if (isVerified) {
        const currentFile = files[0];
        const myFileItemReader = new FileReader();
        myFileItemReader.addEventListener(
          "load",
          () => {
            setSrc(myFileItemReader.result);
            setMaterial(currentFile);
          },
          false
        );
        myFileItemReader.readAsDataURL(currentFile);
      }
    }
  };

  //Form Submit handler
  const handleSubmit = async (e) => {
    e.preventDefault();
    // if(!title ) setToInvalid(e.target.elements['title']);
    // if(!duration ) setToInvalid(e.target.elements['duration']);
    // if(!selectedCompanyId) setToInvalid(e.target.elements['companySelect']);
    // if(!selectedBrandId) setToInvalid(e.target.elements['brandSelect']);
    // if(!selectedMainCategoryId) setToInvalid(e.target.elements['mainCategorySelect']);
    // if(!selectedSubCategoryId) setToInvalid(e.target.elements['subCategorySelect']);
    // if(!selectedSubSubCategoryId) setToInvalid(e.target.elements['subSubCategorySelect']);
    // if(!commercialCategory) setToInvalid(e.target.elements['commercialCategorySelect']);
    // if(!material) setToInvalid(e.target.elements['material']);
    // if(
    //   !title || !duration || !selectedCompanyId || 
    //   !selectedBrandId || !commercialCategory || !selectedMainCategoryId || 
    //   !selectedSubCategoryId || !selectedSubSubCategoryId || !material
    // ) return false;
    let formData = new FormData();
    formData.set('title', title);
    formData.set('duration', duration); 
    formData.set('companyId', selectedCompanyId);
    formData.set('brandId', selectedBrandId);
    formData.set('commercialType', commercialCategory);
    formData.set('mainCategoryId', selectedMainCategoryId);
    formData.set('subCategoryId', selectedSubCategoryId);
    formData.set('subSubCategoryId', selectedSubSubCategoryId);
    formData.set('file', material);
    setInProgress(true);
    // props.createRadioMaterial(formData, percent => setProgressPercent(percent), () => {
    //   toastr.success('Radio Material Successfully Uploaded', 'System');
    //   handleResetForm();
    // });
    const test = {
        title: title,
        duration: duration,
        material: material
    }
    console.log(test);
    handleResetForm();

  };

  const handleResetForm = () => {
    setCompanyId(0);
    setBrandId(0);
    setMainCategoryId(0);
    setSubCategoryId(0);
    setSubSubCategoryId(0);
    setTitle("");
    setDuration("");
    selectedCommercialCategory(0);
    setInProgress(false);
    setSrc(null);
    setMaterial(null);
  };

  //Tile Onchange Handler
  const titleOnChangeHandler = e => {
    e.target.value != "" ? setToValid(e.target) : setToInvalid(e.target);
    const title = e.target.value;
    setTitle(title);
  };

  //Duration Onchange Handler
  const durationOnChangeHandler = e => {
    e.target.value != 0 ? setToValid(e.target) : setToInvalid(e.target);
    const duration = e.target.value;
    setDuration(duration);
  };

  //Comapany Select Onchange Hanlder
  const companyOnChangeHandler = e => {
    e.target.value != 0 ? setToValid(e.target) : setToInvalid(e.target);
    const companyId = e.target.value;
    setCompanyId(companyId);
    const filterBrands = brandList.filter(
      brand => brand.companyId == companyId
    );
    setFilteredBrandList(filterBrands);
  };

  //Brand Select Onchange Handler
  const brandOnChangeHandler = e => {
    e.target.value != 0 ? setToValid(e.target) : setToInvalid(e.target);
    const brandId = e.target.value;
    setBrandId(brandId);
  };

  //Main-Category Select Onchange Handler
  const mainCategoryOnChangeHandler = e => {
    e.target.value != 0 ? setToValid(e.target) : setToInvalid(e.target);
    const mainCategoryId = e.target.value;
    setMainCategoryId(mainCategoryId);
    const filertedSubCategory = subCategoryList.filter(
      subCategory => subCategory.mainCategoryId == mainCategoryId
    );
    setFilteredSubCategoryList(filertedSubCategory);
  };

  //Sub-Category Select Onchange Handler
  const subCategoryOnChangeHandler = e => {
    e.target.value != 0 ? setToValid(e.target) : setToInvalid(e.target);
    const subCategoryId = e.target.value;
    setSubCategoryId(subCategoryId);
    const filterSubSubCategory = subSubCategoryList.filter(
      subSubCategory => subSubCategory.subCategoryId == subCategoryId
    );
    setFilteredSubSubCategoryList(filterSubSubCategory);
  };

  //Sub-Sub-Category Select Onchange Handler
  const subSubCategoryOnChangeHandler = e => {
    e.target.value != 0 ? setToValid(e.target) : setToInvalid(e.target);
    const subSubCategoryId = e.target.value;
    setSubSubCategoryId(subSubCategoryId);
  };

  const commercialTypeOnChangeHandler = e => {
    e.target.value != 0 ? setToValid(e.target) : setToInvalid(e.target);
    const commercialCategory = e.target.value;
    selectedCommercialCategory(commercialCategory);
  };

  //User Effect lifecycle
  useEffect(() => {
    setCompanyList(props.companies);
    setBrandList(props.brands);
    setMainCategoryList(props.main_categories);
    setSubCategoryList(props.sub_categories);
    setSubSubCategoryList(props.sub_sub_categories);
  }, [
    props.companies,
    props.brands,
    props.main_categories,
    props.sub_categories,
    props.sub_sub_categories
  ]);
  let selected = option;

  return (
    <Container>
      <Form className="needs-validation" onSubmit={handleSubmit} noValidate>
        <Row>
          <Col sm={12} xl={6}>
            <Form.Group>
              <label htmlFor="titleInput" className="">
                Title
              </label>
              <input
                type="text"
                name="title"
                value={title}
                className="form-control"
                placeholder="Enter title"
                onChange={e => titleOnChangeHandler(e)}
                required
              />
              <div className="invalid-feedback">Please Fill Title Field</div>
            </Form.Group>
            <Form.Group>
              <label htmlFor="titleInput">Duration (in secs)</label>
              <input
                type="number"
                name="duration"
                value={duration}
                className="form-control"
                placeholder="00"
                onChange={e => durationOnChangeHandler(e)}
                required
              />
              <div className="invalid-feedback">Please Fill Duration Field</div>
            </Form.Group>
            <Form.Group>
              <label htmlFor="companySelect">Company</label>
              <Select
                items={companyList}
                value={selectedCompanyId}
                onChange={e => companyOnChangeHandler(e)}
                name={"companySelect"}
                key={"companySelect"}
              />
              <div className="invalid-feedback">Please Select Comapany</div>
            </Form.Group>
            <Form.Group>
              <label htmlFor="brandSelect">Brand</label>
              <Select
                items={filteredBrandList}
                value={selectedBrandId}
                disabled={selectedCompanyId === 0 && true}
                onChange={e => brandOnChangeHandler(e)}
                name={"brandSelect"}
                key={"brandSelect"}
              />
              <div className="invalid-feedback">Please Select Brand</div>
            </Form.Group>
            <Form.Group>
              <label htmlFor="mainCategorySelect">Main Category</label>
              <Select
                items={mainCategoryList}
                value={selectedMainCategoryId}
                onChange={e => mainCategoryOnChangeHandler(e)}
                name={"mainCategorySelect"}
                key={"mainCategorySelect"}
              />
              <div className="invalid-feedback">
                Please Select Main-Category
              </div>
            </Form.Group>
            <Form.Group>
              <label htmlFor="subCategorySelect">Sub Category</label>
              <Select
                disabled={selectedMainCategoryId == 0 && true}
                value={selectedSubCategoryId}
                items={filteredSubCategoryList}
                onChange={e => subCategoryOnChangeHandler(e)}
                name={"subCategorySelect"}
                key={"subCategorySelect"}
              />
              <div className="invalid-feedback">Please Select Sub-Category</div>
            </Form.Group>
            <Form.Group>
              <label htmlFor="subSubCategorySelect">Sub Sub Category</label>
              <Select
                value={selectedSubSubCategoryId}
                disabled={selectedSubCategoryId == 0 && true}
                items={filteredSubSubCategoryList}
                onChange={e => subSubCategoryOnChangeHandler(e)}
                name={"subSubCategorySelect"}
                key={"subSubCategorySelect"}
              />
              <div className="invalid-feedback">
                Please Select SubSub-Category
              </div>
            </Form.Group>
            <Form.Group>
              <label htmlFor="commercialCategorySelect">
                Commercial Category
              </label>
              <Select
                value={commercialCategory}
                items={commercialCategories}
                onChange={e => commercialTypeOnChangeHandler(e)}
                name={"commercialCategorySelect"}
                key={"commercialCategorySelect"}
                required
              />
              <div className="invalid-feedback">
                Please Select Commercial Type
              </div>
            </Form.Group>
          </Col>
          <Col sm={12} xl={6}>
            <Form.Group>
              <label htmlFor="commercialCategorySelect">
                Media type
              </label>

              <Row>
                <Col>
                  <Form.Check
                    type="radio"
                    selected="selected"
                    value="Image"
                    label="Image"
                    checked={option === "Image"}
                    onChange={e => selectedOption(e.currentTarget.value)}
                  />
                </Col>
                <Col>
                  <Form.Check
                    type="radio"
                    selected="selected"
                    value="Video"
                    label="Video"
                    checked={option === "Video"}
                    onChange={e => selectedOption(e.currentTarget.value)}
                  />
                </Col>
              </Row>
            </Form.Group>
            {selected == "Image" ? (
              <ImagePreview
                handleOnDrop={handleOnDrop}
                src={src}
                material={material}
              />
            ) : (
              <VideoPreview
                handleOnDrop={handleOnDrop}
                src={src}
                material={material}
              />
            )}
          </Col>
        </Row>
        <hr />
        <Row>
          {isInProgress ? (
            <Col>
              <label htmlFor="material">Progress</label>
              <ProgressBar percent={progressPercent} />
            </Col>
          ) : (
            <Fragment>
              <Col xl={9} sm={6}></Col>
              <Col xl={3} sm={6}>
                <Button variant="default" onClick={() => handleResetForm()}>
                  Clear
                </Button>
                <Button variant="primary" type="submit">
                  Submit
                </Button>
              </Col>
            </Fragment>
          )}
        </Row>
      </Form>
    </Container>
  );
};

export default Online;
