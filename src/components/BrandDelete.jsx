import React, { useState, useEffect } from "react";
import { setToInvalid, setToValid } from "../utils";
import { LoadingText } from "./Utility-Component";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { deleteBrand } from "../actions/brandAction";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol,
  MDBCardHeader
} from "mdbreact";

const BrandDelete = props => {
  const [brandId, setBrandId] = useState("");
  const [inProcess, setInProcess] = useState(false);

  
  useEffect(() => {
    setBrandId(props.brandDetails.id);
  }, [props.brandDetails]);

  const handleSubmit = e => {
    e.preventDefault();

    if (!brandId) return false;
    setInProcess(true);
    props.deleteBrand(
      {
        id: brandId
      },
      () => {
        setInProcess(false);
        toastr.success("Brand Successfully Deleted");
        closeSelfHandler();
      }
    );
  };

  const closeSelfHandler = () => {
    props.handleDeleteModalOpen();
  };

  //console.log(props.companyDetails);
  return (
    <MDBModal
      isOpen={props.isDeleteModalOpen}
      toggle={props.handleDeleteModalOpen}
      full-height
      position="top"
    >
      <MDBModalHeader
        toggle={closeSelfHandler}
        tag="h4"
        className="font-weight-bold text-uppercase"
      >
        {props.brandDetails.name}
      </MDBModalHeader>
      <form className="needs-validation" onSubmit={handleSubmit} noValidate>
        <MDBModalBody>
          <MDBRow>
            <MDBCol>
              <p>
                Are you sure you want to delete{" "}
                <font style={{ fontWeight: "900", color: "red" }}>
                  {" "}
                  {props.brandDetails.name}
                </font>
                ?
              </p>
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
            No
          </MDBBtn>
          <MDBBtn color="light-green" type="submit">
            <LoadingText text={"Yes"} loading={inProcess} />
          </MDBBtn>
        </MDBModalFooter>
      </form>
    </MDBModal>
  );
};
BrandDelete.propTypes = {
  deleteBrand: PropTypes.func.isRequired
};
export default connect(null, { deleteBrand })(BrandDelete);
