import React, { useState } from "react";
import TV from "./Form-upload-tv";
import Radio from "./Form-upload-radio";
import Ooh from "./Form-upload-ooh";
import Online from "./Form-upload-online";
import Print_Ad from "./Form-upload-print-ad";
import { mediums } from "../utils";
import { Row, Col } from "react-bootstrap";

const FormUpload = props => {
  //const initialValue = [{ id: 2, type: "Radio" }];
  const [medium, selectedMedium] = useState(2);
  console.log(medium);
  return (
    <div className="card-body">
      <Row>
        <Col>
          <div className="form-group">
            <label>Media: </label>
            <select
              required
              className="form-control col-lg-12"
              value={medium}
              onChange={e => selectedMedium(e.target.value)}
            >
              {
              mediums.map(function(mediumOptions) {
                return (
                  <option key={mediumOptions.id} value={mediumOptions.id}>
                    {mediumOptions.type}
                  </option>
                );
              })}
            </select>
          </div>
        </Col>
        <Col>
        </Col>
      </Row>
      <hr />
      {medium == 1 ? (
        <TV />
      ) : medium == 2 ? (
        <Radio /> 
      ) : medium == 3 ? (
        <Ooh />
      ) : medium == 4 ? (
        <Online />
      ) : (
        <Print_Ad />
      )}
    </div>
  );
};

export default FormUpload;
