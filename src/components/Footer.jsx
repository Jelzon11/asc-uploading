import React, { Component } from "react";

export default class CustomSideNavbar extends Component {
  render() {
    return (
      <footer className="main-footer">
        <strong>
          Copyright &copy; 2019{" "}
          <a href="#">
            {" "}
            <font style={{ color: "red" }}> ASC Materials </font>
          </a>
          .
        </strong>
        All rights reserved.
        <div className="float-right d-none d-sm-inline-block">
          <b>Version</b> 0.0.1
        </div>
      </footer>
    );
  }
}
