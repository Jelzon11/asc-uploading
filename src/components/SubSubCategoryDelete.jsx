import React, { useState, useEffect } from "react";
import { setToInvalid, setToValid } from "../utils";
import { LoadingText } from "./Utility-Component";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { deleteSubSubCategory } from "../actions/subSubCategoryAction";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol,
  MDBCardHeader
} from "mdbreact";

const SubSubCategoryDelete = props => {
  const [subSubCategoryId, setSubSubCategoryId] = useState("");
  const [inProcess, setInProcess] = useState(false);

  
  useEffect(() => {
    setSubSubCategoryId(props.subSubCategoryDetails.id);
  }, [props.subSubCategoryDetails]);

  const handleSubmit = e => {
    e.preventDefault();

    if (!subSubCategoryId) return false;
    setInProcess(true);
    props.deleteSubSubCategory(
      {
        id: subSubCategoryId
      },
      () => {
        setInProcess(false);
        toastr.success("Brand Successfully Deleted");
        closeSelfHandler();
      }
    );
  };

  const closeSelfHandler = () => {
    props.handleDeleteModalOpen();
  };

  return (
    <MDBModal
      isOpen={props.isDeleteModalOpen}
      toggle={props.handleDeleteModalOpen}
      full-height
      position="top"
    >
      <MDBModalHeader
        toggle={closeSelfHandler}
        tag="h4"
        className="font-weight-bold text-uppercase"
      >
        {props.subSubCategoryDetails.name}
      </MDBModalHeader>
      <form className="needs-validation" onSubmit={handleSubmit} noValidate>
        <MDBModalBody>
          <MDBRow>
            <MDBCol>
              <p>
                Are you sure you want to delete{" "}
                <font style={{ fontWeight: "900", color: "red" }}>
                  {" "}
                  {props.subSubCategoryDetails.name}
                </font>
                ?
              </p>
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
            No
          </MDBBtn>
          <MDBBtn color="light-green" type="submit">
            <LoadingText text={"Yes"} loading={inProcess} />
          </MDBBtn>
        </MDBModalFooter>
      </form>
    </MDBModal>
  );
};
SubSubCategoryDelete.propTypes = {
  deleteSubSubCategory: PropTypes.func.isRequired
};
export default connect(null, { deleteSubSubCategory })(SubSubCategoryDelete);
