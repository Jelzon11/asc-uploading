import React, { } from 'react';
import FormUpload from './Form-upload';
import { Provider } from 'react-redux';
import store from '../store';
import { isTokenExist } from '../utils';
import {  Redirect  } from 'react-router-dom'
import { isAuthorize } from '../utils';
import UnAuthorize from './UnAuthorize';
const _uploadPageId = '10a2b656-6cf2-42e1-875d-5433004aaa02';

const Content = () => {
  return isAuthorize(_uploadPageId) ? (
      <div className="content-wrapper">
        <div className="content">
          <div className="container-fluid">
            <div className="col-md-12">
              <br />
              <div className="card">
                <div className="card-header">
                  <h3 className="card-title"> <b>Meta Data</b></h3>
                </div>
                <FormUpload />
              </div>
            </div>
            <br />
          </div>
        </div>
      </div>
  ) : <UnAuthorize />
}

export default Content;

