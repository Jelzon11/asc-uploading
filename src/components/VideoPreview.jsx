import React, { Fragment } from "react";
import Dropzone from "react-dropzone";
import { Form, Card } from "react-bootstrap";

const videoMaxSize = 100000000; //byte
const acceptedFileTypes = "video/mp4";
const acceptedFileTypesArray = acceptedFileTypes.split(",").map(item => {
  return item.trim();
});


const VideoPreview = (props) => {
  const { src, material } = props;

  return (
    <Form.Group>
      <label htmlFor="material">Choose File</label>
      <Dropzone
        onDrop={props.handleOnDrop}
        accept={acceptedFileTypes}
        multiple={false}
        maxSize={videoMaxSize}
      >
        {({ getRootProps, getInputProps }) => (
          <Fragment>
            <div
              style={{
                borderColor: "lightgray",
                borderStyle: "dashed",
                textAlignment: "center",
                margin: "5px 15px",
                height: "120px"
              }}
            >
              <section className={"is-invalid"}>
                <div {...getRootProps()} style={{ width: "100%" }}>
                  <input
                    type={"file"}
                    name={"material"}
                    key={"material"}
                    required
                    {...getInputProps()}
                  />
                  <p
                    style={{
                      color: "lightgray",
                      fontWeight: "bold",
                      textTransform: "uppercase",
                      height: "120px",
                      width: "100%",
                      textAlign: "center"
                    }}
                  >
                    Drag 'n' drop a video here, or click to select
                  </p>
                  <div className="invalid-feedback">
                    Please Attach Video File
                  </div>
                </div>
              </section>
            </div>
          </Fragment>
        )}
      </Dropzone>
      <hr />
      {src && material && (
        <Fragment>
          <label>Preview</label>
          <div style={{ padding: "1rem" }}>
            <Card>
              <Card.Body>
                <Card.Text>
                  Title :{" "}
                  <label style={{ color: "#3283a8" }}>{material.name}</label>
                </Card.Text>
                <Card.Text>
                  Type:{" "}
                  <label style={{ color: "#3283a8" }}>{material.type}</label>
                </Card.Text>
                {src && (
                  <video src={src} controls style={{ width: "100%" }} ></video>
                )}
              </Card.Body>
            </Card>
          </div>
        </Fragment>
      )}
    </Form.Group>
  );
};
export default VideoPreview;