import React, { useState, Fragment, useEffect } from "react";
import { setToInvalid, setToValid } from "../utils";
import { Select, LoadingText } from "./Utility-Component";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { updateUser } from "../actions/userAction";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol
} from "mdbreact";

const UserEdit = props => {
  const [toEdit, setToEdit] = useState(true);
  const [firstName, setFirstName] = useState("");
  const [middleName, setMiddleName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [contact, setContact] = useState("");
  const [userId, setUserId] = useState("");
  const [inProcess, setInProcess] = useState(false);
  const [selectedRoleId, setSelectedRoleId] = useState();
  const [roleList, setRoleList] = useState([]);
  const [selectedRole, setSelectedRole] = useState({});

  const handleClick = () => {
    setToEdit(!toEdit);
  };
  const handleRole = e => {
    //console.log(roleList);
    const selectedRole = roleList.filter(item => {
      return item.id == e.target.value;
    });
    setSelectedRole(selectedRole[0])
   
    e.target.value != 0 ? setToValid(e.target) : setToInvalid(e.target);
  };
  const handleFirstNameChange = e => {
    setFirstName(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };
  const handleMiddleNameChange = e => {
    setMiddleName(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };
  const handleLastNameChange = e => {
    setLastName(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };
  const handleContactChange = e => {
    setContact(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };
  const handleEmailChange = e => {
    setEmail(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };

  const handleSubmit = e => {
    e.preventDefault();
    if (!firstName) setToInvalid(e.target.elements["firstName"]);
    if (!lastName) setToInvalid(e.target.elements["lastName"]);
    if (!middleName) setToInvalid(e.target.elements["middleName"]);
    if (!email) setToInvalid(e.target.elements["email"]);
    if (!contact) setToInvalid(e.target.elements["contact"]);
    if (!selectedRole) setToInvalid(e.target.elements["roleSelect"]);
    if (!firstName || !userId || !middleName || !lastName || !email || !contact || !selectedRole)
      return false;
    const update_user = {
      firstName: firstName,
      lastName: lastName,
      middleName: middleName,
      email: email,
      contact: contact,
      roleId: selectedRole.id,
      roleDetails: selectedRole,
      id: userId
    };
    setInProcess(true);
    props.updateUser(update_user, () => {
      setInProcess(false);
      toastr.success("User Successfully Updated");
      closeSelfHandler();
    });
  };

  useEffect(() => {
    setFirstName(props.userDetails.firstName);
    setLastName(props.userDetails.lastName);
    setMiddleName(props.userDetails.middleName);
    setEmail(props.userDetails.email);
    setContact(props.userDetails.contact);
    setUserId(props.userDetails.id);
    setRoleList(props.roles);
  }, [props.userDetails]);

  const closeSelfHandler = () => {
    props.handleEditModalOpen();
    setToEdit(!toEdit);
  };
  const closeHandler = () => {
    props.handleEditModalOpen();
    setToEdit(true);
  };

  const viewMode = () => {
    return (
      <Fragment>
        <MDBModalBody>
          <MDBRow>
            <MDBCol>
              <label>First name</label>
              <input
                value={props.userDetails.firstName}
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol>
              <label>Middle name</label>
              <input
                value={props.userDetails.middleName}
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol>
              <label>Last name</label>
              <input
                value={props.userDetails.lastName}
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol>
              <label>Email</label>
              <input
                value={props.userDetails.email}
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol>
              <label>Contact</label>
              <input
                value={props.userDetails.contact}
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol>
              <label>Role</label>
              <input
                value={
                  props.userDetails &&
                  props.userDetails.roleDetails &&
                  props.userDetails.roleDetails.name
                }
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="light-green" onClick={handleClick} type="submit">
            {" "}
            Edit
          </MDBBtn>
        </MDBModalFooter>
      </Fragment>
    );
  };
  const editMode = () => {
    return (
      <Fragment>
        <form className="needs-validation" onSubmit={handleSubmit} noValidate>
          <MDBModalBody>
            <MDBRow>
              <MDBCol>
                <label>First Name</label>
                <input
                  name={"firstName"}
                  type="text"
                  value={firstName}
                  className="form-control"
                  onChange={e => handleFirstNameChange(e)}
                  required
                />
                <div className="invalid-feedback">
                  Please provide first name
                </div>
              </MDBCol>
            </MDBRow>
            <MDBRow>
              <MDBCol>
                <label>Middle Name</label>
                <input
                  name={"middleName"}
                  type="text"
                  value={middleName}
                  className="form-control"
                  onChange={e => handleMiddleNameChange(e)}
                  required
                />
                <div className="invalid-feedback">
                  Please provide middle name
                </div>
              </MDBCol>
            </MDBRow>
            <MDBRow>
              <MDBCol>
                <label>Last Name</label>
                <input
                  name={"lastName"}
                  type="text"
                  value={lastName}
                  className="form-control"
                  onChange={e => handleLastNameChange(e)}
                  required
                />
                <div className="invalid-feedback">Please provide last name</div>
              </MDBCol>
            </MDBRow>
            <MDBRow>
              <MDBCol>
                <label>Email</label>
                <input
                  name={"email"}
                  type="text"
                  value={email}
                  className="form-control"
                  onChange={e => handleEmailChange(e)}
                  required
                />
                <div className="invalid-feedback">Please provide email</div>
              </MDBCol>
            </MDBRow>
            <MDBRow>
              <MDBCol>
                <label>Contact</label>
                <input
                  name={"contact"}
                  type="text"
                  value={contact}
                  className="form-control"
                  onChange={e => handleContactChange(e)}
                  required
                />
                <div className="invalid-feedback">Please provide contact</div>
              </MDBCol>
            </MDBRow>
            <MDBRow style={{ marginBottom: "1rem" }}>
              <MDBCol>
                <label> Select Role </label>
                {
                  <Select
                    items={roleList}
                    noChooseOption={true}
                    value={ props.userDetails &&
                      props.userDetails.roleDetails &&
                      props.userDetails.roleId
                    }
                    onChange={e => handleRole(e)}
                    name={"roleSelect"}
                  />
                }
              </MDBCol>
            </MDBRow>
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
              Cancel
            </MDBBtn>
            <MDBBtn color="light-green" type="submit">
              <LoadingText text={"Submit"} loading={inProcess} />
            </MDBBtn>
          </MDBModalFooter>
        </form>
      </Fragment>
    );
  };
  return (
    <MDBModal
      isOpen={props.isEditModalOpen}
      toggle={props.handleEditModalOpen}
      full-height
      position="top"
    >
      <MDBModalHeader toggle={closeHandler}>
        {props.userDetails.name}
      </MDBModalHeader>

      {toEdit ? viewMode() : editMode()}
    </MDBModal>
  );
};
UserEdit.propTypes = {
  updateUser: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  roles: state.roles.items
});
export default connect(mapStateToProps, { updateUser })(UserEdit);
