import React, { Fragment, useState } from "react";
import Materials from "./Materials";
import UnAuthorize from "./UnAuthorize";
import Error404 from "./Error404";
import Categories from "./Categories";
import Companies from "./Companies";
import Brands from "./Brands";
import Users from "./Users";
import { Route, Redirect, Switch } from "react-router-dom";
import CustomSideNavbar from "./CustomSideNavbar";
import Content from "./Content";
//import Roles from "./Roles";
import Products from "./Products";
import { connect } from "react-redux";
import { fetchBrands } from "../actions/brandAction";
import { fetchCompanies } from "../actions/companyAction";
import { fetchMainCategories } from "../actions/mainCategoryAction";
import { fetchSubCategories } from "../actions/subCategoryAction";
import { fetchSubSubCategories } from "../actions/subSubCategoryAction";
import { fetchUsers, fetchMyInfo } from "../actions/userAction";
import { fetchRadioMaterials } from "../actions/radioAction";
import { fetchComponents } from "../actions/getComponentsAction";
import { fetchRoles } from "../actions/roleAction";
import { fetchProducts } from '../actions/productAction';

import CustomNavbar from "./CustomNavbar";
import Footer from "./Footer";
import { PageSpinner } from "./Utility-Component";
import { isAuthorize, decrypted } from "../utils";
import UserLogs from "./UserLogs";

// const _uploadPageId = '10a2b656-6cf2-42e1-875d-5433004aaa02';
// const _userPageId = 'f83a4623-a734-427f-9e72-159a7d1a13fd';
// const _materialPageId = '81cb6447-4aab-40c6-a0f6-7561046fded5';
// const _categoryPageId = '17f9d564-35f9-4bd5-9021-f66b01e2673e';
// const _brandPageId = 'd356a4f4-5e17-4b86-96da-f7c1718b3024';
let componentKey = 1;

const ContentHolder = props => {
  decrypted();
  props.fetchMyInfo();
  props.fetchBrands();
  props.fetchCompanies();
  props.fetchMainCategories();
  props.fetchSubCategories();
  props.fetchSubSubCategories();
  props.fetchRadioMaterials();
  props.fetchUsers();
  props.fetchComponents();
  props.fetchRoles();
  props.fetchProducts();
  //componentKey++;

  return (
    <Fragment>
      <CustomNavbar />
      <Fragment>
        <CustomSideNavbar />
        <Switch>
          <Route exact path="/upload" component={Content} />
          <Route exact path="/materials" component={Materials} />
          <Route exact path="/categories" component={Categories} />
          <Route exact path="/companies" component={Companies} />} />
          <Route exact path="/brands" component={Brands} />
          <Route exact path="/users" component={Users} />
          <Route exact path="/products" component={Products} />
          <Route exact path="/userLogs/:userId" component={UserLogs} />
          <Route component={Error404} />
          
          {/* <Route exact path="/unauthorize" component={UnAuthorize} /> */}
        </Switch>
      </Fragment>
      <Footer />
    </Fragment>
  );
};

export default connect(null, {
  fetchBrands,
  fetchCompanies,
  fetchMainCategories,
  fetchSubCategories,
  fetchSubSubCategories,
  fetchUsers,
  fetchRadioMaterials,
  fetchComponents,
  fetchMyInfo,
  fetchRoles,
  fetchProducts
})(ContentHolder);
