import React, { useState, useEffect } from "react";
import { LoadingText } from "./Utility-Component";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { deleteSubCategory } from "../actions/subCategoryAction";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol,
  MDBCardHeader
} from "mdbreact";

const SubCategoryDelete = props => {
  const [subCategoryId, setSubCategoryId] = useState("");
  const [inProcess, setInProcess] = useState(false);

  
  useEffect(() => {
    setSubCategoryId(props.subCategoryDetails.id);
  }, [props.subCategoryDetails]);

  const handleSubmit = e => {
    e.preventDefault();

    if (!subCategoryId) return false;
    setInProcess(true);
    props.deleteSubCategory(
      {
        id: subCategoryId
      },
      () => {
        setInProcess(false);
        toastr.success("Brand Successfully Deleted");
        closeSelfHandler();
      }
    );
  };

  const closeSelfHandler = () => {
    props.handleDeleteModalOpen();
  };

  return (
    <MDBModal
      isOpen={props.isDeleteModalOpen}
      toggle={props.handleDeleteModalOpen}
      full-height
      position="top"
    >
      <MDBModalHeader
        toggle={closeSelfHandler}
        tag="h4"
        className="font-weight-bold text-uppercase"
      >
        {props.subCategoryDetails.name}
      </MDBModalHeader>
      <form className="needs-validation" onSubmit={handleSubmit} noValidate>
        <MDBModalBody>
          <MDBRow>
            <MDBCol>
              <p>
                Are you sure you want to delete{" "}
                <font style={{ fontWeight: "900", color: "red" }}>
                  {" "}
                  {props.subCategoryDetails.name}
                </font>
                ?
              </p>
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
            No
          </MDBBtn>
          <MDBBtn color="light-green" type="submit">
            <LoadingText text={"Yes"} loading={inProcess} />
          </MDBBtn>
        </MDBModalFooter>
      </form>
    </MDBModal>
  );
};
SubCategoryDelete.propTypes = {
  deleteSubCategory: PropTypes.func.isRequired
};
export default connect(null, { deleteSubCategory })(SubCategoryDelete);
