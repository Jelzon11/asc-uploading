import React, { Fragment } from "react";
import Dropzone from "react-dropzone";
import { Form, Card } from "react-bootstrap";

const imageMaxSize = 100000000; //byte
const acceptedFileTypes = "image/jpg, image/jpeg";
const acceptedFileTypesArray = acceptedFileTypes.split(",").map(item => {
  return item.trim();
});

const ImagePreview = props => {
  const { src, material } = props;
  //console.log(materials);
  return (
    <Form.Group>
      <label htmlFor="material">Choose File</label>
      <Dropzone
        onDrop={props.handleOnDrop}
        accept={acceptedFileTypes}
        multiple={false}
        maxSize={imageMaxSize}
      >
        {({ getRootProps, getInputProps }) => (
          <Fragment>
            <div
              style={{
                borderColor: "lightgray",
                borderStyle: "dashed",
                textAlignment: "center",
                margin: "5px 15px",
                height: "120px"
              }}
            >
              <section className={"is-invalid"}>
                <div {...getRootProps()} style={{ width: "100%" }}>
                  <input
                    type={"file"}
                    name={"material"}
                    key={"material"}
                    required
                    {...getInputProps()}
                  />
                  <p
                    style={{
                      color: "lightgray",
                      fontWeight: "bold",
                      textTransform: "uppercase",
                      height: "120px",
                      width: "100%",
                      textAlign: "center"
                    }}
                  >
                    Drag 'n' drop an Image here, or click to select
                  </p>
                  <div className="invalid-feedback">
                    Please Attach Image File
                  </div>
                </div>
              </section>
            </div>
          </Fragment>
        )}
      </Dropzone>
      <hr />
      {src && material && (
        <Fragment>
          <label>Preview</label>
          <div style={{ padding: "1rem" }}>
            <Card>
              <Card.Body>
                <Card.Text>
                  Title :{" "}
                  <label style={{ color: "#3283a8" }}>{material.name}</label>
                </Card.Text>
                <Card.Text>
                  Type:{" "}
                  <label style={{ color: "#3283a8" }}>{material.type}</label>
                </Card.Text>
                {src && (
                  <img src={src} style={{ width: "100%" }} />
                )}
              </Card.Body>
            </Card>
          </div>
        </Fragment>
      )}
    </Form.Group>
  );
};
export default ImagePreview;
