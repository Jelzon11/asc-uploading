import React, { Fragment, useState, useEffect } from "react";
import SubSubCategoryModalForm from "./Categories-sub-sub-modal";
import SubSubCategoryEdit from "./SubSubCategoryEdit";
import SubSubCategoryDelete from "./SubSubCategoryDelete";
import {
  MDBDataTable,
  MDBCard,
  MDBCardHeader,
  MDBCardBody,
  MDBBtn,
  MDBIcon
} from "mdbreact";
import { connect } from "react-redux";
import moment from "moment";

const SubSubCategories = props => {
  const [subSubCategories, setSubSubCategories] = useState([]);
  const [isModalOpen, setModalOpen] = useState(false);
  const [isEditModalOpen, setEditModalOpen] = useState(false);
  const [isDeleteModalOpen, setDeleteModalOpen] = useState(false);
  const [isCreatedSuccess, setCreatedSuccess] = useState(false);
  const [subSubCategoryDetails, setSubSubCategoryDetails] = useState("");

  const columns = () => {
    return [
      {
        label: "Name",
        field: "name",
        sort: "asc",
        width: 500
      },
      {
        label: "Sub Category",
        field: "subCategoryName",
        sort: "asc",
        width: 100
      },
      {
        label: "Main Category",
        field: "mainCategoryName",
        sort: "asc",
        width: 100
      },
      {
        label: "Last Updated",
        field: "updatedTimestamp",
        sort: "asc",
        width: 100
      },
      {
        label: "Created Date",
        field: "createdTimestamp",
        sort: "asc",
        width: 100
      },
      {
        label: "Action",
        field: "action",
        sort: "asc",
        width: 100
      }
    ];
  };

  const handleModalOpen = () => {
    if (isModalOpen == false) {
      setModalOpen(true);
    } else {
      setModalOpen(false);
    }
  };

  const handleDeleteModalOpen = subSubCategory => {
    if (isDeleteModalOpen == false) {
      setDeleteModalOpen(true);
      setSubSubCategoryDetails(subSubCategory);
    } else {
      setDeleteModalOpen(false);
    }
  };

  const handleEditModalOpen = subSubCategory => {
    if (isEditModalOpen == false) {
      setEditModalOpen(true);
      setSubSubCategoryDetails(subSubCategory);
    } else {
      setEditModalOpen(false);
    }
  };

  const successToastHandler = () => {
    setCreatedSuccess(true);
  };

  const renderData = data => {
    const newData = data.map(item => {
      return {
        name: item.name,
        subCategoryName: item.subCategoryDetails.name,
        mainCategoryName: item.subCategoryDetails.mainCategoryDetails.name,
        updatedTimestamp: moment.unix(item["updatedTimestamp"]).calendar(),
        createdTimestamp: moment.unix(item["createdTimestamp"]).format("L"),
        action: (
          <Fragment>
            <center>
            <MDBBtn
              id={item.id}
              color="light-blue"
              size="sm"
              onClick={() => handleEditModalOpen(item)}
            >
              <MDBIcon icon="pencil-alt" />
            </MDBBtn>
            <MDBBtn
              id={item.id}
              color="blue-grey"
              size="sm"
              onClick={() => handleDeleteModalOpen(item)}
            >
              <MDBIcon icon="trash-alt" />
            </MDBBtn>
            </center>
          </Fragment>
        )
      };
    });
    return newData;
  };

  useEffect(() => {
    setSubSubCategories(props.sub_sub_categories);
  }, [props.sub_sub_categories]);

  return (
    <Fragment>
      <SubSubCategoryModalForm
        isModalOpen={isModalOpen}
        successToastHandler={successToastHandler}
        handleModalOpen={handleModalOpen}
      />
      <SubSubCategoryEdit
        isEditModalOpen={isEditModalOpen}
        handleEditModalOpen={handleEditModalOpen}
        subSubCategoryDetails={subSubCategoryDetails}
      />
      <SubSubCategoryDelete
        isDeleteModalOpen={isDeleteModalOpen}
        handleDeleteModalOpen={handleDeleteModalOpen}
        subSubCategoryDetails={subSubCategoryDetails}
      />
      <MDBCard style={{ padding: "1rem" }}>
        <MDBCardHeader tag="h4" className="font-weight-bold text-uppercase">
          Sub-Sub Category
        </MDBCardHeader>
        <div className="d-flex align-items-start flex-column bd-highlight example-parent">
          <MDBBtn onClick={handleModalOpen} color="default">
            Add New
          </MDBBtn>
        </div>
        <hr />
        <MDBCardBody>
          <MDBDataTable
            smallselfCloseHandler
            bordered
            hover
            order={["name", "desc"]}
            disableRetreatAfterSorting={true}
            sortable={true}
            responsive={true}
            searchLabel={"Search..."}
            responsiveLg={true}
            responsiveXl={true}
            noBottomColumns={true}
            data={{
              rows: renderData(subSubCategories),
              columns: columns()
            }}
          />
        </MDBCardBody>
      </MDBCard>
    </Fragment>
  );
};

const mapStateToProps = state => ({
  sub_sub_categories: state.sub_sub_categories.items
});

export default connect(mapStateToProps)(SubSubCategories);
