import React, { useState, Fragment, useEffect } from "react";
import { setToInvalid, setToValid } from "../utils";
import { LoadingText } from "./Utility-Component";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { updateSubSubCategory } from "../actions/subSubCategoryAction";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol
} from "mdbreact";

const SubSubCategoryEdit = props => {
  const [toEdit, setToEdit] = useState(true);
  const [subSubCategoryName, setSubSubCategoryName] = useState("");
  const [subSubCategoryId, setSubSubCategoryId] = useState("");
  const [inProcess, setInProcess] = useState(false);

  const handleClick = () => {
    setToEdit(!toEdit);
  };
  const handleSubSubCategoryNameChange = e => {
    setSubSubCategoryName(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };
  const handleSubmit = e => {
    e.preventDefault();
    if (!subSubCategoryName) setToInvalid(e.target.elements["subSubCategoryName"]);
    if (!subSubCategoryName || !subSubCategoryId) return false;
    const update_subSubCategory = {
      name: subSubCategoryName,
      id: subSubCategoryId
    };
    setInProcess(true);
    props.updateSubSubCategory(update_subSubCategory, () => {
      setInProcess(false);
      toastr.success("Sub Category Successfully Updated");
      closeSelfHandler();
    });
  };

  useEffect(() => {
    setSubSubCategoryName(props.subSubCategoryDetails.name);
    setSubSubCategoryId(props.subSubCategoryDetails.id);
  }, [props.subSubCategoryDetails]);

  const closeSelfHandler = () => {
    props.handleEditModalOpen();
    setToEdit(!toEdit);
  };
  const closeHandler = () => {
    props.handleEditModalOpen();
    setToEdit(true);
  };

  const viewMode = () => {
    return (
      <Fragment>
        <MDBModalBody>
          <MDBRow>
            <MDBCol>
              <label>Main Category</label>
              <input
                value={
                  props.subSubCategoryDetails &&
                  props.subSubCategoryDetails.subCategoryDetails &&
                  props.subSubCategoryDetails.subCategoryDetails.mainCategoryDetails &&
                  props.subSubCategoryDetails.subCategoryDetails.mainCategoryDetails.name
                }
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol>
              <label>Sub Category</label>
              <input
                value={
                  props.subSubCategoryDetails &&
                  props.subSubCategoryDetails.subCategoryDetails &&
                  props.subSubCategoryDetails.subCategoryDetails.name
                }
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol>
              <label>Sub-sub Category</label>
              <input
                value={props.subSubCategoryDetails.name}
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="light-green" onClick={handleClick} type="submit">
            {" "}
            Edit
          </MDBBtn>
        </MDBModalFooter>
      </Fragment>
    );
  };
  const editMode = () => {
    return (
      <Fragment>
        <form className="needs-validation" onSubmit={handleSubmit} noValidate>
          <MDBModalBody>
            <MDBRow>
              <MDBCol>
                <label>Sub-Sub Category</label>
                <input
                  name={"subSubCategoryName"}
                  type="text"
                  value={subSubCategoryName}
                  className="form-control"
                  onChange={e => handleSubSubCategoryNameChange(e)}
                  required
                />
                <div className="invalid-feedback">
                  Please provide sub-sub category name
                </div>
              </MDBCol>
            </MDBRow>
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
              Cancel
            </MDBBtn>
            <MDBBtn color="light-green" type="submit">
              <LoadingText text={"Submit"} loading={inProcess} />
            </MDBBtn>
          </MDBModalFooter>
        </form>
      </Fragment>
    );
  };
  return (
    <MDBModal
      isOpen={props.isEditModalOpen}
      toggle={props.handleEditModalOpen}
      full-height
      position="top"
    >
      <MDBModalHeader toggle={closeHandler}>
        {props.subSubCategoryDetails.name}
      </MDBModalHeader>

      {toEdit ? viewMode() : editMode()}
    </MDBModal>
  );
};
SubSubCategoryEdit.propTypes = {
  updateSubSubCategory: PropTypes.func.isRequired
};
export default connect(null, { updateSubSubCategory })(SubSubCategoryEdit);
