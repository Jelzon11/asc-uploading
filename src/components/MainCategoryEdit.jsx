import React, { useState, Fragment, useEffect } from "react";
import { setToInvalid, setToValid } from "../utils";
import { LoadingText } from "./Utility-Component";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { updateMainCategory } from "../actions/mainCategoryAction";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol
} from "mdbreact";


const MainCategoryEdit = props => {
  const [toEdit, setToEdit] = useState(true);
  const [mainCategoryName, setMainCategoryName] = useState("");
  const [mainCategoryId, setMainCategoryId] = useState("");
  const [inProcess, setInProcess] = useState(false);

  const handleClick = () => {
    setToEdit(!toEdit);
  };
  const handleMainCategoryNameChange = e => {
    setMainCategoryName(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };
  const handleSubmit = e => {
    e.preventDefault();
    if (!mainCategoryName) setToInvalid(e.target.elements["mainCategoryName"]);
    if (!mainCategoryName || !mainCategoryId) return false;
    const update_mainCategory = {
      name: mainCategoryName,
      id: mainCategoryId
    };
    setInProcess(true);
    props.updateMainCategory(update_mainCategory, () => {
      setInProcess(false);
      toastr.success("Main Category Successfully Updated");
      closeSelfHandler();
    });
  };

  useEffect(() => {
    setMainCategoryName(props.mainCategoryDetails.name);
    setMainCategoryId(props.mainCategoryDetails.id);

  }, [props.mainCategoryDetails]);

  const closeSelfHandler = () => {
    props.handleEditModalOpen();
    setToEdit(!toEdit);
  };
  const closeHandler = () => {
    props.handleEditModalOpen();
    setToEdit(true);
  };

  const viewMode = () => {
    return (
      <Fragment>
        <MDBModalBody>
          <MDBRow>
            <MDBCol>
              <label>Main Category</label>
              <input
                value={props.mainCategoryDetails.name}
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="light-green" onClick={handleClick} type="submit">
            {" "}
            Edit
          </MDBBtn>
        </MDBModalFooter>
      </Fragment>
    );
  };
  const editMode = () => {
    return (
     
      <Fragment>
        <form className="needs-validation" onSubmit={handleSubmit} noValidate>
          <MDBModalBody>
            <MDBRow>
              <MDBCol>
                <label>Company</label>
                <input
                  name={"mainCategoryName"}
                  type="text"
                  value={mainCategoryName}
                  className="form-control"
                  onChange={e => handleMainCategoryNameChange(e)}
                  required
                />
                <div className="invalid-feedback">
                  Please provide main category name
                </div>
              </MDBCol>
            </MDBRow>
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
              Cancel
            </MDBBtn>
            <MDBBtn color="light-green" type="submit">
              <LoadingText text={"Submit"} loading={inProcess} />
            </MDBBtn>
          </MDBModalFooter>
        </form>
      </Fragment>
    );
  };
  return (
    <MDBModal
      isOpen={props.isEditModalOpen}
      toggle={props.handleEditModalOpen}
      full-height
      position="top"
    >
      <MDBModalHeader toggle={closeHandler}>
        {props.mainCategoryDetails.name}
      </MDBModalHeader>

      {toEdit ? viewMode() : editMode()}
    </MDBModal>
  );
};
MainCategoryEdit.propTypes = {
  updateMainCategory: PropTypes.func.isRequired
};
export default connect(null, { updateMainCategory })(MainCategoryEdit);
