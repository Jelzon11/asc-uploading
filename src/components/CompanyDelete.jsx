import React, { useState, useEffect } from "react";
import { setToInvalid, setToValid } from "../utils";
import { LoadingText } from "./Utility-Component";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { deleteCompany } from "../actions/companyAction";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol,
  
} from "mdbreact";

const CompanyDelete = props => {
  const [companyId, setCompanyId] = useState("");
  const [inProcess, setInProcess] = useState(false);

  useEffect(() => {
    setCompanyId(props.companyDetails.id);
  }, [props.companyDetails]);

  const handleSubmit = e => {
    e.preventDefault();

    if (!companyId) return false;
    setInProcess(true);
    props.deleteCompany(
      {
        id: companyId
      },
      () => {
        setInProcess(false);
        toastr.success("Company Successfully Deleted");
        closeSelfHandler();
      }
    );
  };

  const closeSelfHandler = () => {
    props.handleDeleteModalOpen();
  };

  return (
    <MDBModal
      isOpen={props.isDeleteModalOpen}
      toggle={props.handleDeleteModalOpen}
      full-height
      position="top"
    >
      <MDBModalHeader
        toggle={closeSelfHandler}
        tag="h4"
        className="font-weight-bold text-uppercase"
      >
        {props.companyDetails.name}
      </MDBModalHeader>
      <form className="needs-validation" onSubmit={handleSubmit} noValidate>
        <MDBModalBody>
          <MDBRow>
            <MDBCol>
              <p>
                Are you sure you want to delete{" "}
                <font style={{ fontWeight: "900", color: "red" }}>
                  {" "}
                  {props.companyDetails.name}{" "}
                </font>
                ?
              </p>
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
            No
          </MDBBtn>
          <MDBBtn color="light-green" type="submit">
            <LoadingText text={"Yes"} loading={inProcess} />
          </MDBBtn>
        </MDBModalFooter>
      </form>
    </MDBModal>
  );
};
CompanyDelete.propTypes = {
  deleteCompany: PropTypes.func.isRequired
};
export default connect(null, { deleteCompany })(CompanyDelete);
