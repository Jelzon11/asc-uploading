import React, { useState, Fragment, useEffect } from "react";
import { setToInvalid, setToValid } from "../utils";
import { LoadingText } from "./Utility-Component";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { updateProduct } from "../actions/productAction";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol
} from "mdbreact";

const SubSubCategoryEdit = props => {
  const [toEdit, setToEdit] = useState(true);
  const [productName, setProductName] = useState("");
  const [productId, setProductId] = useState("");
  const [inProcess, setInProcess] = useState(false);

  const handleClick = () => {
    setToEdit(!toEdit);
  };
  const handleProductNameChange = e => {
    setProductName(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };
  const handleSubmit = e => {
    e.preventDefault();
    if (!productName) setToInvalid(e.target.elements["productName"]);
    if (!productName || !productId) return false;
    const update_product = {
      name: productName,
      id: productId
    };
    setInProcess(true);
    props.updateProduct(update_product, () => {
      setInProcess(false);
      toastr.success("Product Successfully Updated");
      closeSelfHandler();
    });
  };

  useEffect(() => {
    setProductName(props.productDetails.name);
    setProductId(props.productDetails.id);
  }, [props.productDetails]);

  const closeSelfHandler = () => {
    props.handleEditModalOpen();
    setToEdit(!toEdit);
  };
  const closeHandler = () => {
    props.handleEditModalOpen();
    setToEdit(true);
  };

  const viewMode = () => {
    return (
      <Fragment>
        <MDBModalBody>
          <MDBRow style={{ marginBottom: "1rem" }}>
            <MDBCol>
              <label>Main Category</label>
              <input
                value={
                  props.productDetails &&
                  props.productDetails.mainCategoryDetails &&
                  props.productDetails.mainCategoryDetails.name
                }
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
          <MDBRow style={{ marginBottom: "1rem" }}>
            <MDBCol>
              <label>Sub Category</label>
              <input
                value={
                  props.productDetails &&
                  props.productDetails.subCategoryDetails &&
                  props.productDetails.subCategoryDetails.name
                }
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
          <MDBRow style={{ marginBottom: "1rem" }}>
            <MDBCol>
              <label>Company</label>
              <input
                value={
                  props.productDetails &&
                  props.productDetails.companyDetails &&
                  props.productDetails.companyDetails.name
                }
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
          <MDBRow style={{ marginBottom: "1rem" }}>
            <MDBCol>
              <label>Brand</label>
              <input
                value={
                  props.productDetails &&
                  props.productDetails.brandDetails &&
                  props.productDetails.brandDetails.name
                }
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
          <MDBRow style={{ marginBottom: "1rem" }}>
            <MDBCol>
              <label>Product</label>
              <input
                value={props.productDetails.name}
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="light-green" onClick={handleClick} type="submit">
            {" "}
            Edit
          </MDBBtn>
        </MDBModalFooter>
      </Fragment>
    );
  };
  const editMode = () => {
    return (
      <Fragment>
        <form className="needs-validation" onSubmit={handleSubmit} noValidate>
          <MDBModalBody>
            <MDBRow style={{ marginBottom: "1rem" }}>
              <MDBCol>
                <label>Product</label>
                <input
                  name={"productName"}
                  type="text"
                  value={productName}
                  className="form-control"
                  onChange={e => handleProductNameChange(e)}
                  required
                />
                <div className="invalid-feedback">
                  Please provide product name
                </div>
              </MDBCol>
            </MDBRow>
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
              Cancel
            </MDBBtn>
            <MDBBtn color="light-green" type="submit">
              <LoadingText text={"Submit"} loading={inProcess} />
            </MDBBtn>
          </MDBModalFooter>
        </form>
      </Fragment>
    );
  };
  return (
    <MDBModal
      isOpen={props.isEditModalOpen}
      toggle={props.handleEditModalOpen}
      full-height
      position="top"
    >
      <MDBModalHeader toggle={closeHandler}>
        {props.productDetails.name}
      </MDBModalHeader>

      {toEdit ? viewMode() : editMode()}
    </MDBModal>
  );
};
SubSubCategoryEdit.propTypes = {
  updateProduct: PropTypes.func.isRequired
};
export default connect(null, { updateProduct })(SubSubCategoryEdit);
