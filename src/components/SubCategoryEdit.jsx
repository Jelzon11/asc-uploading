import React, { useState, Fragment, useEffect } from "react";
import { setToInvalid, setToValid } from "../utils";
import { LoadingText } from "./Utility-Component";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { updateSubCategory } from "../actions/subCategoryAction";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol
} from "mdbreact";

const SubCategoryEdit = props => {
  const [toEdit, setToEdit] = useState(true);
  const [subCategoryName, setSubCategoryName] = useState("");
  const [subCategoryId, setSubCategoryId] = useState("");
  const [inProcess, setInProcess] = useState(false);

  const handleClick = () => {
    setToEdit(!toEdit);
  };
  const handleSubCategoryNameChange = e => {
    setSubCategoryName(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };
  const handleSubmit = e => {
    e.preventDefault();
    if (!subCategoryName) setToInvalid(e.target.elements["subCategoryName"]);
    if (!subCategoryName || !subCategoryId) return false;
    const update_subCategory = {
      name: subCategoryName,
      id: subCategoryId
    };
    setInProcess(true);
    props.updateSubCategory(update_subCategory, () => {
      setInProcess(false);
      toastr.success("Sub Category Successfully Updated");
      closeSelfHandler();
    });
  };

  useEffect(() => {
    setSubCategoryName(props.subCategoryDetails.name);
    setSubCategoryId(props.subCategoryDetails.id);
  }, [props.subCategoryDetails]);

  const closeSelfHandler = () => {
    props.handleEditModalOpen();
    setToEdit(!toEdit);
  };
  const closeHandler = () => {
    props.handleEditModalOpen();
    setToEdit(true);
  };

  const viewMode = () => {
    return (
      <Fragment>
        <MDBModalBody>
          <MDBRow>
            <MDBCol>
              <label>Main Category</label>
              <input
                value={
                  props.subCategoryDetails &&
                  props.subCategoryDetails.mainCategoryDetails &&
                  props.subCategoryDetails.mainCategoryDetails.name
                }
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol>
              <label>Sub Category</label>
              <input
                value={props.subCategoryDetails.name}
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="light-green" onClick={handleClick} type="submit">
            {" "}
            Edit
          </MDBBtn>
        </MDBModalFooter>
      </Fragment>
    );
  };
  const editMode = () => {
    return (
      <Fragment>
        <form className="needs-validation" onSubmit={handleSubmit} noValidate>
          <MDBModalBody>
            <MDBRow>
              <MDBCol>
                <label>Company</label>
                <input
                  name={"subCategoryName"}
                  type="text"
                  value={subCategoryName}
                  className="form-control"
                  onChange={e => handleSubCategoryNameChange(e)}
                  required
                />
                <div className="invalid-feedback">
                  Please provide sub category name
                </div>
              </MDBCol>
            </MDBRow>
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
              Cancel
            </MDBBtn>
            <MDBBtn color="light-green" type="submit">
              <LoadingText text={"Submit"} loading={inProcess} />
            </MDBBtn>
          </MDBModalFooter>
        </form>
      </Fragment>
    );
  };
  return (
    <MDBModal
      isOpen={props.isEditModalOpen}
      toggle={props.handleEditModalOpen}
      full-height
      position="top"
    >
      <MDBModalHeader toggle={closeHandler}>
        {props.subCategoryDetails.name}
      </MDBModalHeader>

      {toEdit ? viewMode() : editMode()}
    </MDBModal>
  );
};
SubCategoryEdit.propTypes = {
  updateSubCategory: PropTypes.func.isRequired
};
export default connect(null, { updateSubCategory })(SubCategoryEdit);
