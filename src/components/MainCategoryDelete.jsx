import React, { useState, useEffect } from "react";
import { setToInvalid, setToValid } from "../utils";
import { LoadingText } from "./Utility-Component";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { deleteMainCategory } from "../actions/mainCategoryAction";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol
} from "mdbreact";

const MainCategoryDelete = props => {
  const [mainCategoryId, setMainCategoryId] = useState("");
  const [inProcess, setInProcess] = useState(false);

  useEffect(() => {
    setMainCategoryId(props.mainCategoryDetails.id);
  }, [props.mainCategoryDetails]);

  const handleSubmit = e => {
    e.preventDefault();
    if (!mainCategoryId) return false;
    setInProcess(true);
    props.deleteMainCategory(
      {
        id: mainCategoryId
      },
      () => {
        setInProcess(false);
        toastr.success("Main Category Successfully Deleted");
        closeSelfHandler();
      }
    );
  };

  const closeSelfHandler = () => {
    props.handleDeleteModalOpen();
  };

  return (
    <MDBModal
      isOpen={props.isDeleteModalOpen}
      toggle={props.handleDeleteModalOpen}
      full-height
      position="top"
    >
      <MDBModalHeader
        toggle={closeSelfHandler}
        tag="h4"
        className="font-weight-bold text-uppercase"
      >
        {props.mainCategoryDetails.name}
      </MDBModalHeader>
      <form className="needs-validation" onSubmit={handleSubmit} noValidate>
        <MDBModalBody>
          <MDBRow>
            <MDBCol>
              <p>
                Are you sure you want to delete{" "}
                <font style={{ fontWeight: "900", color: "red" }}>
                  {" "}
                  {props.mainCategoryDetails.name}{" "}
                </font>
                ?
              </p>
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
            No
          </MDBBtn>
          <MDBBtn color="light-green" type="submit">
            <LoadingText text={"Yes"} loading={inProcess} />
          </MDBBtn>
        </MDBModalFooter>
      </form>
    </MDBModal>
  );
};
MainCategoryDelete.propTypes = {
  deleteMainCategory: PropTypes.func.isRequired
};
export default connect(null, { deleteMainCategory })(MainCategoryDelete);
