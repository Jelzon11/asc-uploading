import React, { useState, Fragment, useEffect } from "react";
import { setToInvalid, setToValid } from "../utils";
import { LoadingText } from "./Utility-Component";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { updateRole } from "../actions/roleAction";
import toastr from "toastr";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol
} from "mdbreact";


const RoleEdit = props => {
  const [toEdit, setToEdit] = useState(true);
  const [roleName, setRoleName] = useState("");
  const [roleId, setRoleId] = useState("");
  const [inProcess, setInProcess] = useState(false);

  const handleClick = () => {
    setToEdit(!toEdit);
  };
  const handleRoleNameChange = e => {
    setRoleName(e.target.value);
    setToValid(e.target);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };
  const handleSubmit = e => {
    e.preventDefault();
    if (!roleName) setToInvalid(e.target.elements["role"]);
    if (!roleName || !roleId) return false;
    const update_role = {
      name: roleName,
      id: roleId
    };
    setInProcess(true);
    props.updateRole(update_role, () => {
      setInProcess(false);
      toastr.success("Role Successfully Updated");
      closeSelfHandler();
    });
  };

  useEffect(() => {
    setRoleName(props.roleDetails.name);
    setRoleId(props.roleDetails.id);

  }, [props.roleDetails]);

  const closeSelfHandler = () => {
    props.handleEditModalOpen();
    setToEdit(!toEdit);
  };
  const closeHandler = () => {
    props.handleEditModalOpen();
    setToEdit(true);
  };
  
  const viewMode = () => {
    return (
      <Fragment>
        <MDBModalBody>
          <MDBRow>
            <MDBCol>
              <label>Main Category</label>
              <input
                value={props.roleDetails.name}
                className="form-control"
                disabled
              />
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="light-green" onClick={handleClick} type="submit">
            {" "}
            Edit
          </MDBBtn>
        </MDBModalFooter>
      </Fragment>
    );
  };
  const editMode = () => {
    return (
     
      <Fragment>
        <form className="needs-validation" onSubmit={handleSubmit} noValidate>
          <MDBModalBody>
            <MDBRow>
              <MDBCol>
                <label>Company</label>
                <input
                  name={"mainCategoryName"}
                  type="text"
                  value={mainCategoryName}
                  className="form-control"
                  onChange={e => handleRoleNameChange(e)}
                  required
                />
                <div className="invalid-feedback">
                  Please provide role name
                </div>
              </MDBCol>
            </MDBRow>
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn color="blue-grey" onClick={closeSelfHandler}>
              Cancel
            </MDBBtn>
            <MDBBtn color="light-green" type="submit">
              <LoadingText text={"Submit"} loading={inProcess} />
            </MDBBtn>
          </MDBModalFooter>
        </form>
      </Fragment>
    );
  };
  return (
    <MDBModal
      isOpen={props.isEditModalOpen}
      toggle={props.handleEditModalOpen}
      full-height
      position="top"
    >
      <MDBModalHeader toggle={closeHandler}>
        {props.roleDetails.name}
      </MDBModalHeader>

      {toEdit ? viewMode() : editMode()}
    </MDBModal>
  );
};
RoleEdit.propTypes = {
  updateRole: PropTypes.func.isRequired
};
export default connect(null, { updateRole })(RoleEdit);
