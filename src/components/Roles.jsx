import "../css/datatables.css";
import "../css/bs4.css";
import React, { useEffect, useState, Fragment } from "react";
import {
  MDBDataTable,
  MDBCard,
  MDBCardHeader,
  MDBCardBody,
  MDBBtn
} from "mdbreact";
import { connect } from "react-redux";
import moment from "moment";
import RoleModal from "./RoleModal";

const columns = () => {
  return [
    {
      label: "Name",
      field: "name",
      sort: "asc",
      width: 500
    },
    {
      label: "Last Updated",
      field: "updatedTimestamp",
      sort: "asc",
      width: 100
    },
    {
      label: "Created Date",
      field: "createdTimestamp",
      sort: "asc",
      width: 100
    },
    {
      label: "Action",
      field: "action",
      sort: "asc",
      width: 100
    }
  ];
};

const Roles = () => {
  const [roleList, setRoleList] = useState([]);
  const [isModalOpen, setModalOpen] = useState(false);
  const [isCreatedSuccess, setCreatedSuccess] = useState(false);

  const handleModalOpen = () => {
    if (isModalOpen == false) {
      setModalOpen(true);
    } else {
      setModalOpen(false);
    }
  };

  const successToastHandler = () => {
    setCreatedSuccess(true);
  };
  return (
    <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="col-md-12">
            <br />
            <div>
            <RoleModal 
              isModalOpen={isModalOpen}
              successToastHandler={successToastHandler}
              handleModalOpen={handleModalOpen}
              />
              <MDBCard style={{ padding: "1rem" }}>
                <MDBCardHeader
                  tag="h4"
                  className="font-weight-bold text-uppercase"
                >
                  Roles
                </MDBCardHeader>
                <div className="d-flex align-items-start flex-column bd-highlight example-parent">
                  <MDBBtn onClick={handleModalOpen} color="default">
                    Add Role
                  </MDBBtn>
                </div>
                <hr />

                <MDBCardBody>
                  {/* <MDBDataTable
                                        fixed={true}
                                        small
                                        bordered
                                        hover
                                        order={['name', 'desc']}
                                        disableRetreatAfterSorting={true}
                                        sortable={true}
                                        responsive={true}
                                        searchLabel={'Search...'}
                                        responsiveLg={true}
                                        responsiveXl={true}
                                        noBottomColumns={true}
                                        data={{
                                            rows: renderData(userList),
                                            columns: columns()
                                        }}
                                    /> */}
                </MDBCardBody>
              </MDBCard>
            </div>
            <br />
          </div>
        </div>
      </div>
    </div>
  );
};
export default Roles;
