import { FETCH_RADIO_MATERIALS, ADD_RADIO_MATERIAL, DELETE_RADIO_MATERIAL } from './types';
import axios from 'axios';
import { getToken } from '../utils';
const url = process.env.REACT_APP_URL;
const token = 'bearer ' + getToken();

export const createRadioMaterial = (formData, progressCallback, callBack) => dispatch => {
  axios.request({
    method: 'POST',
    url: url + '/api/materials/radios',
    headers: { 'authorization': token },
    data: formData,
    onUploadProgress: (p) => {
      const percent =  p.loaded/p.total;
      progressCallback && progressCallback(percent * 100);
    }
  })
  .then(resData => {
    dispatch({
      type: ADD_RADIO_MATERIAL,
      payload: resData.data
    })
    callBack && callBack()
  }).catch((err) => {
    console.log('Radio-Action: createRadioMaterial Error:', err);
  });
}

//////////////////////////////////////////////////////

export const fetchReferenceCode = (referenceId, callback) => dispatch => {

  fetch(url + '/api/materials/radios/referencesids/exists', {
      method: 'POST',
      headers: {
          'content-type': 'application/json',
          'authorization': token
      },
      body: JSON.stringify(referenceId)
  })
      .then((res) => {
        if(res.status >= 400) throw new Error('Exist'); 
          return res.json
      })
      .then(() => callback('not exist'))
      .catch(() => callback('exist'))
}

//////////////////////////////////////////////////////

export const fetchRadioMaterials = () => dispatch => {
  fetch(url + '/api/materials/radios',{
                   method: 'GET',
                   headers: {
                      //'content-type': 'application/json',
                      'authorization': token
                   }
               })
      .then(res => res.json())
      .then(radioMaterials =>
          dispatch({
              type: FETCH_RADIO_MATERIALS,
              payload: radioMaterials
          })
      );
}

export const deleteRadioMaterial = (deleteRadioMaterial, callback) => dispatch => {
  fetch(url + "/api/materials/radios", {
    method: "DELETE",
    headers: {
      "content-type": "application/json",
      authorization: token
    },
    body: JSON.stringify(deleteRadioMaterial)
  })
    .then(res => res.json())
    .then((data) => {
      dispatch({
        type: DELETE_RADIO_MATERIAL,
        payload: data
      });
      callback();
    });
};