import { FETCH_SUB_CATEGORIES, ADD_SUB_CATEGORY, UPDATE_SUB_CATEGORY, DELETE_SUB_CATEGORY } from './types';
import moment from 'moment'
import { getToken } from '../utils';
const url = process.env.REACT_APP_URL;
const token = 'bearer ' + getToken();

//Get list SubCategory Action
export const fetchSubCategories = () => dispatch => {
  fetch(url + '/api/materials/subcategories', {
      method: 'GET',
      headers: {
        'content-type': 'application/json',
        'authorization': token
      }
    })
    .then(res => res.json())
    .then(sub_categories =>
      dispatch({
        type: FETCH_SUB_CATEGORIES,
        payload: sub_categories
      })
    );
}

//Create SubCategory Action
export const createSubCategory = (subData, callBack) => dispatch => {
  fetch(url + '/api/materials/subcategories', {
    method: 'POST',
    headers: {
      'content-type': 'application/json',
      'authorization': token
    },
    body: JSON.stringify(subData)
  })
  .then(res => res.json())
  .then(sub_category => {
    dispatch({
      type: ADD_SUB_CATEGORY,
      payload: sub_category
    })
    callBack && callBack()
  });
};

export const updateSubCategory = (updateSubData, callback) => dispatch => {
  fetch(url + "/api/materials/subcategories", {
    method: "PUT",
    headers: {
      "content-type": "application/json",
      authorization: token
    },
    body: JSON.stringify(updateSubData)
  }).then(() => {
    updateSubData.updatedTimestamp = moment().unix();
    dispatch({
      type: UPDATE_SUB_CATEGORY,
      payload: updateSubData
    });
    callback();
  });
};

export const deleteSubCategory = (deleteSubData, callback) => dispatch => {
  fetch(url + "/api/materials/subcategories", {
    method: "DELETE",
    headers: {
      "content-type": "application/json",
      authorization: token
    },
    body: JSON.stringify(deleteSubData)
  })
    .then(res => res.json())
    .then((data) => {
      dispatch({
        type: DELETE_SUB_CATEGORY,
        payload: data
      });
      callback();
    });
};


