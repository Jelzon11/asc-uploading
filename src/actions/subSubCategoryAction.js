import { FETCH_SUB_SUB_CATEGORIES, ADD_SUB_SUB_CATEGORY, UPDATE_SUB_SUB_CATEGORY, DELETE_SUB_SUB_CATEGORY} from './types';
import moment from 'moment';
import { getToken } from '../utils';
const url = process.env.REACT_APP_URL;
const token = 'bearer ' + getToken();

export const fetchSubSubCategories = () => dispatch => {
  fetch(url + '/api/materials/subsubcategories', {
      method: 'GET',
      headers: {
        'content-type': 'application/json',
        'authorization': token
      }
    })
    .then(res => res.json())
    .then(sub_sub_categories =>
      dispatch({
        type: FETCH_SUB_SUB_CATEGORIES,
        payload: sub_sub_categories
      })
    );
}

export const createSubSubCategories = (subSubData, callBack) => dispatch => {
  fetch(url + '/api/materials/subsubcategories', {
    method: 'POST',
    headers: {
      'content-type': 'application/json',
      'authorization': token
    },
    body: JSON.stringify(subSubData)
  })
  .then(res => res.json())
  .then(subsub_category => {
    dispatch({
      type: ADD_SUB_SUB_CATEGORY,
      payload: subsub_category
    })
    callBack && callBack();
  });
};

export const updateSubSubCategory = (updateSubSubData, callback) => dispatch => {
  fetch(url + "/api/materials/subsubcategories", {
    method: "PUT",
    headers: {
      "content-type": "application/json",
      authorization: token
    },
    body: JSON.stringify(updateSubSubData)
  }).then(() => {
    updateSubSubData.updatedTimestamp = moment().unix();
    dispatch({
      type: UPDATE_SUB_SUB_CATEGORY,
      payload: updateSubSubData
    });
    callback();
  });
};

export const deleteSubSubCategory = (deleteSubSubData, callback) => dispatch => {
  fetch(url + "/api/materials/subsubcategories", {
    method: "DELETE",
    headers: {
      "content-type": "application/json",
      authorization: token
    },
    body: JSON.stringify(deleteSubSubData)
  })
    .then(res => res.json())
    .then((data) => {
      dispatch({
        type: DELETE_SUB_SUB_CATEGORY,
        payload: data
      });
      callback();
    });
};

