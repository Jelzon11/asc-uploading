import {
  FETCH_ROLE,
  ADD_ROLE,
  UPDATE_ROLE,
  DELETE_ROLE
} from "../actions/types";
import moment from 'moment';
import { getToken } from '../utils';
const url = process.env.REACT_APP_URL;
const token = 'bearer ' + getToken();

export const fetchRoles = () => dispatch => {
  fetch(url + "/api/users/roles", {
    method: "GET",
    headers: {
      "content-type": "application/json",
      authorization: token
    }
  })
    .then(res => res.json())
    .then(roles =>
      dispatch({
        type: FETCH_ROLE,
        payload: roles
      })
    );
};

export const createRole = (roleData, callback) => dispatch => {
  fetch(url + "/api/materials/companies", {
    method: "POST",
    headers: {
      "content-type": "application/json",
      authorization: token
    },
    body: JSON.stringify(roleData)
  })
    .then(res => res.json())
    .then(role => {
      dispatch({
        type: ADD_ROLE,
        payload: role
      });
      callback();
    });
};

export const updateRole = (updateRoleData, callback) => dispatch => {
  fetch(url + "/api/materials/companies", {
    method: "PUT",
    headers: {
      "content-type": "application/json",
      authorization: token
    },
    body: JSON.stringify(updateRoleData)
  }).then(() => {
    updateRoleData.updatedTimestamp = moment().unix();
    dispatch({
      type: UPDATE_ROLE,
      payload: updateRoleData
    });
    callback();
  });
};

export const deleteRole = (deleteRoleData, callback) => dispatch => {
  fetch(url + "/api/materials/companies", {
    method: "DELETE",
    headers: {
      "content-type": "application/json",
      authorization: token
    },
    body: JSON.stringify(deleteRoleData)
  })
    .then(res => res.json())
    .then((data) => {
      dispatch({
        type: DELETE_ROLE,
        payload: data
      });
      callback();
    });
};
