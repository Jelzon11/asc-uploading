import { FETCH_COMPANIES, ADD_COMPANY, UPDATE_COMPANY, DELETE_COMPANY } from './types';
import moment from 'moment';
import { getToken } from '../utils';
const url = process.env.REACT_APP_URL;
const token = 'bearer ' + getToken();

export const fetchCompanies = () => dispatch => {
  fetch(url + "/api/materials/companies", {
    method: "GET",
    headers: {
      "content-type": "application/json",
      authorization: token
    }
  })
    .then(res => res.json())
    .then(companies =>
      dispatch({
        type: FETCH_COMPANIES,
        payload: companies
      })
    );
};

export const createCompany = (companyData, callback) => dispatch => {
  fetch(url + "/api/materials/companies", {
    method: "POST",
    headers: {
      "content-type": "application/json",
      authorization: token
    },
    body: JSON.stringify(companyData)
  })
    .then(res => res.json())
    .then(company => {
      dispatch({
        type: ADD_COMPANY,
        payload: company
      });
      callback();
    });
};

export const updateCompany = (updateCompanyData, callback) => dispatch => {
  
  fetch(url + "/api/materials/companies", {
    method: "PUT",
    headers: {
      "content-type": "application/json",
      authorization: token
    },
    body: JSON.stringify(updateCompanyData)
  }).then(() => {
    updateCompanyData.updatedTimestamp = moment().unix();
    dispatch({
      type: UPDATE_COMPANY,
      payload: updateCompanyData
    });
    callback();
  });
};

export const deleteCompany = (deleteCompanyData, callback) => dispatch => {
  
  fetch(url + "/api/materials/companies", {
    method: "DELETE",
    headers: {
      "content-type": "application/json",
      authorization: token
    },
    body: JSON.stringify(deleteCompanyData)
  })
    .then(res => res.json())
    .then((data) => {
      dispatch({
        type: DELETE_COMPANY,
        payload: data
      });
      callback();
    });
};
