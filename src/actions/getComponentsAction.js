import { FETCH_COMPONENTS } from './types';
import { getToken } from '../utils';
const url = process.env.REACT_APP_URL;
const token = 'bearer ' + getToken();

export const fetchComponents = () => dispatch => {
    fetch(url + "/api/components", {
        method: 'GET',
        headers: {
            'content-type': 'application/json',
            'authorization': token
        }
    })
    .then(res => res.json())
    .then(components =>
        dispatch({
            type: FETCH_COMPONENTS,
            payload: components
        })
    );
}