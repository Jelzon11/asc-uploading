import { FETCH_PRODUCTS, ADD_PRODUCT, UPDATE_PRODUCT, DELETE_PRODUCT } from './types';
import moment from 'moment';
import { getToken } from '../utils';
import { validateAccess } from '../utils';
const url = process.env.REACT_APP_URL;
const token = 'bearer ' + getToken();

export const fetchProducts = () => dispatch => {
    
    fetch(url + "/api/products", {
        method: 'GET',
        headers: {
            'content-type': 'application/json',
            'authorization': token
        }
    })
    .then(res => res.json())
    .then(product =>
        dispatch({
            type: FETCH_PRODUCTS,
            payload: product
        })
    );
}



export const createProduct = (productData, callback) => dispatch => {
    fetch(url + "/api/products", {
        method: 'POST',
        headers: {
            'content-type': 'application/json',
            'authorization': token

        },
        body: JSON.stringify(productData)
    })
        .then(res => res.json())
        .then(product => {
            dispatch({
                type: ADD_PRODUCT,
                payload: product
            })
            callback();
        });
        
    };

    export const updateProduct = (updateProductData, callback) => dispatch => {
        fetch(url + "/api/products", {
          method: "PUT",
          headers: {
            "content-type": "application/json",
            authorization: token
          },
          body: JSON.stringify(updateProductData)
        }).then(() => {
            updateProductData.updatedTimestamp = moment().unix();
          dispatch({
            type: UPDATE_PRODUCT,
            payload: updateProductData
          });
          callback();
        });
      };

      export const deleteProduct = (deleteProductData, callback) => dispatch => {
        fetch(url + "/api/products", {
          method: "DELETE",
          headers: {
            "content-type": "application/json",
            authorization: token
          },
          body: JSON.stringify(deleteProductData)
        })
          .then(res => res.json())
          .then((data) => {
            dispatch({
              type: DELETE_PRODUCT,
              payload: data
            });
            callback();
          });
      };
   


