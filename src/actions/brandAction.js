import {
  FETCH_BRANDS,
  ADD_BRAND,
  UPDATE_BRAND,
  DELETE_BRAND
} from './types';
import moment from 'moment';
import {
  getToken
} from '../utils';
import {
  validateAccess
} from '../utils';
const url = process.env.REACT_APP_URL;
const token = 'bearer ' + getToken();

export const fetchBrands = () => dispatch => {
  fetch(url + "/api/materials/brands", {
      method: 'GET',
      headers: {
        'content-type': 'application/json',
        'authorization': token
      }
    })
    .then(res => res.json())
    .then(brands =>
      dispatch({
        type: FETCH_BRANDS,
        payload: brands
      })
    );
}


export const createBrand = (brandData, callback) => dispatch => {
  fetch(url + "/api/materials/brands", {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        'authorization': token
      },
      body: JSON.stringify(brandData)
    })
    .then(res => res.json())
    .then(brand => {
      dispatch({
        type: ADD_BRAND,
        payload: brand
      })
      callback();
    });
};

export const updateBrand = (updateBrandData, callback) => dispatch => {
  fetch(url + "/api/materials/brands", {
    method: "PUT",
    headers: {
      "content-type": "application/json",
      authorization: token
    },
    body: JSON.stringify(updateBrandData)
  }).then(() => {
    updateBrandData.updatedTimestamp = moment().unix();
    dispatch({
      type: UPDATE_BRAND,
      payload: updateBrandData
    });
    callback();
  });
};

export const deleteBrand = (deleteBrandData, callback) => dispatch => {
  fetch(url + "/api/materials/brands", {
      method: "DELETE",
      headers: {
        "content-type": "application/json",
        authorization: token
      },
      body: JSON.stringify(deleteBrandData)
    })
    .then(res => res.json())
    .then((data) => {
      dispatch({
        type: DELETE_BRAND,
        payload: data
      });
      callback();
    });
};
