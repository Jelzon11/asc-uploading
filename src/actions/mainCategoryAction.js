import { FETCH_MAIN_CATEGORIES, ADD_MAIN_CATEGORY, DELETE_MAIN_CATEGORY, UPDATE_MAIN_CATEGORY } from './types';
import moment from 'moment';
import { getToken } from '../utils';
const url = process.env.REACT_APP_URL;
const token = 'bearer ' + getToken();

export const fetchMainCategories = () => dispatch => {
  fetch(url + '/api/materials/maincategories', {
      method: 'GET',
      headers: {
        'content-type': 'application/json',
        'authorization': token
      }
    })
    .then(res => res.json())
    .then(main_categories =>
      dispatch({
        type: FETCH_MAIN_CATEGORIES,
        payload: main_categories
      })
    );
}

export const createMainCategory = (mainData, callback) => dispatch => {
  fetch(url + '/api/materials/maincategories', {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        'authorization': token
      },
      body: JSON.stringify(mainData)
    })
    .then(res => res.json())
    .then(main_category => {
      dispatch({
        type: ADD_MAIN_CATEGORY,
        payload: main_category
      })
      callback();
    });
};

export const updateMainCategory = (updateMainData, callback) => dispatch => {
  console.log(updateMainData);
  fetch(url + "/api/materials/maincategories", {
    method: "PUT",
    headers: {
      "content-type": "application/json",
      authorization: token
    },
    body: JSON.stringify(updateMainData)
  }).then(() => {
    updateMainData.updatedTimestamp = moment().unix();
    dispatch({
      type: UPDATE_MAIN_CATEGORY,
      payload: updateMainData
    });
    callback();
  });
};

export const deleteMainCategory = (deleteMainData, callback) => dispatch => {
 
  fetch(url + "/api/materials/maincategories", {
    method: "DELETE",
    headers: {
      "content-type": "application/json",
      authorization: token
    },
    body: JSON.stringify(deleteMainData)
  })
    .then(res => res.json())
    .then((data) => {
      dispatch({
        type: DELETE_MAIN_CATEGORY,
        payload: data
      });
      callback();
    });
};
