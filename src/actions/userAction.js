import { FETCH_USERS, ADD_USER, FETCH_ME, UPDATE_USER, DELETE_USER, FETCH_LOGS } from './types';
import moment from 'moment';
import { getToken } from '../utils';
const url = process.env.REACT_APP_URL;  
const token = 'bearer ' + getToken();

export const fetchUsers = () => dispatch => {
    fetch(url + '/api/users', {
        method: 'GET',
        headers: {
            'content-type': 'application/json',
            'authorization': token
        }
    })
        .then(res => res.json())
        .then(users =>
            dispatch({
                type: FETCH_USERS,
                payload: users
            })
        );
}

export const fetchUserEmails = (userEmail, callback) => dispatch => {

  fetch(url + '/api/users/emails/exists', {
      method: 'POST',
      headers: {
          'content-type': 'application/json',
          'authorization': token

      },
      body: JSON.stringify(userEmail)
  })
      .then((res) => {
        if(res.status >= 400) throw new Error('Exist'); 
          return res.json
      })
      .then(() => callback('not exist'))
      .catch(() => callback('exist'))
}


export const createUser = (userData, callback) => dispatch => {

    fetch(url + '/api/users', {
        method: 'POST',
        headers: {
            'content-type': 'application/json',
            'authorization': token

        },
        body: JSON.stringify(userData)
    })
        .then(res => res.json())
        .then(user => {
            dispatch({
                type: ADD_USER,
                payload: user
            })
            callback(user);
        });
}

export const updateUser = (updateUserData, callback) => dispatch => {
    fetch(url + "/api/users", {
      method: "PUT",
      headers: {
        "content-type": "application/json",
        authorization: token
      },
      body: JSON.stringify(updateUserData)
    }).then(() => {
        updateUserData.updatedTimestamp = moment().unix();
        
      dispatch({
        type: UPDATE_USER,
        payload: updateUserData
      });
      callback();
    });
  };
  
  export const deleteUser = (deleteUserData, callback) => dispatch => {
    
    fetch(url + "/api/users", {
      method: "DELETE",
      headers: {
        "content-type": "application/json",
        authorization: token
      },
      body: JSON.stringify(deleteUserData)
    })
      .then(res => res.json())
      .then((data) => {
        dispatch({
          type: DELETE_USER,
          payload: data
        });
        callback();
      });
  };

export const LogoutUser = (callback) => {
    fetch(url + '/api/users/logins', {
        method: 'DELETE',
        headers: {
            'content-type': 'application/json',
            'authorization': token
        }
    }).then( () => {
        callback &&  callback();
     });
}

export const fetchMyInfo = (callback) => dispatch => {
    fetch(url + '/api/users/me', {
        method: 'GET',
        headers: {
            'content-type': 'application/json',
            'authorization': token
        }
    })
        .then(res => res.json())
        .then(user =>{
            dispatch({
                type: FETCH_ME,
                payload: user
            })
            callback && callback();
        });     
}

export const fetchLogs = (userId, callback) => dispatch => {
  console.log(' from userLogs '+ userId)
  fetch(url + '/api/users/logs?userId=' + userId, {
    method: 'GET',
    headers: {
      'content-type': 'application/json',
      'authorization': token
    }
  })
  .then(res => res.json())
  .then(logs =>{
      dispatch({
          type: FETCH_LOGS,
          payload: logs
      })
      callback && callback();
  });     
}


