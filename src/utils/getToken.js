export default () => {
  const session = localStorage.getItem('ascarchiving');
  if(!session) return null;
  const encodeStr = window.atob(session);
  const parseObject = JSON.parse(encodeStr);
  const token = parseObject.token;
  return token;
}