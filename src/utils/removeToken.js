export default (callback) => {
  const session = localStorage.getItem('ascarchiving');
  if(session){
    localStorage.removeItem('ascarchiving');
    callback && callback();
  } 
}