export default [{
    id: 1,
    name: 'Original'
}, {
    id: 2,
    name: 'EditDown/Derivatives'
}, {
    id: 3,
    name: 'AOB Live'
}, {
    id: 4,
    name: 'AOB Recorded'
}, {
    id: 5,
    name: 'Live DJ Discussion'
}]