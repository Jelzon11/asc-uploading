export default async (token) => {
  let isValid = false;
  if(!token) throw new Error("token is empty");
  await fetch(process.env.REACT_APP_URL + '/api/tokens/validates',{
    method: 'POST',
    headers: {
      'content-type': 'application/json',
      'authorization': token
    },
  })
  .then(resData =>resData.json())
  .then((data) => { 
    if(data && data.isValid){isValid = true}});
  return isValid
}