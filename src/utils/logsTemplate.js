import moment from 'moment';
export default (logId, logParameters, logDetails) => {
  console.log("logId = " + JSON.stringify(logId));
  console.log("parameters = " + JSON.stringify(logParameters));
  console.log("logDetails = " + JSON.stringify(logDetails));

  let referenceId;
  let materialId;
  let title;
  let action;
  let mainCategoryId;
  let mainCategoryName;
  let subCategoryId;
  let subCategoryName;
  let companyId;
  let companyName;
  let brandId;
  let brandName;
  let productId;
  let productName;
  let userId;
  let lastName;
  let firstName;
  let middleName;
  let roleId;
  let roleName;
  let streamRecordId;
  let station;
  let city;
  let region;
  let dateAired;
  let actionTaken;
  let client;
  

  
   switch (logId) {
    case '0f24bb36-6a5e-445b-a827-ffc25448363e':
        referenceId = logParameters.referenceId;
        materialId = logParameters.materialId;
        title = logParameters.title;
        action = logDetails.actionDescription;
    return action +  ' ' + title +' with a reference-code of ' + referenceId;
    
    case '858e64e9-ea6e-4cc2-904b-01b41fb432b1':
        referenceId = logParameters.referenceId;
        materialId = logParameters.materialId;
        title = logParameters.title;
        action = logDetails.actionDescription;
    return action +  ' ' + title +' with a reference-code of ' + referenceId;

    case '0b6d4412-87ca-4750-ba05-7bff9dde8c3b':
        mainCategoryId = logParameters.mainCategoryId;
        mainCategoryName = logParameters.mainCategoryName;
        action = logDetails.actionDescription;
    return action +  ' ' + mainCategoryName;
    
    case '690d6e1f-c8e7-4862-b223-231aab4db9d1':
        mainCategoryId = logParameters.mainCategoryId;
        mainCategoryName = logParameters.mainCategoryName;
        action = logDetails.actionDescription;
    return action +  ' ' + mainCategoryName;

    case 'c4701a0b-4135-4777-abf6-4b0cecc28172':
        mainCategoryId = logParameters.mainCategoryId;
        mainCategoryName = logParameters.mainCategoryName;
        subCategoryId = logParameters.subCategoryId;
        subCategoryName = logParameters.subCategoryName;
        action = logDetails.actionDescription;
    return action +  ' ' + subCategoryName + ' under ' + mainCategoryName;

    case 'a81b827f-abee-4bc5-af79-15c9be896597':
        mainCategoryId = logParameters.mainCategoryId;
        mainCategoryName = logParameters.mainCategoryName;
        subCategoryId = logParameters.subCategoryId;
        subCategoryName = logParameters.subCategoryName;
        action = logDetails.actionDescription;
    return action +  ' ' + subCategoryName + ' under ' + mainCategoryName;

    case '9012bd34-a302-4280-b2f0-0c272ce77105':
        companyId = logParameters.companyId;
        companyName = logParameters.companyName;
        action = logDetails.actionDescription;
    return action +  ' ' + companyName;
    
    case 'e11c8c79-5742-4765-9a45-42a0f2874faa':
        companyId = logParameters.companyId;
        companyName = logParameters.companyName;
        action = logDetails.actionDescription;
    return action +  ' ' + companyName;

    
    case 'd78d6266-a167-4bcb-a604-82523129e1be':
        companyId = logParameters.companyId;
        companyName = logParameters.companyName;
        brandId = logParameters.brandId;
        brandName = logParameters.brandName;
        action = logDetails.actionDescription;
    return action +  ' ' + brandName + ' under ' + companyName;

    
    case '2327a045-188a-4d2b-93d0-53732f3c5a13':
        companyId = logParameters.companyId;
        companyName = logParameters.companyName;
        brandId = logParameters.brandId;
        brandName = logParameters.brandName;
        action = logDetails.actionDescription;
    return action +  ' ' + brandName + ' under ' + companyName;

    case 'be2a28b4-e312-4aeb-8d79-2be131b6e994':
        companyId = logParameters.companyId;
        companyName = logParameters.companyName;
        brandId = logParameters.brandId;
        brandName = logParameters.brandName;
        productId = logParameters.productId;
        productName = logParameters.productName;
        action = logDetails.actionDescription;
    return action +  ' ' + productName + ' under ' + brandName + ' of ' + companyName;

    case '2fc86c41-852c-427b-be26-338bde104c02':
        productId = logParameters.productId;
        productName = logParameters.productName;
        action = logDetails.actionDescription;
    return action +  ' ' + productName;

    case '65a38e02-6bbb-4cf5-8e1d-d37800b4795c':
        userId = logParameters.userId;
        firstName = logParameters.firstName;
        middleName = logParameters.middleName;
        lastName = logParameters.lastName;
        roleId = logParameters.roleId;
        roleName = logParameters.roleName;
        action = logDetails.actionDescription;
    return action +  ' ' + firstName + middleName + lastName + ' as ' + roleName;

    case 'f16cb205-c6dc-4880-9861-2479b115e982':
        userId = logParameters.userId;
        firstName = logParameters.firstName;
        middleName = logParameters.middleName;
        lastName = logParameters.lastName;
        roleId = logParameters.roleId;
        roleName = logParameters.roleName;
        action = logDetails.actionDescription;
    return action +  ' ' + firstName + middleName + lastName + ' as ' + roleName;

    case '0a548fcc-a366-485b-8421-ed44c92c67bc':
        streamRecordId = logParameters.streamRecordId;
        referenceId = logParameters.referenceId;
        title = logParameters.title;
        station = logParameters.station;
        region = logParameters.region;
        city = logParameters.city;
        dateAired =  moment(logParameters.dateAired).unix('YYYY-MM-dddd hh:mm:ss a');
        actionTaken = logParameters.action;
        action = logDetails.actionDescription;
    return action +  ' ' + title + ' at ' + station + region + city + ' last ' + dateAired;

    case '2db392ed-3be7-448e-8b9f-e5d290eba415':
        client = 'ASC-MONITORING/ASC-ARCHIVING';
        firstName = logParameters.firstName;
        middleName = logParameters.middleName;
        lastName = logParameters.lastName;
        roleId = logParameters.roleId;
        roleName = logParameters.roleName;
        action = logDetails.actionDescription;
    return action +  ' as ' + roleName;

    case '79e4f89f-16ce-45e9-9db2-25b53e4c415e':
        client = 'ASC-MONITORING/ASC-ARCHIVING';
        firstName = logParameters.firstName;
        middleName = logParameters.middleName;
        lastName = logParameters.lastName;
        roleId = logParameters.roleId;
        roleName = logParameters.roleName;
        action = logDetails.actionDescription;
    return action;


    default:
      return 0;
  }
}