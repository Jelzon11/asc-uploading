import Cryptr from 'cryptr';
const crtpyr = new Cryptr(process.env.REACT_APP_CLIENTSECRET);
let accessDecrypted = [];

export const decrypted = () => {
  const session = localStorage.getItem('ascarchiving');
  if(session){
    const encodeStr = window.atob(session);
    const parseObject =  JSON.parse(encodeStr);
    const accessEcrypted = parseObject.access;
    accessDecrypted = crtpyr.decrypt(accessEcrypted);
  }
}
// const encodeStr = window.atob(session);
// const parseObject =  JSON.parse(encodeStr);
// const accessEcrypted = parseObject.access;
// accessDecrypted = crtpyr.decrypt(accessEcrypted);

export default (componentId) => {
  return accessDecrypted.indexOf(componentId) >= 0;
}