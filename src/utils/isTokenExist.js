export default () => {
  const session = localStorage.getItem('ascarchiving');
  if(!session) return false;
  const encodeStr = window.atob(session);
  const parseObject = JSON.parse(encodeStr);
  if(!parseObject.token) return false;
  return true;
}